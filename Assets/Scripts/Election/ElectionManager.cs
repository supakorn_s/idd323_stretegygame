using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ElectionManager : MonoBehaviour
{
    [Header("Election Content")]
    [SerializeField] private GameObject _electionContent;
    [SerializeField] private GameObject _useMoneyButton;
    [SerializeField] private GameObject _startElectionButton;
    [SerializeField] private GameObject _closeButton;
    [SerializeField] private TextMeshProUGUI _useMoneyText;
    [SerializeField] private int _cheatingPrice;

    [Header("Overall Election Slider")]
    [SerializeField] private Slider _electionSlider;
    [SerializeField] private Image _electionImager;
    [SerializeField] private Gradient _electionGradient;
    [SerializeField] private TextMeshProUGUI _electionPercentage;
    private bool _electionUIIsOpen = false;

    [Header("Voting Result Slider")]
    [SerializeField] private Slider _resultSlider;
    [SerializeField] private Image _resultImager;
    [SerializeField] private Gradient _resultGradient;
    [SerializeField] private TextMeshProUGUI _resultPercentage;
    [SerializeField] private bool _startingElection = false;
    [SerializeField] private bool _isElectionCompleted = false;
    private float _votingSpeed = 10;
    private float _votingResult = 0;

    [Header("Audio")]
    [SerializeField] private AudioSource _sfxSource;
    [SerializeField] private float _votingSound = 0.1f;
    private float _votingSoundCountdown;

    [Header("Data")]
    [SerializeField] private CountryGovernment _countryGovernment;
    [SerializeField] private CountryEconomy _countryEconomy;
    private GovernmentData _governmentData;
    private CameraController _cameraController;
    private CalenderTimer _calenderTimer;
    private EconomyData _economyData;
    private WinLoseManager _winLoseManager;
    public CountryGovernment countryGovernment => _countryGovernment;
    public GovernmentData governmentData => _governmentData;

    private void Start()
    {
        if (_countryGovernment == null) _countryGovernment = FindObjectOfType<CountryGovernment>();
        if (_countryGovernment == null) _countryEconomy = FindObjectOfType<CountryEconomy>();
        if (_cameraController == null) _cameraController = FindObjectOfType<CameraController>();
        if (_calenderTimer == null) _calenderTimer = FindObjectOfType<CalenderTimer>();
        if (_winLoseManager == null) _winLoseManager = FindObjectOfType<WinLoseManager>();
        _governmentData = _countryGovernment.governmentData;
        _economyData = GameManager.Instance.economyData;
        

        _electionContent.SetActive(false);
        _votingSoundCountdown = _votingSound;

    }
    private void Update()
    {
        UpdateValue();
    }
    private void UpdateValue()
    {
        _electionPercentage.text = (int)(100 - _electionSlider.value) + "  %";
        _resultPercentage.text = (int)_resultSlider.value + "  %";


        if (_startingElection)
        {
            float targetElection = Mathf.MoveTowards(_resultSlider.value, _votingResult, Time.deltaTime * _votingSpeed);

            //_resultSlider.value = Mathf.Clamp(_resultSlider.value, 0, random);

            _resultSlider.value = targetElection;
            _resultImager.color = _resultGradient.Evaluate( _resultSlider.normalizedValue);

            _resultPercentage.text = (int)_resultSlider.value + "  %";

            _votingSoundCountdown -= Time.deltaTime;
            if (_votingSoundCountdown <= 0)
            {
                VotingSound(SoundManager.Instance.electionVoting);
                _votingSoundCountdown = _votingSound;
            }
            if (_resultSlider.value == (_votingResult) || _resultSlider.value >= 99)
            {
                _startingElection = false;
                Debug.Log("You need at least " + (100-ElectionCalculate) + " to pass the election. You got  " + _votingResult);

                _isElectionCompleted = true;
                _useMoneyButton.SetActive(true);
                _closeButton.SetActive(true);

                _votingSoundCountdown = _votingSound;
                VotingSound(SoundManager.Instance.afterVoting);
            }
        }

        if (GameManager.Instance.money >= _cheatingPrice)
        {
            _useMoneyButton.GetComponent<Button>().interactable = true;
        }
        else
        {
            _useMoneyButton.GetComponent<Button>().interactable = false;
        }
    }
    private void VotingSound(AudioClip clip)
    {
        _sfxSource.PlayOneShot(clip);
    }
    public void EnterElection()
    {
        _governmentData = _countryGovernment.governmentData;

        if (_governmentData != null && _governmentData.hasElection == true)
        {
            _isElectionCompleted = false;

            _resultSlider.value = 0;
            _electionUIIsOpen = true;
            OpenCloseElectionUIButton();

            _electionSlider.value = (int)ElectionCalculate;
            _electionImager.color = _electionGradient.Evaluate(_electionSlider.normalizedValue);
            _electionPercentage.text = (100 - _electionSlider.value) + "  %";

            PriceToBribe();

            _useMoneyButton.SetActive(true);
            _startElectionButton.SetActive(true);
            _closeButton.SetActive(false);
        }
    }
    public void StartElection()
    {
        VotingSound(SoundManager.Instance.afterVoting);
        float POValue = GameManager.Instance.publicOpinionValue;
        float bonusVotingValue = POValue / _economyData.votingResultDivider;
        _votingResult = Random.Range(_economyData.minimumElectionValue, _economyData.maximumElectionValue - bonusVotingValue);
        //Debug.Log("Voting Result = " + _votingResult);
        _votingResult += bonusVotingValue;
        //Debug.Log("New Voting Result = " + _votingResult);

        _startingElection = true;

        _isElectionCompleted = true;

        _useMoneyButton.SetActive(false);
    }
    public void CloseElectionTab()
    {
        _resultPercentage.text = "RESULT";

        _electionUIIsOpen = false;
        OpenCloseElectionUIButton();
    }
    public void UseMoney()
    {
        switch (GameManager.Instance.money >= _cheatingPrice)
        {
            case true:
                VotingSound(SoundManager.Instance.afterVoting);
                GameManager.Instance.money -= _cheatingPrice;
                int moneyToPay = (int)(_cheatingPrice * _economyData.votingCheatMoneyMultiplier);

                _cheatingPrice =  moneyToPay;
                _useMoneyText.text = _cheatingPrice.ToString();

                switch (_isElectionCompleted)
                {
                    case true:
                        AfterElectionCheat();
                        break;

                    case false:
                        NewCheatElectionValue();
                        break;
                }
                break;
            case false:

                break;
        }

    }

    private void PriceToBribe()
    {
        _cheatingPrice = GameManager.Instance.money;
        int moneyToPay = (int)(_cheatingPrice * _economyData.votingCheatMoneyPercent);
        _cheatingPrice = moneyToPay;

        _useMoneyText.text = _cheatingPrice.ToString();
    }

    private void OpenCloseElectionUIButton()
    {
        switch (_electionUIIsOpen)
        {
            case true:
                _cameraController.state = CameraController.cameraState.UNMOVEABLE;
                _calenderTimer.changeSpeed(0);
                _calenderTimer.canEditTime = false;

                _electionContent.SetActive(true);
                break;
            case false:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(1);
                _calenderTimer.canEditTime = true;

                _electionContent.SetActive(false);

                ElectionResultReport();
                break;
        }
    }

    private float ElectionCalculate
    {
        get
        {
            float POValue = GameManager.Instance.publicOpinionValue;
            float ECOValue = GameManager.Instance.economyValue;
            float ElectionBonus = GameManager.Instance.electionStackupChance;
            float statElectionBonus = _countryEconomy.CalculateElectionStatChance;
            Debug.Log("Election Bonus " + ElectionBonus);
            Debug.Log("Stat Election Bonus " + statElectionBonus);
            float BaseChance = _economyData.electionBaseChance;
            float Divider = _economyData.electionDivider;

            float playerChance = BaseChance * ((POValue + ECOValue) / Divider);
            Debug.Log("Election Chance = " + playerChance);
            playerChance = playerChance + (playerChance * ElectionBonus) + (playerChance * statElectionBonus); // Election chance calculate here
            //float addition = Random.Range(0, (100-playerChance)/2);
            //playerChance = playerChance + addition;

            if (playerChance <= 40)
            {
                float bonusValue = Random.Range( 0, playerChance/2);
                playerChance += bonusValue;
            }
            if (playerChance >= 95)
            {
                playerChance = 94;
            }

            Debug.Log("New Election Chance = " + playerChance);

            return playerChance;
        }
    }

    private void NewCheatElectionValue()
    {
        float newElectionValue = 100 - _electionSlider.value;
        _electionSlider.value = Random.Range(0, newElectionValue * _economyData.beforeVotingDivider) + _electionSlider.value;
        if (_electionSlider.value >= 99)
        {
            _electionSlider.value = 99;
        }

        _electionImager.color = _electionGradient.Evaluate(_electionSlider.normalizedValue);


        _electionPercentage.text = (int)(100 - _electionSlider.value) + "  %";
    }
    private void NewCheatResultValue()
    {
        float newResultValue = 100 - _resultSlider.value;

        _resultSlider.value = Random.Range(0, newResultValue*_economyData.afterVotingDivider) + _resultSlider.value;
        if (_resultSlider.value >= 99)
        {
            _resultSlider.value = 99;
        }

        _resultImager.color = _resultGradient.Evaluate(_resultSlider.normalizedValue);

        _resultPercentage.text = (int)_resultSlider.value + "  %";
    }
    private void AfterElectionCheat()
    {
        float randomFloat = Random.value;
        int cheat = 0;
        if (randomFloat > 0.5)
        {
            cheat = 1;
        }
        else
        {
            cheat = 2;
        }

        Debug.Log(cheat);
        switch (cheat)
        {
            case 1:
                NewCheatElectionValue();
                //NewCheatResultValue();
                break;

            case 2:
                NewCheatResultValue();
                break;
        }
    }

    private void ElectionResultReport()
    {
        _electionPercentage.text = (100 - _electionSlider.value) + "  %";
        _resultPercentage.text = _resultSlider.value + "  %";

        switch (_resultSlider.value >= (100 - _electionSlider.value))
        {
            case true:
                Debug.Log("You win");

                // Reset Election Chance
                float currentElectionChance = GameManager.Instance.electionStackupChance;
                GameManager.Instance.AddElectionBonus(-currentElectionChance);
                break;

            case false:
                Debug.Log("You Lose");
                _winLoseManager.GameOver(_winLoseManager.loseDescriptionVoting);
                break;
        }
    }


    public void DebugCheatingVoting()
    {
        float newResultValue = 99;

        _resultSlider.value = newResultValue;

        _resultImager.color = _resultGradient.Evaluate(_resultSlider.normalizedValue);

        _resultPercentage.text = _resultSlider.value + "  %Debug";
    }

}
