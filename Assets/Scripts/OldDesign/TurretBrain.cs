using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretBrain : MonoBehaviour
{
    TurretClass turret;
    public enum turretState { IDLE, ROTATE, SHOOT };
    public turretState state = turretState.IDLE;

    [Header("Turret Setting")]
    public string turretName;
    public float turretHealth;
    public int BaseDamage;
    private TurretController _TurretController;

    [Header("Faction")]
    public bool isEnemy;

    void Start()
    {
        turret = new TurretClass(turretName, turretHealth, BaseDamage, isEnemy);
        _TurretController = GetComponent<TurretController>();
    }
    void Update()
    {
        switch (state)
        {
            case turretState.IDLE:
                
                break;
            case turretState.ROTATE:

                break;
            case turretState.SHOOT:

                break;
        }
    }
}
