using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    [Header("Raycast")]
    public GameObject turretRayStart;
    public float turretRayDistance = 5f;
    public float radiusView = 5f;
    public float radiusCheckDelay = 0.25f;
    private GameObject target;

    private void targetDetection()
    {
        Collider2D collision = Physics2D.OverlapCircle(transform.position, radiusView);
        if (collision != null)
        {
            GameObject hitGameObject = collision.gameObject;
            if (hitGameObject.GetComponent<TurretClass>() != null)
            {
                target = hitGameObject;
            }
            else if (hitGameObject.GetComponent<ShipClass>() != null)
            {
                target = hitGameObject;
            }
        }
    }

    public IEnumerator targetDetectionCoroutine()
    {
        yield return new WaitForSeconds(radiusCheckDelay);
        targetDetection();

    }
}
