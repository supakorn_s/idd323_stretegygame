using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBrain : MonoBehaviour
{
    [HideInInspector] public PlayerInputManager _inputManager;
    ShipClass testing;
    public enum shipState { MOVEABLE, UNMOVEABLE };
    public shipState state = shipState.MOVEABLE;

    [Header("Ship Setting")]
    [SerializeField] private string shipName;
    [SerializeField] private float hitPoint;
    [SerializeField] private int weaponSlot;
    private bool isSelected = false;

    void Start()
    {
        testing = new ShipClass(shipName, hitPoint, weaponSlot);
    }

    void Update()
    {
        if (isSelected)
        {
            state = shipState.MOVEABLE;
        }
        else
        {
            state = shipState.UNMOVEABLE;
        }
        switch (state)
        {
            case shipState.MOVEABLE:
                testing.moving();
                break;
            case shipState.UNMOVEABLE:

                break;
        }
    }
}
