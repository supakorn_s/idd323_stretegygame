using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> _TutorialGameObject;
    [SerializeField] private int _activeTutorialUI = 0;

    [SerializeField] private CalenderTimer _calenderTimer;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private CardEventManager _cardEventManager;
    [SerializeField] private WinLoseManager _winLoseManager;
    [SerializeField] private GameObject _raycastBlocker;

    [SerializeField] private GameObject _countryStatBarTab;
    [SerializeField] private GameObject _CalenderTab;
    [SerializeField] private GameObject _FundTab;
    [SerializeField] private GameObject _rankingButton;
    [SerializeField] private GameObject _upgradeButton;

    [Header("Events")]
    [SerializeField] private GameObject _UpgradeContent;

    private GameManager _manager;

    private void Start()
    {
        _manager = GameManager.Instance;

        _activeTutorialUI = 0;
        _raycastBlocker.SetActive(true);
    }
    private void Update()
    {
        SimpleTutorial();
    }


    private void SimpleTutorial()
    {
        if (PlayerInputManager.Instance.tutorialNext() && !PlayerInputManager.Instance.EscapePress())
        {
            if (_activeTutorialUI != 6 && _activeTutorialUI != 9 && _activeTutorialUI != 12 
                && _activeTutorialUI != 13 && _activeTutorialUI != 14 && _activeTutorialUI != 17
                && _activeTutorialUI != 19 && _activeTutorialUI != 20 && _activeTutorialUI != 24)
            {
                _activeTutorialUI += 1;
                SoundManager.Instance.OnClickUI();
            }
        }

        switch (_activeTutorialUI)
        {
            case 0:
                StopPlayerMovement();

                _countryStatBarTab.SetActive(false);
                _CalenderTab.SetActive(false);
                _FundTab.SetActive(false);
                _rankingButton.SetActive(false);
                _upgradeButton.SetActive(false);
                break;
            case 2:
                StopPlayerMovement();
                _countryStatBarTab.SetActive(true);
                break;
            case 3:
                StopPlayerMovement();
                _CalenderTab.SetActive(true);
                break;
            case 4:
                StopPlayerMovement();
                _FundTab.SetActive(true);
                break;
            case 6:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _raycastBlocker.SetActive(false);
                PlaySession1();
                break;
            case 7:
                StopPlayerMovement();
                _raycastBlocker.SetActive(true);
                break;
            case 8:
                StopPlayerMovement();
                _upgradeButton.SetActive(true);
                break;
            case 9:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(0);
                _raycastBlocker.SetActive(false);
                break;
            case 10:
                StopPlayerMovement();
                _raycastBlocker.SetActive(true);
                break;
            case 12:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _raycastBlocker.SetActive(false);
                _upgradeButton.SetActive(false);
                _UpgradeContent.SetActive(false);
                PlaySession3();
                break;
            case 13:
                StopPlayerMovement();
                _upgradeButton.SetActive(true);
                _UpgradeContent.SetActive(true);
                _raycastBlocker.SetActive(false);
                break;
            case 14:
                StopPlayerMovement();
                _raycastBlocker.SetActive(false);
                break;
            case 16:
                StopPlayerMovement();
                _cardEventManager.CallEvent();
                break;
            case 17:
                _raycastBlocker.SetActive(false);
                break;
            case 19:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(0);
                _rankingButton.SetActive(true);
                _raycastBlocker.SetActive(false);
                break;
            case 20:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(0);
                _raycastBlocker.SetActive(false);
                break;

            case 23:
                _winLoseManager.WinGame();
                _activeTutorialUI = 24;
                break;
            case 24:
                _raycastBlocker.SetActive(false);
                break;

            default:
                StopPlayerMovement();
                break;
        }

        for (int i = 0; i < _TutorialGameObject.Count; i++)
        {
            if (i == _activeTutorialUI)
            {
                _TutorialGameObject[i].SetActive(true);
            }
            else
            {
                _TutorialGameObject[i].SetActive(false);
            }
        }
    }
    private void PlaySession1()
    {
        if (_activeTutorialUI == 6)
        {
            if (GameManager.Instance.months > 2)
            {
                _activeTutorialUI = 7;
                SoundManager.Instance.OnClickUI();
            }
        }
    }   // 2 months
    public void PlaySession2()  // Open a shop, attach to a shop button
    {
        if (_activeTutorialUI == 9)
        {
            _activeTutorialUI = 10;
            SoundManager.Instance.OnClickUI();
        }
    }
    private void PlaySession3()
    {
        if (_activeTutorialUI == 12)
        {
            if (GameManager.Instance.money > 7500000)
            {
                _activeTutorialUI = 13;
                SoundManager.Instance.OnClickUI();
            }
        }
    }
    public void PlaySession4()
    {
        if (_activeTutorialUI == 13)
        {
            _activeTutorialUI = 14;
            SoundManager.Instance.OnClickUI();
        }
    }
    public void PlaySession5()
    {
        if (_activeTutorialUI == 14)
        {
            _activeTutorialUI = 15;
            SoundManager.Instance.OnClickUI();
        }
    }
    public void PlaySessionRanking()
    {
        if (_activeTutorialUI == 19)
        {
            _activeTutorialUI = 20;
            SoundManager.Instance.OnClickUI();
        }
        else if (_activeTutorialUI == 20)
        {
            _activeTutorialUI = 21;
            SoundManager.Instance.OnClickUI();
        }
    }
    public void PlaySession6()
    {
        if (_activeTutorialUI == 17)
        {
            _activeTutorialUI = 18;
            SoundManager.Instance.OnClickUI();
        }
    }
    private void StopPlayerMovement()
    {
        _calenderTimer.changeSpeed(0);
        _cameraController.state = CameraController.cameraState.UNMOVEABLE;
        _raycastBlocker.SetActive(true);
    }
}