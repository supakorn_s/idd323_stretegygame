using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GovernmentBrain : MonoBehaviour
{

    [Header("Reference")]
    [SerializeField] private CountryGovernment _CountryGovernment;
    [SerializeField] private GovernmentData _governmentData;
    [SerializeField] private Image _governmentSprite;

    [SerializeField] private TextMeshProUGUI _systemNameText;
    [SerializeField] private TextMeshProUGUI _StartingFundText;
    [SerializeField] private TextMeshProUGUI _publicOpinionText;
    [SerializeField] private TextMeshProUGUI _economyStrengthText;
    [SerializeField] private TextMeshProUGUI _militaryStrengthText;
    [SerializeField] private TextMeshProUGUI _electionText;
    //private TextMeshProUGUI[] decideColorValue;

    [SerializeField] private Color _normalColor = Color.white;
    [SerializeField] private Color _buffColor = Color.green;
    [SerializeField] private Color _debuffColor = Color.red;

    private UpgradeUI _upgradeUI;

    private void Start()
    {
        startCallOut();
    }

    private void startCallOut()
    {
        _governmentSprite.sprite = _governmentData.governmentSprite;
        _systemNameText.text = _governmentData.systemName;
        _upgradeUI = FindObjectOfType<UpgradeUI>();

        List<float> decideColorValue = new List<float>{_governmentData.publicOpinionValue,
            _governmentData.economyStrengthValue, _governmentData.militaryStrengthValue};
        List<TextMeshProUGUI> decideColorText = new List<TextMeshProUGUI>{_publicOpinionText,
            _economyStrengthText, _militaryStrengthText};

        _StartingFundText.text = _governmentData.StartingFund.ToString("#,#");
        _publicOpinionText.text = _governmentData.publicOpinionValue.ToString();
        _economyStrengthText.text = _governmentData.economyStrengthValue.ToString();
        _militaryStrengthText.text = _governmentData.militaryStrengthValue.ToString();

        for (int i = 0; i < decideColorValue.Count; i++)
        {
            if (decideColorValue[i] > 0)
            {
                decideColorText[i].color = _buffColor;
            }
            if (decideColorValue[i] < 0)
            {
                decideColorText[i].color = _debuffColor;
            }
            if (decideColorValue[i] == 0)
            {
                decideColorText[i].color = _normalColor;
            }
        }

        switch (_governmentData.hasElection)
        {
            case true:
                _electionText.text = "YES";
                break;
            case false:
                _electionText.text = "NO";
                break;
        }

        }

    public void selectSystem()
    {
        _CountryGovernment.changeGovernment(_governmentData);
        _upgradeUI.governmentTab.SetActive(false);
        //_upgradeUI.upgradeContentTab.SetActive(true);
        _upgradeUI.OpenCloseUpgradeUIButton();
    }
}
