using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretClass
{
    private string _Name;
    private float _health;
    private int _baseDamage;
    private bool _isEnemy;

    private GameObject target;

    public string turretName => _Name;

    public TurretClass(string tName, float tHealth , int baseDamage, bool isEnemy)
    {
        _Name = tName;
        _health = tHealth;
        _baseDamage = baseDamage;
        _isEnemy = isEnemy;
    }

    private void targetDetection(GameObject origin, float radiousView)
    {
        Collider2D collision = Physics2D.OverlapCircle(origin.transform.position, radiousView);
        if (collision != null)
        {
            GameObject hitGameObject = collision.gameObject;
            if (hitGameObject.GetComponent<TurretClass>() != null)
            {
                target = hitGameObject;
            }
            else if (hitGameObject.GetComponent<ShipClass>() != null)
            {
                target = hitGameObject;
            }
        }
        else
        {
            
        }
    }
    public IEnumerator targetDetectionCoroutine(float delay, GameObject origin, float radiousView)
    {
        yield return new WaitForSeconds(delay);
        targetDetection(origin, radiousView);
        
    }
    public void turretTargeting(GameObject rayStart, float rayDistance)
    {
        RaycastHit2D hitTarget = Physics2D.Raycast(rayStart.transform.position, rayStart.transform.right, rayDistance);
        if (hitTarget.collider != null)
        {
            GameObject hitGameObject = hitTarget.collider.gameObject;
            if (hitGameObject.GetComponent<TurretClass>() != null)
            {

            }
            else if (hitGameObject.GetComponent<ShipClass>() != null)
            {

            }
            Debug.Log(hitTarget);
        }
    }
}
