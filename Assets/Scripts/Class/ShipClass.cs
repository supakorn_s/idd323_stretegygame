using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipClass
{
    [HideInInspector] public PlayerInputManager _inputManager;

    private string _name;
    private float _health;
    private int _weaponSlot;

    public string name => _name;

    public ShipClass(string sName,float sHealth, int sWeaponSlot)
    {
        _inputManager = PlayerInputManager.Instance;

        _name = sName;
        _health = sHealth;
        _weaponSlot = sWeaponSlot;
    }
    
    public void moving()
    {
        
    }
}
