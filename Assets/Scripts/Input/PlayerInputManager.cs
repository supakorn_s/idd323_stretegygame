using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputManager : MonoBehaviour
{
    private static PlayerInputManager _instance;

    public Input_master _inputMaster;

    public static PlayerInputManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _inputMaster = new Input_master();
    }

    private void OnEnable()
    {
        _inputMaster.Enable();
    }
    private void OnDisable()
    {
        _inputMaster.Disable();
    }

    public Vector2 GetCamMovement()
    {
        return _inputMaster.Player.CameraMovement.ReadValue<Vector2>();
    }
    public Vector2 GetMouseDelta()
    {
        return _inputMaster.Player.MouseDelta.ReadValue<Vector2>();
    }
    public Vector2 GetMousePosition()
    {
        return _inputMaster.Player.MousePosition.ReadValue<Vector2>();
    }
    public Vector2 GetMouseScroll()
    {
        return _inputMaster.Player.MouseScroll.ReadValue<Vector2>();
    }
    public bool LeftClick()
    {
        return _inputMaster.Player.LeftClick.triggered;
    }
    public bool RightClick()
    {
        return _inputMaster.Player.RightClick.triggered;
    }
    public bool EscapePress()
    {
        return _inputMaster.Player.Escape.triggered;
    }
    public bool SpaceBarPress()
    {
        return _inputMaster.Player.Spacebar.triggered;
    }
    public bool tutorialNext()
    {
        return _inputMaster.Player.TutorialNext.triggered;
    }
    public bool CheatPress()
    {
        return _inputMaster.Player.F1Cheat.triggered;
    }
}
