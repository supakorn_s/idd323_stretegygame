using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistrictClass
{
    [HideInInspector] public PlayerInputManager _inputManager;

    private GameObject _districtPrefab;
    private SpriteRenderer _renderer;
    private Sprite _districtImage;

    private void start()
    {
        _renderer.sprite = _districtImage;

        _inputManager = PlayerInputManager.Instance;
    }

    public DistrictClass(GameObject districtPrefab, SpriteRenderer spriteRenderer, Sprite districtImage)
    {
        _districtPrefab = districtPrefab;
        _renderer = spriteRenderer;
        _districtImage = districtImage;
    }
}
