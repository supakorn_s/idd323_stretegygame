using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class DistrictBrain : MonoBehaviour
{

    [HideInInspector] public DistrictData _districtData;
    [HideInInspector] public SpriteRenderer sRenderer;
    private CountryEconomy _countryEconomy;

    [SerializeField] private GameObject _informationObject;
    [SerializeField] private TextMeshPro _informationText;

    [TextArea] [SerializeField] private string _badDText;
    [TextArea] [SerializeField] private string _ComDText;
    [TextArea] [SerializeField] private string _HarDText;
    [TextArea] [SerializeField] private string _IndDText;
    [TextArea] [SerializeField] private string _ResDText;
    private bool _playSound = false;

    private void Start()
    {
        sRenderer.sprite = _districtData.districtImage;
        name = _districtData.districtName;

        _countryEconomy = FindObjectOfType<CountryEconomy>();

        districtEconomyToCountryEconomy();
        districtStatToGameManager();

        switch (_districtData.districtName)
        {
            case "Bad District":
                _informationText.text = _badDText;
                break;
            case "Commercial District":
                _informationText.text = _ComDText;
                break;
            case "Harbour District":
                _informationText.text = _HarDText;
                break;
            case "Industry District":
                _informationText.text = _IndDText;
                break;
            case "Residential District":
                _informationText.text = _ResDText;
                break;
        }
    }

    private void Update()
    {
        regionHovering();
    }

    public void districtEconomyToCountryEconomy()
    {
        if (_districtData.moneyBonus != 0) _countryEconomy.incomeValueList.Add(_districtData.moneyBonus);
        //if (_districtData.publicOpinionBonus != 0) _countryEconomy.publicOpinionValueList.Add(_districtData.publicOpinionBonus);
        //if (_districtData.economyBonus != 0) _countryEconomy.economyBonusList.Add(_districtData.economyBonus);
        //if (_districtData.militaryBonus != 0) _countryEconomy.MilitaryBonusList.Add(_districtData.militaryBonus);

        if (_districtData.moneyPercent != 0) _countryEconomy.incomePercentList.Add(_districtData.moneyPercent);
        if (_districtData.publicOpinionPercent != 0) _countryEconomy.publicPercentList.Add(_districtData.publicOpinionPercent);
        if (_districtData.economyPercent != 0) _countryEconomy.economyPercentList.Add(_districtData.economyPercent);
        if (_districtData.militaryPercent != 0) _countryEconomy.MilitaryPercentList.Add(_districtData.militaryPercent);
    }

    public void districtStatToGameManager()
    {
        GameManager.Instance.addDataToCountryStat(_districtData.publicOpinionBonus, 
            _districtData.economyBonus, _districtData.militaryBonus);
    }


    private void regionHovering()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(PlayerInputManager.Instance.GetMousePosition());
        RaycastHit2D hitObject = Physics2D.Raycast(mousePosition, Vector2.zero, 0f);
        if (hitObject.collider != null && hitObject.collider.gameObject == this.gameObject)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                _informationObject.SetActive(true);
                sRenderer.color = Color.yellow;
                PlayHoverSound();
            }
            else
            {
                _informationObject.SetActive(false);
                sRenderer.color = Color.white;
                _playSound = false;
            }
        }
        else
        {
            _informationObject.SetActive(false);
            sRenderer.color = Color.white;
            _playSound = false;
        }
    }

    public void PlayHoverSound()
    {
        if (_playSound == false)
        {
            SoundManager.Instance.OnHoverUI();
            _playSound = true;
        }
    }
}
