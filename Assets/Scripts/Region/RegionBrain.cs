using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RegionBrain : MonoBehaviour
{
    [HideInInspector] public PlayerInputManager _inputManager;

    [SerializeField] private Color originalColor = Color.white;
    [SerializeField] private Color highlightColor;

    private SpriteRenderer sr;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _inputManager = PlayerInputManager.Instance;
    }

    private void Update()
    {
        regionHovering();
    }

    private void regionHovering()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(_inputManager.GetMousePosition());
        RaycastHit2D hitObject = Physics2D.Raycast(mousePosition, Vector2.zero, 0f);
        if (hitObject.collider != null && hitObject.collider.gameObject == this.gameObject)
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                changeHighlightColor();
            }
            else
            {
                resetColor();
            }
        }
        else
        {
            resetColor();
        }
    }

    public void changeHighlightColor()
    {
        sr.color = highlightColor;
    }
    public void resetColor()
    {
        sr.color = originalColor;
    }

}
