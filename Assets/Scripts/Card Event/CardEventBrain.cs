using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class CardEventBrain : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private CardEventData _cardEventData;
    [SerializeField] private Animator _animator;
    [SerializeField] private string _openTrigger = "OpenCard";
    private CardEventManager _cardEventManager;
    private CountryUpgrade _countryUpgrade;
    private bool _isShowingFront = false;
    private bool _isSelected = false;

    [Header("Card")]
    [SerializeField] private TextMeshProUGUI _cardName;
    [SerializeField] private TextMeshProUGUI _cardDescription;
    [SerializeField] private TextMeshProUGUI _cardEffect;
    [SerializeField] private Image _backCard;
    [SerializeField] private Image _frontCard;
    [SerializeField] private Color _originalColor = Color.white;
    [SerializeField] private Color _highlightColor = Color.yellow;

    public Image backCard => _backCard;
    public Image frontCard => _frontCard;

    [Header("Audio")]
    [SerializeField] private AudioSource _sfxSource;

    void Start()
    {
        _cardEventManager = FindObjectOfType<CardEventManager>();
        _countryUpgrade = FindObjectOfType<CountryUpgrade>();

        _cardName.text = _cardEventData.cardName;
        _cardDescription.text = _cardEventData.cardDescription;
        _cardEffect.text = _cardEventData.effectDescription;
    }
    private void Update()
    {
        PlayAnimation();
    }

    void PlayAnimation()
    {
        if (_isSelected && _isShowingFront == false && _cardEventManager.alreadySelectCard == false)
        {
            if (PlayerInputManager.Instance.LeftClick())
            {
                _animator.SetTrigger(_openTrigger);
                _isShowingFront = true;

                _cardEventManager.SelectedCard();

                _countryUpgrade.insertNewEventData(_cardEventData.statEffectData);
                if (_cardEventData.subsStatEffectData != null) _countryUpgrade.insertNewEventData(_cardEventData.subsStatEffectData);
            }
        }

    }
    public void setCardEventData(CardEventData newEvent)
    {
        _cardEventData = newEvent;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_cardEventManager.alreadySelectCard == false)
        {
            _isSelected = true;
            _backCard.color = _highlightColor;
        }
        else
        {
            _isSelected = false;
            _backCard.color = _originalColor;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _isSelected = false;
        _backCard.color = _originalColor;
    }

    public void OnHoverUI()
    {
        _sfxSource.PlayOneShot(SoundManager.Instance.hovering);
    }
    public void OnClickUI()
    {
        _sfxSource.PlayOneShot(SoundManager.Instance.clicking);
    }
}
