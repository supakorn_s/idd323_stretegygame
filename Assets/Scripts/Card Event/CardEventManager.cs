using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardEventManager : MonoBehaviour
{
    
    [SerializeField] private CountryBrain _countryBrain;
    [SerializeField] private GameManager _gameManager;
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private CalenderTimer _calenderTimer;
    [SerializeField] private GameObject _eventContent;
    [SerializeField] private GameObject _cardCanvasChild;
    [SerializeField] private GameObject _cardPrefab;
    [SerializeField] private GameObject _returnButton;
    [SerializeField] private int _totalSpawnCard = 3;


    [SerializeField] private int _eventEvery;
    [SerializeField] private float _dayToEvent = 0;
    [SerializeField] private List<GameObject> _spawnedCard;  // DEBUG [serializefield]

    [Header("Card Data")]
    [SerializeField] private List<CardEventData> _goodPublicEvent;
    [SerializeField] private List<CardEventData> _badPublicEvent;
    [SerializeField] private List<CardEventData> _goodEconomyEvent;
    [SerializeField] private List<CardEventData> _badEconomyEvent;
    [SerializeField] private List<CardEventData> _goodMilitaryEvent;
    [SerializeField] private List<CardEventData> _badMilitaryEvent;

    private bool _alreadySelectCard = false;
    private bool _eventContentIsOpen = false;
    public bool alreadySelectCard => _alreadySelectCard;


    private void Start()
    {
        if (_countryBrain == null) _countryBrain = FindObjectOfType<CountryBrain>();
        if (_gameManager == null) _gameManager = FindObjectOfType<GameManager>();
        if (_gameManager == null) _cameraController = FindObjectOfType<CameraController>();
        if (_gameManager == null) _calenderTimer = FindObjectOfType<CalenderTimer>();
        _returnButton.SetActive(false);
        _eventEvery = GameManager.Instance.economyData.cardEventEvery;

        _eventContent.SetActive(false);

    }
    private void Update()
    {
        if (Mathf.FloorToInt(_dayToEvent) >= _eventEvery)
        {
            CallEvent();
        }
    }

    private void OpenCloseEventUIButton()
    {
        switch (_eventContentIsOpen)
        {
            case true:
                _cameraController.state = CameraController.cameraState.UNMOVEABLE;
                _calenderTimer.changeSpeed(0);
                _calenderTimer.canEditTime = false;
                break;
            case false:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(1);
                _calenderTimer.canEditTime = true;
                break;
        }
    }

    public void spawnCard()
    {
        float _poValue = _gameManager.publicOpinionValue;
        float _ecoValue = _gameManager.economyValue;
        float _milValue = _gameManager.militaryValue;

        for (int i = 0; i < _totalSpawnCard; i++)
        {
            designatedCardType();
        }
    }
    void startCardSpawn(List<CardEventData> cardListToSpawn)
    {
        GameObject card = Instantiate(_cardPrefab, _cardCanvasChild.transform);
        card.transform.localPosition = new Vector3(0, 0, 0);
        CardEventBrain _cardEventBrain = card.GetComponent<CardEventBrain>();

        int rad = Random.Range(0, cardListToSpawn.Count);
        _cardEventBrain.setCardEventData(cardListToSpawn[rad]);

        _spawnedCard.Add(card);
    }

    private int designatedGoodBadType   // Random Good or bad card
    {
        get
        {   // 1 = Good, 2 = Bad, 3 = Mixed or Both
            int goodBadType;
            float randomValue = Random.value;
            float cardEventValue = _countryBrain.countryEconomy.CalculateCardEventStatChance;
            float goodValue = _countryBrain._economyData.goodEvent;
            float badValue = goodValue + _countryBrain._economyData.badEvent;

            // The less randomValue, the better chance of getting good card.
            randomValue = Mathf.Clamp(randomValue - (randomValue * cardEventValue), 0.01f, 0.99f);

            if (randomValue <= goodValue)
            {
                goodBadType = 1;
                return goodBadType;
            }
            else
            {
                if (randomValue <= badValue)
                {
                    goodBadType = 2;
                    return goodBadType;
                }
                else
                {
                    goodBadType = 3;
                    return goodBadType;
                }
            }
        }
    }
    private void designatedCardType()  // Random type of card
    {
        // 1 = Good, 2 = Bad, 3 = Mixed or Both
        float randomValue = Random.value;

        float _poValue = _gameManager.publicOpinionValue;
        float _ecoValue = _gameManager.economyValue;
        float _milValue = _gameManager.militaryValue;
        //Debug.Log("Public " + _poValue);
        //Debug.Log("Eco " + _ecoValue);
        //Debug.Log("Military " + _milValue);

        // Find what is the total value in percent
        float findPercent = (100 / (_poValue + _ecoValue + _milValue));
        // This value is the percent from 100 in 0-1 value
        _poValue = ((_poValue * findPercent) / 100);
        _ecoValue = ((_ecoValue * findPercent) / 100);
        _milValue = ((_milValue * findPercent) / 100);
        //Debug.Log("Public2 " + _poValue);
        //Debug.Log("Eco2 " + _ecoValue);
        //Debug.Log("Military2 " + _milValue);


        switch (designatedGoodBadType)
        {
            case 1:
                if (Random.value <= _poValue)
                {
                    startCardSpawn(_goodPublicEvent);
                    //startCardSpawn
                }
                else if (Random.value <= _poValue + _ecoValue)
                {
                    startCardSpawn(_goodEconomyEvent);
                }
                else if (Random.value <= _poValue + _ecoValue + _milValue)
                {
                    startCardSpawn(_goodMilitaryEvent);
                }
                break;
            case 2:
                if (Random.value <= _poValue)
                {
                    startCardSpawn(_badPublicEvent);
                    //startCardSpawn
                }
                else if (Random.value <= _poValue + _ecoValue)
                {
                    startCardSpawn(_badEconomyEvent);
                }
                else if (Random.value <= _poValue + _ecoValue + _milValue)
                {
                    startCardSpawn(_badMilitaryEvent);
                }
                break;
            case 3:
                float goodBadValue = Random.Range(1,2);
                switch (goodBadValue)
                {
                    case 1:
                        if (Random.value <= _poValue)
                        {
                            startCardSpawn(_goodPublicEvent);
                            //startCardSpawn
                        }
                        else if (Random.value <= _poValue + _ecoValue)
                        {
                            startCardSpawn(_goodEconomyEvent);
                        }
                        else if (Random.value <= _poValue + _ecoValue + _milValue)
                        {
                            startCardSpawn(_goodMilitaryEvent);
                        }
                        break;
                    case 2:
                        if (Random.value <= _poValue)
                        {
                            startCardSpawn(_badPublicEvent);
                            //startCardSpawn
                        }
                        else if (Random.value <= _poValue + _ecoValue)
                        {
                            startCardSpawn(_badEconomyEvent);
                        }
                        else if (Random.value <= _poValue + _ecoValue + _milValue)
                        {
                            startCardSpawn(_badMilitaryEvent);
                        }
                        break;
                }

                break;
        }
    }

    public void SelectedCard()
    {
        _returnButton.SetActive(true);
        _alreadySelectCard = true;
    }
    public void ClosedEvent()
    {
        for (int i = 0; i < _spawnedCard.Count; i++)
        {
            Destroy(_spawnedCard[i]);
        }
        _spawnedCard = new List<GameObject>();

        _eventContentIsOpen = false;
        OpenCloseEventUIButton();

        _alreadySelectCard = false;
        _returnButton.SetActive(false);
        _eventContent.gameObject.SetActive(false);
    }
    public void AddDayToEvent(int finalSpeed)
    {
        _dayToEvent += Time.deltaTime * finalSpeed;
    }
    public void CallEvent()
    {
        _eventContent.gameObject.SetActive(true);
        _eventContentIsOpen = true;
        _dayToEvent = 0.0f;
        spawnCard();
        OpenCloseEventUIButton();
    }
}
