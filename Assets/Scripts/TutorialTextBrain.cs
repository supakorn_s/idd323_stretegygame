using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialTextBrain : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textbox;

    private void Start()
    {
        _textbox.text = GameManager.Instance.countryName;
    }

    
}
