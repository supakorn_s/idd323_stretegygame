using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance;
    public static SoundManager Instance { get { return _instance; } }


    [Header("Audio Source")]
    [SerializeField] private AudioSource _bgmSource1;
    [SerializeField] private AudioSource _bgmSource2;
    [SerializeField] private AudioSource _sfxSource;

    [Header("BGM Lists")]
    [SerializeField] private List<AudioClip> _bgmMainmenu;
    [SerializeField] private AudioClip _bgmWinning;
    [SerializeField] private AudioClip _bgmLosing;
    public List<AudioClip> bgmMainmenu => _bgmMainmenu;
    public AudioClip bgmWinning => _bgmWinning;
    public AudioClip bgmLosing => _bgmLosing;
    
    [Header("BGM Events Lists")]
    [SerializeField] private List<AudioClip> _bgmDefault;
    [SerializeField] private List<AudioClip> _bgmDemocracy;
    [SerializeField] private List<AudioClip> _bgmSocialism;
    [SerializeField] private List<AudioClip> _bgmMonarchy;
    [SerializeField] private List<AudioClip> _bgmDictator;
    [SerializeField] private List<AudioClip> _bgmCommunism;
    [SerializeField] private List<AudioClip> _currentBGMList;
    public List<AudioClip> bgmDemocracy => _bgmDemocracy;
    public List<AudioClip> bgmSocialism => _bgmSocialism;
    public List<AudioClip> bgmMonarchy => _bgmMonarchy;
    public List<AudioClip> bgmDictator => _bgmDictator;
    public List<AudioClip> bgmCommunism => _bgmCommunism;

    [Header("SFX Lists")]
    [SerializeField] private AudioClip _clicking;
    [SerializeField] private AudioClip _hovering;
    [SerializeField] private AudioClip _popup;
    [SerializeField] private AudioClip _electionVoting;
    [SerializeField] private AudioClip _afterVoting;

    public AudioClip clicking => _clicking;
    public AudioClip hovering => _hovering;
    public AudioClip popup => _popup;
    public AudioClip electionVoting => _electionVoting;
    public AudioClip afterVoting => _afterVoting;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        OnSceneLoaded();
    }
    private void Update()
    {
        CheckIfPlaying();
    }

    private void CheckIfPlaying()
    {
        if (!_bgmSource1.isPlaying && _currentBGMList != null)
        {

            ChooseListBGM(_currentBGMList);

            _bgmSource1.Play();
        }
    }
    private void OnSceneLoaded()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        switch (scene)
        {
            case 0:
                ChooseListBGM(_bgmMainmenu);
                break;
            case 1:
                ChooseListBGM(_bgmDefault);
                break;
            case 2:
                ChooseListBGM(_bgmDefault);
                break;
        }

        _bgmSource1.Play();

    }
    public void ChooseListBGM(List<AudioClip> list)
    {
        _bgmSource1.Stop();

        int randomIndex = RandomClip(list);
        AudioClip bgmToPlay = list[randomIndex];
        _currentBGMList = list;

        _bgmSource1.clip = bgmToPlay;
    }
    public void ChooseClipBGM(AudioClip clips)
    {
        _bgmSource1.Stop();

        _currentBGMList = null;

        _bgmSource1.clip = clips;

        _bgmSource1.Play();
    }
    public void OnHoverUI()
    {
        _sfxSource.PlayOneShot(_hovering);
    }
    public void OnClickUI()
    {
        _sfxSource.PlayOneShot(_clicking);
    }
    public void PlaySFX(AudioClip toPlay)
    {
        _sfxSource.PlayOneShot(toPlay);
    }

    private int RandomClip(List<AudioClip> list)
    {
        int listNumber;
        listNumber = Random.Range(0, list.Count);
        return listNumber;
    }


}
