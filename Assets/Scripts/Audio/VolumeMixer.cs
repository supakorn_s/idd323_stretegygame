using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeMixer : MonoBehaviour
{
    [Header("Volume Control")]
    [SerializeField]
    static VolumeMixer soundForce;
    [SerializeField] private AudioMixer audioMixer;
    [SerializeField] private Slider slider;
    [SerializeField] private string ExposerString;

    [Header("Testing Sound")]
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _testSound;

    void Awake()
    {
        soundForce = this;
        // Set volume to match game data here
        if (!PlayerPrefs.HasKey(ExposerString))
        {
            PlayerPrefs.SetFloat(ExposerString, 1.0f);
        }
        slider.value = PlayerPrefs.GetFloat(ExposerString);
    }

    public void SetVolume(float sliderValue)
    {
        audioMixer.SetFloat(ExposerString, Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat(ExposerString, sliderValue);
    }
    public void PlayEffectOnce()
    {
        _audioSource.PlayOneShot(_testSound);
    }
}
