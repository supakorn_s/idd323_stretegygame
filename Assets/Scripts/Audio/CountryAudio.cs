using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CountryAudio : MonoBehaviour
{
    private static CountryAudio _instance;
    public static CountryAudio Instance { get { return _instance; } }

    [SerializeField] private SoundManager _soundManager;
    [SerializeField] private CountryGovernment _countryGovernment;


    private void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        if (_soundManager == null) _soundManager = FindObjectOfType<SoundManager>();
        if (_countryGovernment == null) _countryGovernment = FindObjectOfType<CountryGovernment>();
    }

    public void ChangeGovernmentAudio()
    {
        if (_soundManager != null && _countryGovernment.governmentData != null)
        {
            switch (_countryGovernment.governmentData.name)
            {
                case "CommunismData":
                    _soundManager.ChooseListBGM(_soundManager.bgmCommunism);
                    break;
                case "DemocracyData":
                    _soundManager.ChooseListBGM(_soundManager.bgmDemocracy);
                    break;
                case "MilitaryData":
                    _soundManager.ChooseListBGM(_soundManager.bgmDictator);
                    break;
                case "MonachyData":
                    _soundManager.ChooseListBGM(_soundManager.bgmMonarchy);
                    break;
                case "SocialismData":
                    _soundManager.ChooseListBGM(_soundManager.bgmSocialism);
                    break;
            }

        }
    }
}
