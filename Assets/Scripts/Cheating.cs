using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Cheating : MonoBehaviour
{
    [SerializeField] private bool _enableCheating = false;

    [SerializeField] private GameObject _cheatPanel;
    [SerializeField] private PauseMenu _pauseMenu;

    [Header("Cheating value")]
    [SerializeField] private int _money = 500000;
    [SerializeField] private TextMeshProUGUI _moneyText;
    [SerializeField] private TextMeshProUGUI _moneyText2;

    [SerializeField] private Slider _POSlider;
    [SerializeField] private TextMeshProUGUI _POSliderText;
    [SerializeField] private Slider _ECOSlider;
    [SerializeField] private TextMeshProUGUI _ECOSliderText;
    [SerializeField] private Slider _MILSlider;
    [SerializeField] private TextMeshProUGUI _MILSliderText;

    [SerializeField] private TMP_InputField _ElectionDayField;
    
    
    private bool _cheatIsOpen = false;

    private void Start()
    {
        _cheatPanel.SetActive(false);

        _moneyText.text = "Money\n+\n" + _money.ToString("#,#") + "$";
        _moneyText2.text = "Money\n+\n" + (_money*2).ToString("#,#") + "$";
    }
    private void Update()
    {
        OpenCloseCheatTab();
    }

    private void OpenCloseCheatTab()
    {
        if (_enableCheating)
        {
            if (_pauseMenu.gameData.isTransitioning == false && !GameManager.Instance.isPaused)
            {
                _POSlider.value = GameManager.Instance.publicOpinionValue;
                _ECOSlider.value = GameManager.Instance.economyValue;
                _MILSlider.value = GameManager.Instance.militaryValue;
                _POSliderText.text = _POSlider.value.ToString();
                _ECOSliderText.text = _ECOSlider.value.ToString();
                _MILSliderText.text = _MILSlider.value.ToString();

                _ElectionDayField.placeholder.GetComponent<TextMeshProUGUI>().text = ((int)GameManager.Instance.electDayIn).ToString();

                if (PlayerInputManager.Instance.CheatPress())
                {
                    switch (_cheatIsOpen)
                    {
                        case true:
                            CloseTab();
                            break;
                        case false:
                            OpenTab();
                            break;
                    }
                }
            }
            else
            {
                CloseTab();
            }
        }
    }
    private void OpenTab()
    {
        _cheatPanel.SetActive(true);
        _cheatIsOpen = true;
    }
    private void CloseTab()
    {
        _cheatPanel.SetActive(false);
        _cheatIsOpen = false;
    }

    //Cheating Tab
    public void CheatMoney()
    {
        GameManager.Instance.money += _money;
    }
    public void CheatMoney2()
    {
        GameManager.Instance.money += (_money * 2);
    }
    public void CheatPublicValue()
    {
        GameManager.Instance.CheatPublicOpinion(_POSlider.value);
        _POSliderText.text = _POSlider.value.ToString();
    }
    public void CheatEconomicValue()
    {
        GameManager.Instance.CheatEconomic(_ECOSlider.value);
        _ECOSliderText.text = _ECOSlider.value.ToString();
    }
    public void CheatMilitaryValue()
    {
        GameManager.Instance.CheatMilitary(_MILSlider.value);
        _MILSliderText.text = _MILSlider.value.ToString();
    }
    public void CheatElectionDay()
    {
        float dayValue = 60;
        string inputField = _ElectionDayField.text;
        float.TryParse(inputField, out dayValue);

        if (dayValue >= -100)
        {
            if (dayValue <= 0)
            {
                dayValue = 0;
            }
            GameManager.Instance.electDayIn = dayValue;
        }

        _ElectionDayField.placeholder.GetComponent<TextMeshProUGUI>().text = GameManager.Instance.electDayIn.ToString();
        _ElectionDayField.text = null;
    }
}
