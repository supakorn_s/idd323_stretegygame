using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New data manager", menuName = "Manager/Economy Data Manager", order = 1)]
public class EconomyData : ScriptableObject
{
    [Header("Starting Stat")]
    [SerializeField] private string _countryName = "South Zhina";
    private string _newCountryName;
    [SerializeField] private int _startingMoney = 5000000;
    [SerializeField] private int _days = 25;
    [SerializeField] private int _months = 1;
    [SerializeField] private int _years = 2021;
    [SerializeField] private int _electionEvery = 360;

    [SerializeField] [Range(0, 100)] private float _publicOpinionValue = 50;
    [SerializeField] [Range(0, 100)] private float _economyValue = 50;
    [SerializeField] [Range(0, 100)] private float _militaryValue = 50;
    [SerializeField] [Range(0, 100)] private float _minimumOrderToLose = 50;

    public string countryName => _countryName;
    public string newCountryName => _newCountryName;
    public int startingMoney => _startingMoney;
    public int days => _days;
    public int months => _months;
    public int years => _years;
    public int electionEvery => _electionEvery;
    public float publicOpinionValue => _publicOpinionValue;
    public float economyValue => _economyValue;
    public float militaryValue => _militaryValue;
    public float minimumOrderToLose => _minimumOrderToLose;



    [Header("Base Calculation")]
    [SerializeField] private int _incomeBase;
    [SerializeField] private float _incomeMultiplying = 0.5f;
    [SerializeField] private int _outcomeBase;
    [SerializeField] private int _endOfTheMonthDate;
    public int incomeBase => _incomeBase;
    public float incomeMultiplying => _incomeMultiplying;
    public int outcomeBase => _outcomeBase;
    public int endOfTheMonthDate => _endOfTheMonthDate;

    [Header("Decay Value Per Month")]
    // In Value per month
    [SerializeField] [Range(-100, 0)] private float _publicOpinionDecayValue;
    [SerializeField] [Range(-100, 0)] private float _economyDecayValue;
    [SerializeField] [Range(-100, 0)] private float _militaryDecayValue;

    public float publicOpinionDecayValue => _publicOpinionDecayValue;
    public float economyDecayValue => _economyDecayValue;
    public float militaryDecayValue => _militaryDecayValue;

    [Header("Stat Effect status in percent")]
    [SerializeField] [Range(0, 100)] private float _veryBad = 25f;
    [SerializeField] [Range(0, 100)] private float _bad = 25f;
    [SerializeField] [Range(0, 100)] private float _normal = 50f;
    [SerializeField] [Range(0, 100)] private float _good = 75f;
    [SerializeField] [Range(0, 100)] private float _veryGood = 75f;
    public float veryBad => _veryBad;
    public float bad => _bad;
    public float normal => _normal;
    public float good => _good;
    public float veryGood => _veryGood;

    [Header("District Rate")]
    [SerializeField] [Range(0, 1)] private float _goodDistrictRate;
    public float goodDistrictRate => _goodDistrictRate;

    [HideInInspector] private int currentMoney;
    [HideInInspector] public int monthlyIncome;
    [HideInInspector] public int monthlyOutcome;

    [Header("Card Event Rate")]
    [SerializeField] private int _cardEventEvery = 60; //day
    [SerializeField] private float _goodEvent = 0.5f; //day
    [SerializeField] private float _badEvent = 0.25f; //day

    public int cardEventEvery => _cardEventEvery;
    public float goodEvent => _goodEvent;
    public float badEvent => _badEvent;

    [Header("Election Rate")]
    // Formular Base chance X [(PO+ECO)/ Election Divider] + Election chance
    [SerializeField] private float _minimumElectionValue = 5; 
    [SerializeField] private float _maximumElectionValue = 80; 
    [SerializeField] private float _electionBaseChance = 70; 
    [SerializeField] private float _electionDivider = 150;
    [SerializeField] private float _votingResultDivider = 5;
    [SerializeField] private float _votingCheatMoneyPercent = 0.4f; // Calculate the amount of money used in cheating
    [SerializeField] private float _votingCheatMoneyMultiplier = 1.25f; // Calculate the amount of money used when cheating more than 1 times.
    [SerializeField] private float _beforeVotingDivider = 0.3f; // Use when use cheat election before complete the election.
    [SerializeField] private float _afterVotingDivider = 0.5f; // Use when use cheat election after completed the election.

    public float minimumElectionValue => _minimumElectionValue;
    public float maximumElectionValue => _maximumElectionValue;
    public float electionBaseChance => _electionBaseChance;
    public float electionDivider => _electionDivider;
    public float votingResultDivider => _votingResultDivider;
    public float votingCheatMoneyPercent => _votingCheatMoneyPercent;
    public float votingCheatMoneyMultiplier => _votingCheatMoneyMultiplier;
    public float beforeVotingDivider => _beforeVotingDivider;
    public float afterVotingDivider => _afterVotingDivider;


    [Header("Score Calculation")]
    [SerializeField] private float _upgradeMultiply = 10000;
    [SerializeField] private float _publicMultiply = 1500;
    [SerializeField] private float _economyMultiply = 1500;
    [SerializeField] private float _militaryMultiply = 1500;
    [SerializeField] private float _moneyDivider = 1000f;
    [SerializeField] private float _badPublicMultiply = 2500;

    public float upgradeMultiply => _upgradeMultiply;
    public float publicMultiply => _publicMultiply;
    public float economyMultiply => _economyMultiply;
    public float militaryMultiply => _militaryMultiply;
    public float moneyDivider => _moneyDivider;
    public float badPublicMultiply => _badPublicMultiply;


    public void updateNewValue(int _currentMoney, int _currentIncome, int _currentOutcome)
    {
        currentMoney = _currentMoney;
        monthlyIncome = _currentIncome;
        monthlyOutcome = _currentOutcome;

        GameManager.Instance.money = currentMoney;
    }

    public bool endOfTheMonth()
    {
        return GameManager.Instance.days == endOfTheMonthDate;
    }

    public int getCurrentMoney
    {
        get
        {
            currentMoney = GameManager.Instance.money;
            return currentMoney;
        }
    }

    public void changeCountryName(string newName)
    {
        _newCountryName = newName;
    }
}
