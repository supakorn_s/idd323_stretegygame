using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New data manager", menuName = "Manager/Game Data Manager", order = 0)]
public class GameData : ScriptableObject
{
    [Header("Statistic")]
    private string countryName;
    private int money;
    private int days;
    private int months;
    private int years;

    [HideInInspector] public bool isTransitioning = false;

    public void loadGameData(string _countryName, int _money, int _day, int _month, int _year)
    {
        _countryName = countryName;
        _money = money;
        _day = days;
        _month = months;
        _year = years;
    }

    public void saveGameData(string _countryName, int _money, int _day, int _month, int _year)
    {
        countryName = _countryName;
        money = _money;
        days = _day;
        months = _month;
        years = _year;
    }

    public void transitionCompleteSwitch(bool transitionState)
    {
        isTransitioning = transitionState;
    }
}
