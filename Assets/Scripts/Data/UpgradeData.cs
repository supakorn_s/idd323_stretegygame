using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Upgrade", menuName = "Data/Upgrade Data", order = 1)]
public class UpgradeData : ScriptableObject
{
    [Header("Upgrade Attribute")]
    [SerializeField] private string _upgradeName;
    [SerializeField] [TextArea(5,20)] private string _upgradeDescription;
    [SerializeField] private int _price;
    [SerializeField] private Sprite _upgradeSprite;
    [SerializeField] private StatEffectData _statEffectData;
    [SerializeField] private StatEffectData _secondaryStatEffectData;

    public string upgradeName => _upgradeName;
    public string upgradeDescription => _upgradeDescription;
    public int price => _price;
    public Sprite upgradeSprite => _upgradeSprite;
    public StatEffectData statEffectData => _statEffectData;
    public StatEffectData secondaryStatEffectData => _secondaryStatEffectData;

    [Header("Upgrade")]
    [Range(0, 20)] [SerializeField] private int _upgradeTier;
    [SerializeField] private bool _unlockNextTier = false;
    [SerializeField] private bool _openGovernmentTab = false;

    public int upgradeTier => _upgradeTier;
    public bool unlockNextTier => _unlockNextTier;
    public bool openGovernmentTab => _openGovernmentTab;


}
