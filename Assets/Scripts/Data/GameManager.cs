using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    [Header("Statistic")]
    [SerializeField] private string _countryName;
    public int money;
    public int days;
    public int months;
    public int years;
    public float electDayIn;


    [Range(0, 100)][SerializeField] private float _publicOpinionValue = 50;
    [Range(0, 100)][SerializeField] private float _economyValue = 50;
    [Range(0, 100)][SerializeField] private float _militaryValue = 50;

    [SerializeField] private float _electionStackupChance = 0;

    public string countryName => _countryName;
    public float publicOpinionValue => _publicOpinionValue;
    public float economyValue => _economyValue;
    public float militaryValue => _militaryValue;
    public float electionStackupChance => _electionStackupChance;

    [Header("Reference")]
    [SerializeField] private EconomyData _economyData;
    [HideInInspector] public bool isPaused = false;
    [HideInInspector] public float electionEvery;

    public EconomyData economyData => _economyData;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        money = _economyData.startingMoney;
        electionEvery = _economyData.electionEvery;
        electDayIn = electionEvery;

        _countryName = _economyData.newCountryName;
    }

    public void loadDateData(int _day, int _month, int _year)
    {
        days = _day;
        months = _month;
        years = _year;
    }

    public void loadMoneyData(int _money)
    {
        //_money = _money;
    }

    public void loadCountryStatData(float POValue, float ECOValue, float MILValue)
    {
        _publicOpinionValue = POValue;
        _economyValue = ECOValue;
        _militaryValue = MILValue;
    }
    public void addDataToCountryStat(float POValue, float ECOValue, float MILValue)
    {
        _publicOpinionValue += POValue;
        _economyValue += ECOValue;
        _militaryValue += MILValue;

        if (_publicOpinionValue >= 100) _publicOpinionValue = 100;
        if (_economyValue >= 100) _economyValue = 100;
        if (_militaryValue >= 100) _militaryValue = 100;
    }

    public void AddElectionBonus(float NewElectionBonusChance)
    {
        _electionStackupChance += NewElectionBonusChance;
    }


    public void DebugSkipEntireMonth()
    {
        months = 29;
    }
    public void DebugStartElection()
    {
        electDayIn = 5;
    }
    public void CheatPublicOpinion(float POvalue)
    {
        _publicOpinionValue = POvalue;
    }
    public void CheatEconomic(float ECOvalue)
    {
        _economyValue = ECOvalue;
    }
    public void CheatMilitary(float MILvalue)
    {
        _militaryValue = MILvalue;
    }
}
