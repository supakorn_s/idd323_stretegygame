using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New District Type", menuName = "Data/District Data", order = 0)]
public class DistrictData : ScriptableObject
{
    [Header("District Description")]
    [SerializeField] private string _districtName;
    [TextArea] [SerializeField] private string districtDescription;
    [TextArea] [SerializeField] private string districtEffect;
    [SerializeField] private Sprite _districtImage;

    public string districtName => _districtName;
    public Sprite districtImage => _districtImage;

    [Header("District Type")]
    [SerializeField] private bool _isResidential = false;
    [SerializeField] private bool _isIndustry = false;
    [SerializeField] private bool _isHarbour = false;
    [SerializeField] private bool _isCommercial = false;
    [SerializeField] private bool _isBadZone = false;
    public bool isResidential => _isResidential;
    public bool isIndustry => _isIndustry;
    public bool isHarbour => _isHarbour;
    public bool isCommercial => _isCommercial;
    public bool isBadZone => _isBadZone;

    [Header("Bonus add up")]
    [SerializeField] private int _moneyBonus;
    [SerializeField] private float _publicOpinionBonus;
    [SerializeField] private float _economyBonus;
    [SerializeField] private float _militaryBonus;
    public int moneyBonus => _moneyBonus;
    public float publicOpinionBonus => _publicOpinionBonus;
    public float economyBonus => _economyBonus;
    public float militaryBonus => _militaryBonus;

    [Header("Percent bonus add up")]
    [SerializeField] private float _moneyPercent;
    [SerializeField] private float _publicOpinionPercent;
    [SerializeField] private float _economyPercent;
    [SerializeField] private float _militaryPercent;
    public float moneyPercent => _moneyPercent;
    public float publicOpinionPercent => _publicOpinionPercent;
    public float economyPercent => _economyPercent;
    public float militaryPercent => _militaryPercent;

}
