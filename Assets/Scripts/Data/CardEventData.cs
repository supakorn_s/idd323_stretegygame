using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Data/Card Data", order = 1)]
public class CardEventData : ScriptableObject
{
    [Header("Card Description")]
    [SerializeField] private string _cardName;
    [TextArea] [SerializeField] private string _cardDescription;
    [TextArea] [SerializeField] private string _effectDescription;
    [SerializeField] private StatEffectData _statEffectData;
    [SerializeField] private StatEffectData _subsStatEffectData;

    public string cardName => _cardName;
    public string cardDescription => _cardDescription;
    public string effectDescription => _effectDescription;
    public StatEffectData statEffectData => _statEffectData;
    public StatEffectData subsStatEffectData => _subsStatEffectData;

}
