using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stat Effect Data", menuName = "Data/Stat Effect Data", order = 0)]
public class StatEffectData : ScriptableObject
{
    [Header("Uncheck to add bonus once")]
    [SerializeField] private bool _perMonth = true;
    public bool perMonth => _perMonth;

    [Header("Bonus add up")]
    [SerializeField] private int _moneyBonus;
    [SerializeField] private float _publicOpinionBonus;
    [SerializeField] private float _economyBonus;
    [SerializeField] private float _militaryBonus;
    public int moneyBonus => _moneyBonus;
    public float publicOpinionBonus => _publicOpinionBonus;
    public float economyBonus => _economyBonus;
    public float militaryBonus => _militaryBonus;

    [Header("Percent bonus add up")]
    [Range(-1, 1)] [SerializeField] private float _moneyPercent;
    [Range(-1, 1)] [SerializeField] private float _publicOpinionPercent;
    [Range(-1, 1)] [SerializeField] private float _economyPercent;
    [Range(-1, 1)] [SerializeField] private float _militaryPercent;

    [Header("Percent event bonus add up")]
    [Range(-1, 1)] [SerializeField] private float _electionPercent;
    [Range(-1, 1)] [SerializeField] private float _eventPercent;
    public float moneyPercent => _moneyPercent;
    public float publicOpinionPercent => _publicOpinionPercent;
    public float economyPercent => _economyPercent;
    public float militaryPercent => _militaryPercent;
    public float electionPercent => _electionPercent;
    public float eventPercent => _eventPercent;

    /*public void changeOneTimePayment(bool disableThisBool)
    {
        _oneTimePaid = disableThisBool;
    }*/
}
