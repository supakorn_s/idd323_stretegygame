using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Government Type", menuName = "Data/Government Data", order = 0)]
public class GovernmentData : ScriptableObject
{
    [Header("Government Description")]
    [SerializeField] private string _systemName;
    [SerializeField] private Sprite _governmentSprite;
    [SerializeField] private StatEffectData _governmentPassiveStat;

    public string systemName => _systemName;
    public Sprite governmentSprite => _governmentSprite;
    public StatEffectData governmentPassiveStat => _governmentPassiveStat;

    [Header("Starting Bonus")]
    [SerializeField] private bool _hasElection;

    [SerializeField] private int _StartingFund;

    [SerializeField] private float _publicOpinionValue;

    [SerializeField] private float _economyStrengthValue;

    [SerializeField] private float _militaryStrengthValue;

    public bool hasElection => _hasElection;
    public int StartingFund => _StartingFund;
    public float publicOpinionValue => _publicOpinionValue;
    public float economyStrengthValue => _economyStrengthValue;
    public float militaryStrengthValue => _militaryStrengthValue;

    //[Header("Percentage Modifier")]
}
