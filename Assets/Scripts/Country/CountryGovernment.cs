using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryGovernment : MonoBehaviour
{
    [Header("Notification Text")]
    [TextArea] [SerializeField] private string _changeGovernmentRevenueText = "Government Bonus";
    [TextArea] [SerializeField] private string _changeGovernmentPOText = "Government Public Opinion Bonus";
    [TextArea] [SerializeField] private string _changeGovernmentECOText = "Government Economy Bonus";
    [TextArea] [SerializeField] private string _changeGovernmentMILText = "Government Military Bonus";


    private GovernmentData _governmentData;
    private string _systemName;
    private bool _hasElection = true;
    public GovernmentData governmentData => _governmentData;
    private InformationbarUpdate _informationbarUpdate;
    private CountryUpgrade _CountryUpgrade;

    private void Start()
    {
        _informationbarUpdate = FindObjectOfType<InformationbarUpdate>();
        _CountryUpgrade = FindObjectOfType<CountryUpgrade>();
    }

    public void changeGovernment(GovernmentData newGovernmentData)
    {
        _governmentData = newGovernmentData;

        _systemName = _governmentData.systemName;
        _hasElection = _governmentData.hasElection;

        CountryAudio.Instance.ChangeGovernmentAudio();

        updateCurrentStat();

        if (newGovernmentData.governmentPassiveStat != null)
        {
            _CountryUpgrade.insertNewEventData(newGovernmentData.governmentPassiveStat);
        }
    }

    private void updateCurrentStat()
    {
        GameManager.Instance.money += _governmentData.StartingFund;
        GameManager.Instance.addDataToCountryStat(_governmentData.publicOpinionValue, _governmentData.economyStrengthValue, _governmentData.militaryStrengthValue);
        _informationbarUpdate.spawnText(_changeGovernmentRevenueText + " ", _governmentData.StartingFund, true);
        _informationbarUpdate.spawnText(_changeGovernmentPOText + " ", _governmentData.publicOpinionValue, false);
        _informationbarUpdate.spawnText(_changeGovernmentECOText + " ", _governmentData.economyStrengthValue, false);
        _informationbarUpdate.spawnText(_changeGovernmentMILText + " ", _governmentData.militaryStrengthValue, false);
    }
}
