using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryDistrict : MonoBehaviour
{
    [Header("Data Manager")]
    [SerializeField] private CountryBrain _countryBrain;

    [Header("Country District Setting")]
    [SerializeField] private GameObject DistrictPrefab;
    [SerializeField] private DistrictData[] DistrictData;
    [SerializeField] private DistrictData[] BadDistrictData;

    private void Start()
    {
        createDistrict();
    }

    public void createDistrict() // + Set Initial position
    {
        for (int i = 0; i < _countryBrain.Regions.Length; i++)
        {
            // Set the position of the District here.
            Vector3 regionPos = _countryBrain.Regions[i].transform.position;
            GameObject newDistrict = Instantiate(DistrictPrefab, regionPos, Quaternion.identity);

            DistrictBrain _districtBrain = newDistrict.GetComponent<DistrictBrain>();

            if (designatedDistrict == 1)
            {
                int randomDData = Random.Range(0, DistrictData.Length);
                _districtBrain._districtData = DistrictData[randomDData];
            }
            else
            {
                int randomDData = Random.Range(0, BadDistrictData.Length);
                _districtBrain._districtData = BadDistrictData[randomDData];
            }
        }
    }

    private int designatedDistrict
    {
        get
        {
            int districtType;
            //Good District Rate
            if (Random.value <= _countryBrain._economyData.goodDistrictRate)
            {
                districtType = 1;
                return districtType;
            }
            else //Bad District Rate
            {
                districtType = 0;
                return districtType;
            }
        }
    }
}
