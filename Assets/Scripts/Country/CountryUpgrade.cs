using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryUpgrade : MonoBehaviour
{
    [Range(0, 20)][SerializeField] private int _currentUpgradeTier = 0;

    [SerializeField] private List<UpgradeData> _upgradeData;
    [SerializeField] private List<StatEffectData> _statEffectData;

    public int currentUpgradeTier => _currentUpgradeTier;
    public List<UpgradeData> upgradeData => _upgradeData;
    public List<StatEffectData> statEffectData => _statEffectData;


    public void increaseTier(int increaseBy)
    {
        _currentUpgradeTier += increaseBy;
    }
    public void insertNewUpgradeData(UpgradeData newUpgradeData, StatEffectData newStatEffectData)
    {
        _upgradeData.Add(newUpgradeData);
        _statEffectData.Add(newStatEffectData);
    }
    public void insertNewEventData(StatEffectData newStatEffectData)
    {
        _statEffectData.Add(newStatEffectData);
    }

}
