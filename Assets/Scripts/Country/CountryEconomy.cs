using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryEconomy : MonoBehaviour
{
    [Header("Data Manager")]
    [SerializeField] private CountryBrain _countryBrain;
    private CountryUpgrade _countryUpgrade;
    private InformationbarUpdate _informationbarUpdate;
    public EconomyData _economyData;

    private int _currentMoney;
    private int _monthlyIncome;
    private int _monthlyOutcome;

    private int _monthlyIncomeBase;
    private float _monthlyIncomeMultiplyingBase;
    private int _monthlyOutcomeBase;

    private int totalRegion;

    [HideInInspector] public List<int> incomeValueList;
    [HideInInspector] public List<float> publicOpinionValueList;
    [HideInInspector] public List<float> economyBonusList;
    [HideInInspector] public List<float> MilitaryBonusList;

    [HideInInspector] public List<float> incomePercentList;
    [HideInInspector] public List<float> publicPercentList;
    [HideInInspector] public List<float> economyPercentList;
    [HideInInspector] public List<float> MilitaryPercentList;

    private int totalIncomeValue;
    private int totalOutcomeValue;
    private float totalPercentIncomeValue;
    private float totalPercentOutcomeValue;

    private float statPOValue;
    private float statECOValue;
    private float statMILValue;

    private int upgradeIncomeValue;
    private float upgradePOValue;
    private float upgradeECOValue;
    private float upgradeMILValue;

    [Header("Stat Effect Lists")]
    [SerializeField] private List<StatEffectData> _publicOpinionStatEffect;
    [SerializeField] private List<StatEffectData> _economyStatEffect;
    [SerializeField] private List<StatEffectData> _militaryStatEffect;
    [SerializeField] private StatEffectData _POEffectInUse;
    [SerializeField] private StatEffectData _ECOEffectInUse;
    [SerializeField] private StatEffectData _MILffectInUse;
    private float _cardEventChance;
    private float _electionChance;
    public float cardEventChance => _cardEventChance;
    public float electionChance => _electionChance;

    private void Start()
    {
        if (_countryBrain._economyData == null)
        {
            return;
        }

        _economyData = _countryBrain._economyData;
        _countryUpgrade = _countryBrain.countryUpgrade;
        _monthlyIncomeBase = _economyData.incomeBase;
        _monthlyIncomeMultiplyingBase = _economyData.incomeMultiplying;
        _monthlyOutcomeBase = _economyData.outcomeBase;
        _informationbarUpdate = FindObjectOfType<InformationbarUpdate>();
    }
    
    private void revenueCalculate()
    {
        _currentMoney = _economyData.getCurrentMoney;
        // New OTHERS BONUS data here
        StatCalculate();
        upgradeCalculate();
        float newPOValue = statPOValue + upgradePOValue;
        float newECOValue = statECOValue + upgradeECOValue;
        float newMILValue = statMILValue + upgradeMILValue;

        // These below give out the new value and update the text to new value
        int _currentIncome = IncomeCalculate;
        int _currentOutcome = OutcomeCalculate;
        if (upgradeIncomeValue > 0) _currentIncome += upgradeIncomeValue;
        else if (upgradeIncomeValue < 0) _currentOutcome += upgradeIncomeValue;

        _informationbarUpdate.spawnText("Total Income ", _currentIncome, true);
        _informationbarUpdate.spawnText("Total Outcome ", _currentOutcome, true);
        _informationbarUpdate.spawnText("Public Opinion ", newPOValue, false);
        _informationbarUpdate.spawnText("Economy ", newECOValue, false);
        _informationbarUpdate.spawnText("Military ", newMILValue, false);

        _currentMoney += _currentIncome + _currentOutcome;
        if (_currentMoney >= 1000000000)
        {
            _currentMoney = 999999999;
        }


        GameManager.Instance.addDataToCountryStat(newPOValue, newECOValue, newMILValue);
        GameManager.Instance.money = _currentMoney;
        //_economyData.updateNewValue(_currentMoney, _currentIncome, _currentOutcome);
        //_countryBrain._moneyUpdate.updateMoney(_currentMoney);

    }
    private int IncomeCalculate
    {
        get
        {
            // Money from all Regions
            totalRegion = _countryBrain.Regions.Length;
            int regionIncome = totalRegion * _monthlyIncomeBase;

            // Bonus Money from Region calculate
            for (int i = 0; i < incomeValueList.Count; i++)
            {
                if (incomeValueList[i] > 0)
                {
                    totalIncomeValue += incomeValueList[i];
                }
            }
            int newTotalIncomeValue = totalIncomeValue;

            // Bonus Percent Money from Region calculate
            for (int i = 0; i < incomePercentList.Count; i++)
            {
                if (incomePercentList[i] > 0)
                {
                    totalPercentIncomeValue += incomePercentList[i];
                }
            }
            //totalPercentIncomeValue = _currentMoney * totalPercentIncomeValue;    //Old Formula
            totalPercentIncomeValue = ((_currentMoney * _monthlyIncomeMultiplyingBase) + regionIncome + newTotalIncomeValue) * totalPercentIncomeValue;
            int newTotalPercentIncomeValue = (int)totalPercentIncomeValue;

            _monthlyIncome = regionIncome + newTotalIncomeValue + newTotalPercentIncomeValue;
            totalIncomeValue = 0;
            totalPercentIncomeValue = 0;
            return _monthlyIncome;
        }
    }
    private int OutcomeCalculate
    {
        get
        {
            // Money from all Regions
            totalRegion = _countryBrain.Regions.Length;
            int regionOutcome = totalRegion * _monthlyOutcomeBase;

            // Bonus Money from Region calculate
            for (int i = 0; i < incomeValueList.Count; i++)
            {
                if (incomeValueList[i] < 0)
                {
                    totalOutcomeValue += incomeValueList[i];
                }
            }
            int newTotalOutcomeValue = totalOutcomeValue;

            // Bonus Percent Money from Region calculate
            for (int i = 0; i < incomePercentList.Count; i++)
            {
                if (incomePercentList[i] < 0)
                {
                    totalPercentOutcomeValue += incomePercentList[i];
                }
            }
            totalPercentOutcomeValue = _currentMoney * totalPercentOutcomeValue;
            int newTotalPercentOutcomeValue = (int)totalPercentOutcomeValue;

            //Debug.Log("Total Value = " + newTotalOutcomeValue);
            //Debug.Log("Total Percent = " + newTotalPercentOutcomeValue + " and " + totalPercentOutcomeValue);

            _monthlyOutcome = regionOutcome + newTotalOutcomeValue + newTotalPercentOutcomeValue;
            totalOutcomeValue = 0;
            totalPercentOutcomeValue = 0;
            return _monthlyOutcome;
        }
    }
    private void StatCalculate()
    {
        currentStatAndEconomy();
        float POValue = GameManager.Instance.publicOpinionValue;
        float ECOValue = GameManager.Instance.economyValue;
        float MILValue = GameManager.Instance.militaryValue;
        float newPOValue;
        float newECOValue;
        float newMILValue;

        float POStat = _POEffectInUse.publicOpinionBonus + _ECOEffectInUse.publicOpinionBonus + _MILffectInUse.militaryBonus;
        float POPercent = POValue * (_POEffectInUse.publicOpinionPercent + _ECOEffectInUse.publicOpinionPercent + _MILffectInUse.militaryPercent);
        newPOValue = POStat + POPercent + _economyData.publicOpinionDecayValue;

        float ECOStat = _POEffectInUse.economyBonus + _ECOEffectInUse.economyBonus + _MILffectInUse.economyBonus;
        float ECOPercent = ECOValue * (_POEffectInUse.economyPercent + _ECOEffectInUse.economyPercent + _MILffectInUse.economyPercent);
        newECOValue = ECOStat + ECOPercent + _economyData.economyDecayValue;

        float MILStat = _POEffectInUse.militaryBonus + _ECOEffectInUse.militaryBonus + _MILffectInUse.militaryBonus;
        float MILPercent = MILValue * (_POEffectInUse.militaryPercent + _ECOEffectInUse.militaryPercent + _MILffectInUse.militaryPercent);
        newMILValue = MILStat + MILPercent + _economyData.militaryDecayValue;


        statPOValue = newPOValue;
        statECOValue = newECOValue;
        statMILValue = newMILValue;
        //GameManager.Instance.addDataToCountryStat(newPOValue,newECOValue, newMILValue);
    }
    private void currentStatAndEconomy()
    {
        int _pov = CurrentStatInPercent(GameManager.Instance.publicOpinionValue);
        int _ecov = CurrentStatInPercent(GameManager.Instance.economyValue);
        int _milv = CurrentStatInPercent(GameManager.Instance.militaryValue);

        switch (_pov)
        {
            case 1:
                _POEffectInUse = _publicOpinionStatEffect[0];
                break;
            case 2:
                _POEffectInUse = _publicOpinionStatEffect[1];
                break;
            case 3:
                _POEffectInUse = _publicOpinionStatEffect[2];
                break;
            case 4:
                _POEffectInUse = _publicOpinionStatEffect[3];
                break;
            case 5:
                _POEffectInUse = _publicOpinionStatEffect[4];
                break;
        }
        switch (_ecov)
        {
            case 1:
                _ECOEffectInUse = _economyStatEffect[0];
                break;
            case 2:
                _ECOEffectInUse = _economyStatEffect[1];
                break;
            case 3:
                _ECOEffectInUse = _economyStatEffect[2];
                break;
            case 4:
                _ECOEffectInUse = _economyStatEffect[3];
                break;
            case 5:
                _ECOEffectInUse = _economyStatEffect[4];
                break;
        }
        switch (_milv)
        {
            case 1:
                _MILffectInUse = _militaryStatEffect[0];
                break;
            case 2:
                _MILffectInUse = _militaryStatEffect[1];
                break;
            case 3:
                _MILffectInUse = _militaryStatEffect[2];
                break;
            case 4:
                _MILffectInUse = _militaryStatEffect[3];
                break;
            case 5:
                _MILffectInUse = _militaryStatEffect[4];
                break;
        }
    }
    private int CurrentStatInPercent(float statValue)
    {
        int statStatus = 3;

        if (statValue > _economyData.normal)
        {
            if (statValue < _economyData.good)
            {
                statStatus = 4;
            }
            else if (statValue >= _economyData.veryGood)
            {
                statStatus = 5;
            }
        }
        if (statValue < _economyData.normal)
        {
            if (statValue > _economyData.bad)
            {
                statStatus = 2;
            }
            else if (statValue <= _economyData.veryBad)
            {
                statStatus = 1;
            }
        }
        else if (statValue == _economyData.normal)
        {
            statStatus = 3;
        }
        return statStatus;
    }
    private void upgradeCalculate()
    {
        List<StatEffectData> upgradeData = _countryUpgrade.statEffectData;
        _currentMoney = _economyData.getCurrentMoney;
        int _pov = CurrentStatInPercent(GameManager.Instance.publicOpinionValue);
        int _ecov = CurrentStatInPercent(GameManager.Instance.economyValue);
        int _milv = CurrentStatInPercent(GameManager.Instance.militaryValue);

        int newMoney = 0;
        float newPO = 0;
        float newECO = 0;
        float newMIL = 0;

        int oneMoney = 0;
        float onePO = 0;
        float oneECO = 0;
        float oneMIL = 0;

        float electionBonus = 0;
        float moneyPercent = 0.0f;

        upgradeIncomeValue = 0;
        upgradePOValue = 0;
        upgradeECOValue = 0;
        upgradeMILValue = 0;

        //Addup Upgrade
        if (upgradeData.Count != 0)
        {
            for (int i = 0; i < upgradeData.Count; i++)
            {
                newMoney = 0;
                newPO = 0;
                newECO = 0;
                newMIL = 0;

                oneMoney = 0;
                onePO = 0;
                oneECO = 0;
                oneMIL = 0;
                switch (upgradeData[i].perMonth)
                {
                    case true:
                        newMoney += upgradeData[i].moneyBonus;
                        //newMoney += (int)(_currentMoney * upgradeData[i].moneyPercent); Old Formula
                        //newMoney += (int)((IncomeCalculate + newMoney) * upgradeData[i].moneyPercent); Old Formula
                        moneyPercent += upgradeData[i].moneyPercent;
                        //Debug.Log("Total Money% " + moneyPercent);

                        newPO += upgradeData[i].publicOpinionBonus;
                        newPO += _pov * upgradeData[i].publicOpinionPercent;

                        newECO += upgradeData[i].economyBonus;
                        newECO += _ecov * upgradeData[i].economyPercent;

                        newMIL += upgradeData[i].militaryBonus;
                        newMIL += _milv * upgradeData[i].militaryPercent;
                        break;
                    case false:
                        oneMoney += upgradeData[i].moneyBonus;
                        //oneMoney += (int)(_currentMoney * upgradeData[i].moneyPercent); Old Formula
                        moneyPercent += upgradeData[i].moneyPercent;

                        onePO += upgradeData[i].publicOpinionBonus;
                        onePO += _pov * upgradeData[i].publicOpinionPercent;

                        oneECO += upgradeData[i].economyBonus;
                        oneECO += _ecov * upgradeData[i].economyPercent;

                        oneMIL += upgradeData[i].militaryBonus;
                        oneMIL += _milv * upgradeData[i].militaryPercent;

                        break;
                }
                electionBonus += upgradeData[i].electionPercent;
                GameManager.Instance.AddElectionBonus(electionBonus);

                upgradeIncomeValue += newMoney + oneMoney;
                //Debug.Log("Total upgrade money " + upgradeIncomeValue);
                upgradePOValue += newPO + onePO;
                upgradeECOValue += newECO + oneECO;
                upgradeMILValue += newMIL + oneMIL;
                //GameManager.Instance.addDataToCountryStat(upgradePOValue, upgradeECOValue, upgradeMILValue); add it on revenus calculator alr.
            }
            upgradeIncomeValue = (int)(upgradeIncomeValue + ((IncomeCalculate + upgradeIncomeValue) * moneyPercent));
            //Debug.Log("Finalize Bonus Money  " + upgradeIncomeValue);


            // Remove Upgrade that offer 1 time payment
            for (int i = 0; i < upgradeData.Count; i++)
            {
                if (upgradeData[i].perMonth == false)
                {
                    upgradeData[i] = null;
                }
            }
            upgradeData.RemoveAll(StatEffectData => StatEffectData == null);
        }
        

    }
    public float CalculateCardEventStatChance
    {
        get
        {
            _cardEventChance = 0;
            List<StatEffectData> upgradeData = new List<StatEffectData>();
            upgradeData.Add(_POEffectInUse);
            upgradeData.Add(_ECOEffectInUse);
            upgradeData.Add(_MILffectInUse);

            if (upgradeData.Count != 0)
            {
                for (int i = 0; i < upgradeData.Count; i++)
                {
                    _cardEventChance += upgradeData[i].eventPercent;
                }
            }
            //Debug.Log(cardEventChance);
            return cardEventChance;
        }
    }
    public float CalculateElectionStatChance
    {
        get
        {
            StatCalculate();
            _electionChance = 0;
            List<StatEffectData> upgradeData = new List<StatEffectData>();
            upgradeData.Add(_POEffectInUse);
            upgradeData.Add(_ECOEffectInUse);
            upgradeData.Add(_MILffectInUse);

            if (upgradeData.Count != 0)
            {
                for (int i = 0; i < upgradeData.Count; i++)
                {
                    _electionChance += upgradeData[i].electionPercent;
                }
            }
            
            return electionChance;
        }
    }
    public void EndOfTheMonth()
    {
        revenueCalculate();
    }
}
