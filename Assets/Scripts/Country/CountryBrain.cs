using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryBrain : MonoBehaviour
{
    [HideInInspector] public PlayerInputManager _inputManager;
    [Header("Data Manager")]
    public GameData _gameData;
    public EconomyData _economyData;
    public moneyUpdate _moneyUpdate;
    [SerializeField] private CountryGovernment _countryGovernment;
    [SerializeField] private CountryUpgrade _countryUpgrade;
    [SerializeField] private CountryEconomy _countryEconomy;

    public CountryGovernment countryGovernment => _countryGovernment;
    public CountryUpgrade countryUpgrade => _countryUpgrade;
    public CountryEconomy countryEconomy => _countryEconomy;

    [Header("Country Brain Setting")]
    public GameObject[] Regions;
    private GameObject rayHitObject;

    void Start()
    {
        _inputManager = PlayerInputManager.Instance;
    }
    private void Update()
    {
        
    }

    private void regionSelect()
    {
        if (_inputManager.LeftClick())
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(_inputManager.GetMousePosition());
            RaycastHit2D hitObject = Physics2D.Raycast(mousePosition, Vector2.zero, 0f);
            if (hitObject.collider != null)
            {
                rayHitObject = hitObject.collider.gameObject;
                if (rayHitObject.GetComponent<RegionBrain>())
                {
                    RegionBrain rayHitScript = rayHitObject.GetComponent<RegionBrain>();
                    rayHitScript.changeHighlightColor();
                }
            }
        }
    }
}
