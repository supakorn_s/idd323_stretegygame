using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [HideInInspector] public PlayerInputManager _inputManager;
    
    public enum cameraState { MOVEABLE, UNMOVEABLE };
    public cameraState state = cameraState.MOVEABLE;

    [Header("Camera Setting")]
    public float panSpeed = 20f;
    public Vector2 panLimit;
    public float camBorderEdge = 10f;
    public float scrollSpeed = 5f;
    [Range(1, 50)] public float scrollLimitMin = 5f;
    [Range(5, 50)] public float scrollLimitMax = 10f;

    // Variable and Ref
    Camera cam;


    void Start()
    {
        _inputManager = PlayerInputManager.Instance;
        
        cam = GetComponentInChildren<Camera>();
    }
    void Update()
    {
        if (cam == null)
        {
            return;
        }
        switch (state)
        {
            case cameraState.MOVEABLE:
                cameraMovement();
                cameraScroll();
                break;
            case cameraState.UNMOVEABLE:

                break;
        }
    }

    void cameraMovement()
    {
        Vector3 pos = transform.position;

        Vector2 inputMovement = _inputManager.GetCamMovement();
        Vector3 camResult = new Vector3(inputMovement.x, inputMovement.y, 0);
        Vector2 mousePosition = _inputManager.GetMousePosition();


        // Keyboard Cam
        if (inputMovement.x != 0 || inputMovement.y != 0)
        {
            pos += camResult * panSpeed * Time.deltaTime;
        }

        // Mouse Cam
        if (camResult.x == 0 && mousePosition.x >= Screen.width - camBorderEdge)
        {
            pos += new Vector3(1f,0f,0f) * panSpeed * Time.deltaTime;
        }
        else if (camResult.x == 0 && mousePosition.x <= camBorderEdge)
        {
            pos -= new Vector3(1f, 0f, 0f) * panSpeed * Time.deltaTime;
        }
        if (camResult.y == 0 && mousePosition.y >= Screen.height - camBorderEdge)
        {
            pos += new Vector3(0f, 1f, 0f) * panSpeed * Time.deltaTime;
        }
        else if (camResult.y == 0 && mousePosition.y <= camBorderEdge)
        {
            pos -= new Vector3(0f, 1f, 0f) * panSpeed * Time.deltaTime;
        }

        pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
        pos.y = Mathf.Clamp(pos.y, -panLimit.y, panLimit.y);

        transform.position = pos ;
    }
    void cameraScroll()
    {
        Vector2 inputScroll = _inputManager.GetMouseScroll();
        float scroll = inputScroll.y;

        if (scroll < 0)
        {
            cam.orthographicSize -= scroll * scrollSpeed * Time.deltaTime;
        }
        if (scroll > 0)
        {
            cam.orthographicSize -= scroll * scrollSpeed * Time.deltaTime;
        }

        cam.orthographicSize = Mathf.Clamp(cam.orthographicSize, scrollLimitMin, scrollLimitMax);
    }
    void cameraFocus(Vector2 positionToFocus)
    {
        transform.position = positionToFocus;
    }
}
