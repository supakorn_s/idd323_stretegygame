using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class moneyUpdate : MonoBehaviour
{
    [Header("Data Manager")]
    [SerializeField] private EconomyData _economyData;

    private int currentMoney;
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private TextMeshProUGUI countryNameText;


    private void Start()
    {
        currentMoney = _economyData.startingMoney;
        GameManager.Instance.loadMoneyData(currentMoney);

        moneyText.text = currentMoney.ToString("#,#");
        countryNameText.text = _economyData.newCountryName;
    }
    private void Update()
    {
        money();
    }
    private void money()
    {
        currentMoney = GameManager.Instance.money;
        moneyText.text = currentMoney.ToString("#,#");
    }
    public void updateMoney(int newMoney)
    {
        currentMoney = newMoney;
        GameManager.Instance.money = currentMoney;
        moneyText.text = currentMoney.ToString("#,#");
    }
}
