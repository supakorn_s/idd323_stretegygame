using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpgradeUI : MonoBehaviour
{
    [Header("Canvas Tab")]
    [SerializeField] private GameObject _upgradeContentTab;
    [SerializeField] private GameObject _governmentTab;

    [SerializeField] private GameObject _upgradeCanvasChild;
    [SerializeField] private GameObject _upgradePrefab;

    private bool ContentTabOpen = false;

    public GameObject upgradeContentTab => _upgradeContentTab;
    public GameObject governmentTab => _governmentTab;

    [Header("Upgrade Text")]
    [SerializeField] private TextMeshProUGUI upgradeTitleText;
    [SerializeField] private TextMeshProUGUI upgradeDescriptionText;

    [Header("Upgrade Options")]
    [SerializeField] private List<UpgradeData> _UpgradeTier0;
    [SerializeField] private List<UpgradeData> _UpgradeTier1;
    [SerializeField] private List<UpgradeData> _UpgradeTier2;
    [SerializeField] private List<UpgradeData> _UpgradeTier3;
    [SerializeField] private List<UpgradeData> _UpgradeTier4;
    [SerializeField] private List<UpgradeData> _UpgradeTier5;


    private CameraController _cameraController;
    private CalenderTimer _calenderTimer;
    private CountryUpgrade _countryUpgrade;
    public CountryUpgrade countryUpgrade =>_countryUpgrade;

    private void Start()
    {
        _upgradeContentTab.SetActive(false);
        _cameraController = FindObjectOfType<CameraController>();
        _calenderTimer = FindObjectOfType<CalenderTimer>();
        _countryUpgrade = FindObjectOfType<CountryUpgrade>();
    }
    public void OpenCloseUpgradeUIButton()
    {
        ContentTabOpen = !ContentTabOpen;
        _upgradeContentTab.SetActive(ContentTabOpen);

        switch (ContentTabOpen)
        {
            case true:
                _cameraController.state = CameraController.cameraState.UNMOVEABLE;
                _calenderTimer.changeSpeed(0);
                _calenderTimer.canEditTime = false;
                break;
            case false:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(1);
                _calenderTimer.canEditTime = true;
                break;
        }
    }

    public void changeUpgradeText(string _upgradeTitleText, string _upgradeDescriptionText)
    {
        upgradeTitleText.text = _upgradeTitleText;
        upgradeDescriptionText.text = _upgradeDescriptionText;
    }
    public void spawnUpgrade()
    {    
        switch (checkCurrentTier())
        {
            case 0:
                for (int i = 0; i < _UpgradeTier0.Count; i++)
                {
                    GameObject upgrade = Instantiate(_upgradePrefab, _upgradeCanvasChild.transform);
                    upgrade.transform.localPosition = new Vector3(0, 0, 0);
                    UpgradeItemUI _upgradeItemUI = upgrade.GetComponent<UpgradeItemUI>();
                    _upgradeItemUI._upgradeData = _UpgradeTier0[i];
                }
                break;
            case 1:
                for (int i = 0; i < _UpgradeTier1.Count; i++)
                {
                    GameObject upgrade = Instantiate(_upgradePrefab, _upgradeCanvasChild.transform);
                    upgrade.transform.localPosition = new Vector3(0, 0, 0);
                    UpgradeItemUI _upgradeItemUI = upgrade.GetComponent<UpgradeItemUI>();
                    _upgradeItemUI._upgradeData = _UpgradeTier1[i];
                }
                break;
            case 2:
                for (int i = 0; i < _UpgradeTier2.Count; i++)
                {
                    GameObject upgrade = Instantiate(_upgradePrefab, _upgradeCanvasChild.transform);
                    upgrade.transform.localPosition = new Vector3(0, 0, 0);
                    UpgradeItemUI _upgradeItemUI = upgrade.GetComponent<UpgradeItemUI>();
                    _upgradeItemUI._upgradeData = _UpgradeTier2[i];
                }
                break;
            case 3:
                for (int i = 0; i < _UpgradeTier3.Count; i++)
                {
                    GameObject upgrade = Instantiate(_upgradePrefab, _upgradeCanvasChild.transform);
                    upgrade.transform.localPosition = new Vector3(0, 0, 0);
                    UpgradeItemUI _upgradeItemUI = upgrade.GetComponent<UpgradeItemUI>();
                    _upgradeItemUI._upgradeData = _UpgradeTier3[i];
                }
                break;
            case 4:
                for (int i = 0; i < _UpgradeTier3.Count; i++)
                {
                    GameObject upgrade = Instantiate(_upgradePrefab, _upgradeCanvasChild.transform);
                    upgrade.transform.localPosition = new Vector3(0, 0, 0);
                    UpgradeItemUI _upgradeItemUI = upgrade.GetComponent<UpgradeItemUI>();
                    _upgradeItemUI._upgradeData = _UpgradeTier4[i];
                }
                break;
            case 5:
                for (int i = 0; i < _UpgradeTier3.Count; i++)
                {
                    GameObject upgrade = Instantiate(_upgradePrefab, _upgradeCanvasChild.transform);
                    upgrade.transform.localPosition = new Vector3(0, 0, 0);
                    UpgradeItemUI _upgradeItemUI = upgrade.GetComponent<UpgradeItemUI>();
                    _upgradeItemUI._upgradeData = _UpgradeTier5[i];
                }
                break;
        }
        
        
       

    }
    private int checkCurrentTier()
    {
        int currentTier = _countryUpgrade.currentUpgradeTier;
        return currentTier;
    }
    // Special Upgrade Action
    private void unlockTier(int tierToUnlock)
    {

    }
}
