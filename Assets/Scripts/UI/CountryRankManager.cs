using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountryRankManager : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private CalenderTimer _calenderTimer;

    [Header("Object")]
    [SerializeField] private GameObject _rankingCanvas;
    [SerializeField] private GameObject _winButton;
    [SerializeField] private List<GameObject> _objectToHide;
    [SerializeField] private bool _rankingCanvasIsOpen = false;

    [Header("Country")]
    [SerializeField] private GameObject _countryPrefab;
    [SerializeField] private GameObject _contentCanvas;
    [SerializeField] private int _countryToSpawn; // Player Will spawn after this
    [SerializeField] private CountryRankBrain _playerCountryBrain;

    [SerializeField] private List<string> _countryname;
    [SerializeField] private List<CountryRankBrain> _countryRankBrain;
    [SerializeField] private List<float> _scoreList = new List<float>() { 10000, 25000, 50000, 75000, 100000, 200000, 400000, 600000, 800000, 1000000 };
    [SerializeField] private List<float> _countryscore;
    private float _playerScore;

    [Header("Score calculation Variable")]
    [SerializeField] private CountryUpgrade _countryUpgrade;


    private void Start()
    {
        if (_cameraController == null) _cameraController = FindObjectOfType<CameraController>();
        if (_calenderTimer == null) _calenderTimer = FindObjectOfType<CalenderTimer>();

        SpawnSetup();
        _rankingCanvas.SetActive(false);
        _winButton.SetActive(false);

    }

    private void Update()
    {
        _playerCountryBrain.changeScore(ScoreCalculator);
        if (_rankingCanvasIsOpen)
        {
            Ranking();

            int currentIndex = _playerCountryBrain.gameObject.transform.GetSiblingIndex();
            if (currentIndex == 0)
            {
                _winButton.SetActive(true);
            }
        }
    }

    void Ranking()
    {
        // Other Country Ranking + Player
        float newPlayerScore = ScoreCalculator;
        _countryscore.Sort();
        for (int i = 0; i < _countryscore.Count; i++)
        {
            for (int i2 = 0; i2 < _countryRankBrain.Count; i2++)
            {
                if (_countryscore[i] == _countryRankBrain[i2].scoreNumber)
                {
                    _countryRankBrain[i2].gameObject.transform.SetSiblingIndex(_countryscore.Count - i - 1);
                    _countryRankBrain[i2].changeRank(_countryscore.Count - i);
                    //_countryscore.Sort();
                }
                else if (_countryscore[i] == _playerScore)
                {
                    _playerCountryBrain.gameObject.transform.SetSiblingIndex(_countryscore.Count - i - 1);
                    _playerCountryBrain.changeRank(_countryscore.Count - i);


                    _playerScore = newPlayerScore;
                    _countryscore[i] = _playerScore;
                }

                /*if (newPlayerScore == _countryRankBrain[i2].scoreNumber)
                {
                    _countryRankBrain[i2].gameObject.transform.SetSiblingIndex(_countryscore.Count - i);
                    _countryRankBrain[i2].changeRank(_countryscore.Count - i);
                    //_countryscore.Sort();

                    _playerScore = newPlayerScore;
                }*/
            }
        }
        _countryscore.Sort();
    }
    void SpawnSetup()
    {
        List<string> _remainingName = new List<string>(_countryname);
        List<float> _remainingScore = new List<float>(_scoreList);
        bool addMillion = false;

        for (int i = 0; i < _countryToSpawn; i++)
        {
            GameObject countryRank = Instantiate(_countryPrefab, _contentCanvas.transform);
            countryRank.transform.localPosition = new Vector3(0, 0, 0);
            countryRank.name = i.ToString();

            CountryRankBrain _countryBrain = countryRank.GetComponent<CountryRankBrain>();
            int _nameIndex = Random.Range(0, _remainingName.Count);
            int _scoreIndex = Random.Range(0, _remainingScore.Count);

            if (!addMillion)
            {
                int _millionScoreIndex = _remainingScore.Count-1;
                _countryBrain.SetUpBrain(_remainingName[_nameIndex], 1, _remainingScore[_millionScoreIndex], false);

                addMillion = true;
                _remainingScore.RemoveAt(_millionScoreIndex);
            }
            else
            {
                _countryBrain.SetUpBrain(_remainingName[_nameIndex], 1, _remainingScore[_scoreIndex], false);

                _remainingScore.RemoveAt(_scoreIndex);
            }
            _remainingName.RemoveAt(_nameIndex);
            _countryRankBrain.Add(_countryBrain);

            _countryscore.Add(_countryBrain.scoreNumber);
            _countryscore.Sort();
        }
        SpawnPlayerRank();
    }
    void SpawnPlayerRank()
    {
        GameObject countryRank = Instantiate(_countryPrefab, _contentCanvas.transform);
        countryRank.transform.localPosition = new Vector3(0, 0, 0);
        countryRank.name = (_countryToSpawn + 1).ToString() + " Player";

        CountryRankBrain _countryBrain = countryRank.GetComponent<CountryRankBrain>();
        _countryBrain.SetUpBrain(GameManager.Instance.countryName, _countryToSpawn + 1, ScoreCalculator, true);

        _playerCountryBrain = _countryBrain;
        _countryRankBrain.Add(_countryBrain);

        _playerScore = ScoreCalculator;
        _countryBrain.changeScore(_playerScore);
        _countryscore.Add(_countryBrain.scoreNumber);
        _countryscore.Sort();
    }

    public float ScoreCalculator
    {
        get
        {
            float POValue = GameManager.Instance.publicOpinionValue;
            float ECOValue = GameManager.Instance.economyValue;
            float MILValue = GameManager.Instance.militaryValue;
            float moneyValue = GameManager.Instance.money;

            float upgradeScore = _countryUpgrade.upgradeData.Count * GameManager.Instance.economyData.upgradeMultiply;
            float publicScore = POValue * GameManager.Instance.economyData.publicMultiply;
            float economyScore = ECOValue * GameManager.Instance.economyData.economyMultiply;
            float militaryScore = MILValue * GameManager.Instance.economyData.militaryMultiply;
            float moneyScore = moneyValue / GameManager.Instance.economyData.moneyDivider;
            float publicPenalty = ((100 - POValue) * GameManager.Instance.economyData.badPublicMultiply);

            float newScore = upgradeScore + publicScore + economyScore + militaryScore +
                moneyScore - publicPenalty;

            return newScore;
        }
    }




    public void OpenCloseEventUIButton()
    {
        switch (_rankingCanvasIsOpen)
        {
            case false:
                _rankingCanvas.SetActive(true);
                _rankingCanvasIsOpen = true;
                for (int i = 0; i < _objectToHide.Count; i++)
                {
                    _objectToHide[i].SetActive(false);
                }


                _cameraController.state = CameraController.cameraState.UNMOVEABLE;
                _calenderTimer.changeSpeed(0);
                _calenderTimer.canEditTime = false;

                Ranking();
                break;
            case true:
                _rankingCanvas.SetActive(false);
                _rankingCanvasIsOpen = false;
                for (int i = 0; i < _objectToHide.Count; i++)
                {
                    _objectToHide[i].SetActive(true);
                }


                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(1);
                _calenderTimer.canEditTime = true;

                Ranking();
                break;
        }
    }
}
