using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoaderCheck : MonoBehaviour
{
    [SerializeField] private GameData _gameData;

    public void transitioningSwitch()
    {
        _gameData.transitionCompleteSwitch(true);
    }
    public void transitionCompleteSwitch()
    {
        _gameData.transitionCompleteSwitch(false);
    }
}
