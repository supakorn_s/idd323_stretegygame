using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{

    [SerializeField] Animator transition;
    [SerializeField] GameObject image;
    [SerializeField] float transitiontime;

    private void Awake()
    {
        image.SetActive(true);
    }

    public void startLoadLevel(int level)
    {
        StartCoroutine(loadlevel(level));
    }

    public void exitToDestop()
    {
        StartCoroutine(exitGame());
    }

    IEnumerator loadlevel(int levelindex)
    {
        Time.timeScale = 1;

        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitiontime);

        SceneManager.LoadScene(levelindex);

    }
    IEnumerator exitGame()
    {
        Time.timeScale = 1;

        transition.SetTrigger("Start");

        yield return new WaitForSeconds(transitiontime);

        Application.Quit();

    }
}
