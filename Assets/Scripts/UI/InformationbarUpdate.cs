using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InformationbarUpdate : MonoBehaviour
{
    [SerializeField] private GameObject _canvasChildPrefab;
    [SerializeField] private GameObject _informationTextPrefab;

    [SerializeField] private Color _spawnTitleColor = Color.black;
    [SerializeField] private Color _spawnPositiveDescriptionColor = Color.green;
    [SerializeField] private Color _spawnNegativeDescriptionColor = Color.red;

    public Color spawnTitleColor => _spawnTitleColor;
    public Color spawnPositiveDescriptionColor => _spawnPositiveDescriptionColor;
    public Color spawnNegativeDescriptionColor => _spawnNegativeDescriptionColor;

    public void spawnText(string title, float newFloat, bool isMoney)
    {
        GameObject notificationText = Instantiate(_informationTextPrefab, _canvasChildPrefab.transform);
        InformationText _InformationText = notificationText.GetComponent<InformationText>();
        notificationText.transform.localPosition = new Vector3(0, 0, 0);


        if (_InformationText == null) return;

        switch (isMoney)
        {
            case true:
                if (newFloat > 0)
                {
                    _InformationText.SetMoneyText(title, newFloat, _spawnPositiveDescriptionColor);
                }
                else
                {
                    _InformationText.SetMoneyText(title, newFloat, _spawnNegativeDescriptionColor);
                }
                break;
            case false:
                if (newFloat > 0)
                {
                    newFloat = Mathf.Round(newFloat * 100.0f) / 100.0f;
                    _InformationText.SetText(title, newFloat, _spawnPositiveDescriptionColor);
                }
                else
                {
                    //newFloat = Mathf.Round(newFloat * 100.0f) * 0.01f;
                    newFloat = Mathf.Round(newFloat * 100.0f) / 100.0f;
                    _InformationText.SetText(title, newFloat, _spawnNegativeDescriptionColor);
                }
                break;

                // Can use
                //("F2")
                //yourFloat.ToString("F2")
        }
    }
}
