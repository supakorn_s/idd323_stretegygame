using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CountryRankBrain : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _countryName;
    [SerializeField] private TextMeshProUGUI _rankText;
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private int _rankNumber;
    [SerializeField] private float _scoreNumber;
    [SerializeField] private Image _backPlate;
    [SerializeField] private Color _playerHighlight = Color.blue;
    private bool _isPlayer = false;

    private int _currentIndex;
    public int rankNumber => _rankNumber;
    public float scoreNumber => _scoreNumber;
    public int currentIndex => _currentIndex;
    public bool isPlayer => _isPlayer;

    public void SetUpBrain(string newName, int newRank, float newScore, bool isPlayer)
    {
        _countryName.text = newName;
        _rankText.text = newRank.ToString();
        _scoreText.text = newScore.ToString();
        _isPlayer = isPlayer;

        _rankNumber = newRank;
        _scoreNumber = newScore;

        if (isPlayer)
        {
            _backPlate.color = _playerHighlight;
        }
    }


    public void setRankScore(int newRank, float newScore)
    {
        _rankText.text = newRank.ToString();
        _scoreNumber = newScore;
        _scoreText.text = _scoreNumber.ToString();
    }

    public void changeScore(float newScore)
    {
        _scoreNumber = newScore;
        _scoreText.text = _scoreNumber.ToString();
    }
    public void changeRank(int newRank)
    {
        _rankText.text = newRank.ToString();
    }
}
