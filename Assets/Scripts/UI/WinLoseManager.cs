using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class WinLoseManager : MonoBehaviour
{

    [Header("Object")]
    private CameraController _cameraController;
    private CalenderTimer _calenderTimer;
    private CountryGovernment _countryGovernment;
    [SerializeField] private GameObject _winCanvas;
    [SerializeField] private GameObject _loseCanvas;
    [SerializeField] private TextMeshProUGUI _winDescriptionText;
    [SerializeField] private TextMeshProUGUI _loseDescriptionText;
    [SerializeField] private SceneLoader _sceneLoaderMenu;
    [SerializeField] private bool _isGameOver = false;

    [Header("Description")]
    [TextArea][SerializeField] private string _winDescription = "Your country is the best in the world! no one can stand against you. Baiden Joestar, Vladimier Steputin, Kim Too Um or even King O' cha cannot beat you!";
    [TextArea][SerializeField] private string _loseDescriptionVoting = "You lost against other candinates, You might be better than them but they are better than you in the eyes of others.";
    [TextArea][SerializeField] private string _loseDescriptionOverthrown =  "Everyone hate you, you are the tyrrany in their eyes. Your country is once a beautiful place but it's now a ruin because of you.";
    [TextArea][SerializeField] private string _loseDescriptionNoMoney = "You cannot manage the country's money and now everyone has more country debt than every before. Everyone want you to get out by any mean necessary, it's best that you disappear, for now. . .";

    public string winDescription => _winDescription;
    public string loseDescriptionVoting => _loseDescriptionVoting;
    public string loseDescriptionOverthrown => _loseDescriptionOverthrown;
    public string loseDescriptionNoMoney => _loseDescriptionNoMoney;
    private void Start()
    {
        _winCanvas.SetActive(false);
        _loseCanvas.SetActive(false);

        if (_cameraController == null) _cameraController = FindObjectOfType<CameraController>();
        if (_calenderTimer == null) _calenderTimer = FindObjectOfType<CalenderTimer>();
        if (_sceneLoaderMenu == null) _sceneLoaderMenu = FindObjectOfType<SceneLoader>();
        if (_countryGovernment == null) _countryGovernment = FindObjectOfType<CountryGovernment>();
    }

    private void Update()
    {
        LoseCondition();

    }
    public void GameOver(string _loseDescription)
    {
        _isGameOver = true;
        OpenCloseElectionUIButton();


        _loseCanvas.SetActive(true);
        _loseDescriptionText.text = _loseDescription;

        SoundManager.Instance.ChooseClipBGM(SoundManager.Instance.bgmLosing);
    }
    public void WinGame()
    {
        _isGameOver = true;
        OpenCloseElectionUIButton();


        _winCanvas.SetActive(true);
        _winDescriptionText.text = _winDescription;

        SoundManager.Instance.ChooseClipBGM(SoundManager.Instance.bgmWinning);
    }
    public void loadScene(int SceneNumber)
    {
        _sceneLoaderMenu.startLoadLevel(SceneNumber);
    }
    public void resetScene()
    {
        int SceneNumber = SceneManager.GetActiveScene().buildIndex;

        _sceneLoaderMenu.startLoadLevel(SceneNumber);
    }
    private void LoseCondition()
    {
        if (!_isGameOver)
        {
            if (GameManager.Instance.money <= 0)
            {
                GameOver(loseDescriptionNoMoney);
            }

            if (_countryGovernment.governmentData == null || _countryGovernment.governmentData.hasElection)
            {
                float currentStat = (GameManager.Instance.publicOpinionValue + GameManager.Instance.economyValue + GameManager.Instance.militaryValue) / 3;
                if (currentStat <= (GameManager.Instance.economyData.minimumOrderToLose - (GameManager.Instance.economyData.minimumOrderToLose * 0.5)))
                {
                    GameOver(_loseDescriptionOverthrown);
                }
            }

            if (_countryGovernment.governmentData != null && !_countryGovernment.governmentData.hasElection)
            {
                float currentOrder = (GameManager.Instance.publicOpinionValue + GameManager.Instance.economyValue + GameManager.Instance.militaryValue) / 3;
                if (currentOrder <= GameManager.Instance.economyData.minimumOrderToLose)
                {
                    GameOver(_loseDescriptionOverthrown);
                }
            }
        }
    }
    private void OpenCloseElectionUIButton()
    {
        switch (_isGameOver)
        {
            case true:
                _cameraController.state = CameraController.cameraState.UNMOVEABLE;
                _calenderTimer.changeSpeed(0);
                _calenderTimer.canEditTime = false;

                break;
            case false:
                _cameraController.state = CameraController.cameraState.MOVEABLE;
                _calenderTimer.changeSpeed(1);
                _calenderTimer.canEditTime = true;

                break;
        }
    }
}
