using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    [Header("Menu Canvas List")]
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject mainPauseMenu;
    [SerializeField] private GameObject settingMenu;
    [SerializeField] private SceneLoader sceneLoaderMenu;

    [Header("Audio")]
    [SerializeField] private AudioSource _sfxAudioSource;

    [Header("Resolution")]
    [SerializeField] private ResolutionSetting _ResolutionSetting;

    [Header("Reference")]
    [SerializeField] private GameData _gameData;
    public GameData gameData => _gameData;


    private void Start()
    {
        onStart();
    }
    private void Update()
    {
        if (PlayerInputManager.Instance.EscapePress())
        {
            PauseAndResume();
        }
    }

    private void onStart()
    {
        _ResolutionSetting.dropDownCurrentStartResolution();

        pauseMenu.SetActive(false);
        settingMenu.SetActive(false);
        _gameData.isTransitioning = true;
    }


    public void PauseAndResume()
    {
        if (_gameData.isTransitioning == false)
        {
            if (GameManager.Instance.isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
    }
    private void Pause()
    {
        pauseMenu.SetActive(true);
        mainPauseMenu.SetActive(true);
        GameManager.Instance.isPaused = true;
        Time.timeScale = 0.0f;
    }
    private void Resume()
    {
        Return();
        pauseMenu.SetActive(false);
        GameManager.Instance.isPaused = false;
        Time.timeScale = 1f;
    }
    public void OpenOptions()
    {
        mainPauseMenu.SetActive(false);
        settingMenu.SetActive(true);
    }
    public void Return()
    {
        mainPauseMenu.SetActive(true);
        settingMenu.SetActive(false);
    }
    public void loadScene(int SceneNumber)
    {
        sceneLoaderMenu.startLoadLevel(SceneNumber);
    }
}
