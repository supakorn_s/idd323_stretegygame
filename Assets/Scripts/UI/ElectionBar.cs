using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ElectionBar : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _electionTitleText;
    [SerializeField] private string _electionTitle = "ELECTION IN";
    [SerializeField] private string _noElectionTitle = "COUNTRY ORDER";
    [SerializeField] private Slider _electionSlider;
    [SerializeField] private Image _electionImager;
    [SerializeField] private Gradient electionGradient;

    [SerializeField] private CountryGovernment _countryGovernment;

    private void Start()
    {
        _electionSlider.maxValue = GameManager.Instance.electionEvery;
        if (_countryGovernment == null) _countryGovernment = FindObjectOfType<CountryGovernment>();
    }

    private void Update()
    {
        ElectionUpdate();
    }

    public void ElectionUpdate()
    {
        if (_countryGovernment.governmentData != null)
        {
            _electionTitleText.gameObject.SetActive(true);
            _electionSlider.gameObject.SetActive(true);

            switch (_countryGovernment.governmentData.hasElection)
            {
                case true:
                    _electionSlider.maxValue = GameManager.Instance.electionEvery;

                    _electionTitleText.text = _electionTitle;

                    _electionSlider.value = GameManager.Instance.electDayIn;

                    _electionImager.color = electionGradient.Evaluate(_electionSlider.normalizedValue);

                    break;
                case false:
                    _electionSlider.minValue = GameManager.Instance.economyData.minimumOrderToLose;
                    _electionSlider.maxValue = 100;

                    _electionTitleText.text = _noElectionTitle;

                    float currentOrder = (GameManager.Instance.publicOpinionValue + GameManager.Instance.economyValue + GameManager.Instance.militaryValue) / 3;
                    _electionSlider.value = currentOrder;

                    _electionImager.color = electionGradient.Evaluate(_electionSlider.normalizedValue);
                    break;
            }
        }
        else
        {
            _electionTitleText.gameObject.SetActive(false);
            _electionSlider.gameObject.SetActive(false);
        }
    }
}
