using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CountryStatBarUpdate : MonoBehaviour
{
    [Header("Data Manager")]
    [SerializeField] private EconomyData _economyData;

    [Header("Bar")]
    [SerializeField] private Slider publicOpinionBar;
    [SerializeField] private Image publicOpinionFillRect;
    [SerializeField] private Slider economyBar;
    [SerializeField] private Image economyFillRect;
    [SerializeField] private Slider militaryBar;
    [SerializeField] private Image militaryFillRect;
    [SerializeField] private Gradient statbarGradient;

    [SerializeField] private TextMeshProUGUI POtext;
    [SerializeField] private TextMeshProUGUI ECOtext;
    [SerializeField] private TextMeshProUGUI MILtext;



    private void Start()
    {
        GameManager.Instance.loadCountryStatData(_economyData.publicOpinionValue, _economyData.economyValue, _economyData.militaryValue);
    }
    private void Update()
    {
        updateBar();
    }
    public void updateBar()
    {
        publicOpinionBar.value = GameManager.Instance.publicOpinionValue;
        economyBar.value = GameManager.Instance.economyValue;
        militaryBar.value = GameManager.Instance.militaryValue;

        publicOpinionFillRect.color = statbarGradient.Evaluate(publicOpinionBar.normalizedValue);
        economyFillRect.color = statbarGradient.Evaluate(economyBar.normalizedValue);
        militaryFillRect.color = statbarGradient.Evaluate(militaryBar.normalizedValue);

        POtext.text = ((int)publicOpinionBar.value).ToString();
        ECOtext.text = ((int)economyBar.value).ToString();
        MILtext.text = ((int)militaryBar.value).ToString();
    }
}
