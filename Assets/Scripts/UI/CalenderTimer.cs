using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CalenderTimer : MonoBehaviour
{
    public enum TimerState { PAUSE, NORMAL, DOUBLE, FIVE, TEN };
    [Header("Time State")]
    public TimerState timeState = TimerState.NORMAL;

    [Header("Data Manager")]
    public EconomyData _economyData;
    public CountryEconomy _countryEconomy;
    public CardEventManager _cardEventManager;
    public ElectionManager _electionManager;

    [Header("Date")]
    [SerializeField] private TextMeshProUGUI dayText;
    [SerializeField] private TextMeshProUGUI monthText;
    [SerializeField] private TextMeshProUGUI yearText;


    private float dayCount;
    private int currentSpeed = 1;
    private int finalSpeed = 1;
    [SerializeField] public bool canEditTime = true;

    private void Start()
    {
        dayCount = _economyData.days;

        timeState = TimerState.PAUSE;

        dayText.text = _economyData.days.ToString();
        monthText.text = _economyData.months.ToString();
        yearText.text = _economyData.years.ToString();
        GameManager.Instance.loadDateData(_economyData.days, _economyData.months, _economyData.years);

        if (_cardEventManager == null) _cardEventManager = FindObjectOfType<CardEventManager>();
        if (_electionManager == null) _electionManager = FindObjectOfType<ElectionManager>();
    }
    private void Update()
    {
        pauseButton();
        switch (timeState)
        {
            case TimerState.PAUSE:
                finalSpeed = 0;
                break;
            case TimerState.NORMAL:
                finalSpeed = currentSpeed;
                Timer();
                break;
            case TimerState.DOUBLE:
                finalSpeed = currentSpeed * 2;
                Timer();
                break;
            case TimerState.FIVE:
                finalSpeed = currentSpeed * 5;
                Timer();
                break;
            case TimerState.TEN:
                finalSpeed = currentSpeed * 10;
                Timer();
                break;
        }
    }

    private void Timer()
    {
        dayCount += Time.deltaTime * finalSpeed;
        GameManager.Instance.days = Mathf.FloorToInt(dayCount);

        _cardEventManager.AddDayToEvent(finalSpeed);

        if (GameManager.Instance.days > 30)
        {
            dayCount = 1;
            GameManager.Instance.days = 1;
            GameManager.Instance.months += 1;

            _countryEconomy.EndOfTheMonth();
        }
        if (GameManager.Instance.months > 12)
        {
            GameManager.Instance.months = 1;
            GameManager.Instance.years += 1;
        }
        if (GameManager.Instance.electDayIn <= 0)
        {
            GameManager.Instance.electDayIn = GameManager.Instance.electionEvery;
            _electionManager.EnterElection();
        }
        else
        {
            GameManager.Instance.electDayIn -= Time.deltaTime * finalSpeed;
        }


        dayText.text = GameManager.Instance.days.ToString();
        monthText.text = GameManager.Instance.months.ToString();
        yearText.text = GameManager.Instance.years.ToString();
    }

    public void changeSpeed(int stateValue)
    {
        switch ((TimerState)stateValue)
        {
            case TimerState.PAUSE:
                timeState = TimerState.PAUSE;
                break;
            case TimerState.NORMAL:
                timeState = TimerState.NORMAL;
                break;
            case TimerState.DOUBLE:
                timeState = TimerState.DOUBLE;
                break;
            case TimerState.FIVE:
                timeState = TimerState.FIVE;
                break;
            case TimerState.TEN:
                timeState = TimerState.TEN;
                break;
        }
    }

    private void pauseButton()
    {
        if (PlayerInputManager.Instance.SpaceBarPress() && canEditTime == true)
        {
            if (timeState != TimerState.PAUSE)
            {
                timeState = TimerState.PAUSE;
            }
            else
            {
                timeState = TimerState.NORMAL;
            }
        }
    }
}
