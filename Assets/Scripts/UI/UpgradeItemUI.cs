using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeItemUI : MonoBehaviour
{
    public UpgradeData _upgradeData;
    private StatEffectData _statEffectData;
    private StatEffectData _secondaryEffectData;
    private CountryUpgrade _countryUpgrade;

    // [Header("Upgrade Information")]
    private string _upgradeName;
    private string _upgradeDescription;
    private int _price;
    private Sprite _upgradeSprite;
    private bool purchased = false;

    [Header("Reference Object")]
    private UpgradeUI _upgradeItemUI;
    private CountryUpgrade _CountryUpgrade;
    [SerializeField] private Button buyButton;
    [SerializeField] private TextMeshProUGUI priceTagText;
    [SerializeField] private Image upgradeLogo;

    [Header("Audio")]
    [SerializeField] private AudioSource _sfxSource;

    private void Start()
    {
        _upgradeItemUI = FindObjectOfType<UpgradeUI>();
        _CountryUpgrade = FindObjectOfType<CountryUpgrade>();

        _upgradeName = _upgradeData.upgradeName;
        _upgradeDescription = _upgradeData.upgradeDescription;
        _price = _upgradeData.price;
        _upgradeSprite = _upgradeData.upgradeSprite;
        _statEffectData = _upgradeData.statEffectData;
        _secondaryEffectData = _upgradeData.secondaryStatEffectData;
        _countryUpgrade = _upgradeItemUI.countryUpgrade;

        upgradeLogo.sprite = _upgradeSprite;
        priceTagText.text = _price.ToString("#,#");
    }
    private void Update()
    {
        check();
    }
    private void check()
    {
        switch (purchased)
        {
            case false:
                if (GameManager.Instance.money >= _price)
                {
                    buyButton.interactable = true;
                }
                else
                {
                    buyButton.interactable = false;
                }
                break;
            case true:
                
                break;
        }
    }

    public void buyUpgrade()
    {
        if (GameManager.Instance.money >= _price)
        {
            //buyButton.SetActive(false);
            GameManager.Instance.money -= _price;
            purchased = true;
            buyButton.interactable = false;

            if (_upgradeData.unlockNextTier)
            {
                _countryUpgrade.increaseTier(1);
                _upgradeItemUI.spawnUpgrade();
            }
            if (_upgradeData.openGovernmentTab)
            {
                _upgradeItemUI.governmentTab.SetActive(true);
                _upgradeItemUI.upgradeContentTab.SetActive(false);
                //_upgradeItemUI.OpenCloseUpgradeUIButton();
            }

            _countryUpgrade.insertNewUpgradeData(_upgradeData, _statEffectData);
            if (_secondaryEffectData != null) _countryUpgrade.insertNewEventData(_secondaryEffectData);


            Destroy(gameObject);
        }
    }
    private void changeUpgradeText()
    {
        _upgradeItemUI.changeUpgradeText(_upgradeName, _upgradeDescription);
    }
    private void clearUpgradeText()
    {
        _upgradeItemUI.changeUpgradeText(null, null);
    }

    public void OnHoverUI()
    {
        _sfxSource.PlayOneShot(SoundManager.Instance.hovering);
    }
    public void OnClickUI()
    {
        _sfxSource.PlayOneShot(SoundManager.Instance.clicking);
    }
}
