using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuManager : MonoBehaviour
{
    [Header("Menu Canvas List")]
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject playMenu;
    [SerializeField] private GameObject settingMenu;
    [SerializeField] private GameObject creditMenu;
    [SerializeField] private SceneLoader sceneLoaderMenu;

    [Header("Resolution")]
    [SerializeField] private ResolutionSetting _ResolutionSetting;

    [Header("Audio")]
    [SerializeField] private AudioSource _sfxAudioSource;

    [Header("Data")]
    [SerializeField] private TMP_InputField _newCountryName;
    [SerializeField] private EconomyData _EconomyData;
    [SerializeField] private EconomyData _TutorialData;

    private void Start()
    {
        onStart();
    }

    private void onStart()
    {
        settingMenu.SetActive(true);
        _ResolutionSetting.dropDownCurrentStartResolution();
        mainMenu.SetActive(true);
        settingMenu.SetActive(false);
        creditMenu.SetActive(false);
        playMenu.SetActive(false);
    }
    public void playOpen()
    {
        mainMenu.SetActive(false);
        playMenu.SetActive(true);
    }
    public void settingOpen()
    {
        mainMenu.SetActive(false);
        settingMenu.SetActive(true);
    }
    public void creditOpen()
    {
        mainMenu.SetActive(false);
        creditMenu.SetActive(true);
    }

    public void returnToMenu()
    {
        mainMenu.SetActive(true);
        settingMenu.SetActive(false);
        creditMenu.SetActive(false);
        playMenu.SetActive(false);
    }

    public void loadScene(int SceneNumber)
    {
        sceneLoaderMenu.startLoadLevel(SceneNumber);
    }
    public void exitGame()
    {
        sceneLoaderMenu.exitToDestop();
    }

    public void changeCountryName(int SceneNumber)
    {
        if (_newCountryName.text != "")
        {
            _EconomyData.changeCountryName(_newCountryName.text);
        }
        else
        {
            _EconomyData.changeCountryName(_EconomyData.countryName);
        }

        loadScene(SceneNumber);
    }
    public void loadTutorialStage(int SceneNumber)
    {
        if (_newCountryName.text != "")
        {
            _TutorialData.changeCountryName(_newCountryName.text);
        }
        else
        {
            _TutorialData.changeCountryName(_TutorialData.countryName);
        }

        loadScene(SceneNumber);
    }

}
