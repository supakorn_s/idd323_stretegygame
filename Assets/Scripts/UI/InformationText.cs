using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InformationText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _titleText;
    [SerializeField] private TextMeshProUGUI _valueText;

    [SerializeField] private float _notificationTimer = 6;
    [SerializeField] private Animator _anim;

    private void Start()
    {
        StartCoroutine(textDisappear(_notificationTimer));
    }
    private IEnumerator textDisappear(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        _anim.SetTrigger("Out");
    }
    public void SetText(string title, float newNumber, Color valueColor)
    {
        _titleText.text = title;
        _valueText.text = newNumber.ToString();
        _valueText.color = valueColor;
    }
    public void SetMoneyText(string title, float newNumber, Color valueColor)
    {
        _titleText.text = title;
        _valueText.text = newNumber.ToString("#,#");
        _valueText.color = valueColor;
    }
    public void selfDestroy()
    {
        Destroy(gameObject);
    }

}
