﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CountryAudio_tD98AEE2B6E07CF70DF16AE69E4490E2DF4E53840_CustomAttributesCacheGenerator__soundManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryAudio_tD98AEE2B6E07CF70DF16AE69E4490E2DF4E53840_CustomAttributesCacheGenerator__countryGovernment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSource1(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x53\x6F\x75\x72\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSource2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__sfxSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmMainmenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x47\x4D\x20\x4C\x69\x73\x74\x73"), NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmWinning(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmLosing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmDefault(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x47\x4D\x20\x45\x76\x65\x6E\x74\x73\x20\x4C\x69\x73\x74\x73"), NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmDemocracy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSocialism(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmMonarchy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmDictator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmCommunism(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__currentBGMList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__clicking(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x46\x58\x20\x4C\x69\x73\x74\x73"), NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__hovering(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__popup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__electionVoting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__afterVoting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_soundForce(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x43\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_audioMixer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_slider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_ExposerString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x69\x6E\x67\x20\x53\x6F\x75\x6E\x64"), NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__testSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator__inputManager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator_panSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator_scrollLimitMin(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 50.0f, NULL);
	}
}
static void CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator_scrollLimitMax(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 5.0f, 50.0f, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardEventData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__animator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__openTrigger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__backCard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__frontCard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__originalColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__highlightColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__sfxSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__countryBrain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__gameManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__cameraController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__calenderTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__eventContent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__cardCanvasChild(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__cardPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__returnButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__totalSpawnCard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__eventEvery(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__dayToEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__spawnedCard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__goodPublicEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64\x20\x44\x61\x74\x61"), NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__badPublicEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__goodEconomyEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__badEconomyEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__goodMilitaryEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__badMilitaryEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__enableCheating(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__cheatPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__pauseMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__money(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x61\x74\x69\x6E\x67\x20\x76\x61\x6C\x75\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__moneyText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__moneyText2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__POSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__POSliderText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__ECOSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__ECOSliderText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__MILSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__MILSliderText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__ElectionDayField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShipClass_tCEC33637778AE65ED3C124A8853D8146EA28096F_CustomAttributesCacheGenerator__inputManager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TurretClass_t381B8B951F6379C6BD738D5ADBB5E4E0750A9F21_CustomAttributesCacheGenerator_TurretClass_targetDetectionCoroutine_m633FD800BF36138D7A2B26D8932088823863C0E0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_0_0_0_var), NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9__ctor_mBA51F5C3479716B5B5D1CB4CCE4AE935676E7801(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_IDisposable_Dispose_m13935AB646A11C03594BDF24BE60C20703CC4069(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AD1BFF77A8EAA12CA4A22894B9D76289F3605DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_mA388A93F226960939E6D49C21E9EEF25BC6599E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mFF85508530D8B6F47243884D4293F575601073A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__inputManager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__gameData(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__countryGovernment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__countryUpgrade(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__countryEconomy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator_Regions(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x75\x6E\x74\x72\x79\x20\x42\x72\x61\x69\x6E\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator__countryBrain(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator_DistrictPrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x75\x6E\x74\x72\x79\x20\x44\x69\x73\x74\x72\x69\x63\x74\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator_DistrictData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator_BadDistrictData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__countryBrain(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_incomeValueList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_publicOpinionValueList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_economyBonusList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_MilitaryBonusList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_incomePercentList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_publicPercentList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_economyPercentList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_MilitaryPercentList(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__publicOpinionStatEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x45\x66\x66\x65\x63\x74\x20\x4C\x69\x73\x74\x73"), NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__economyStatEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__militaryStatEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__POEffectInUse(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__ECOEffectInUse(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__MILffectInUse(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tEC5C4C07EE132E4E2EC3788DC946FE9D1C61D093_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentRevenueText(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x54\x65\x78\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentPOText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentECOText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentMILText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void CountryUpgrade_tBC8EE09975B567D6ED594264D41B1922CDF5D5C8_CustomAttributesCacheGenerator__currentUpgradeTier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 20.0f, NULL);
	}
}
static void CountryUpgrade_tBC8EE09975B567D6ED594264D41B1922CDF5D5C8_CustomAttributesCacheGenerator__upgradeData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryUpgrade_tBC8EE09975B567D6ED594264D41B1922CDF5D5C8_CustomAttributesCacheGenerator__statEffectData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x43\x61\x72\x64"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x2F\x43\x61\x72\x64\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__cardName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64\x20\x44\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E"), NULL);
	}
}
static void CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__cardDescription(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__effectDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__statEffectData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__subsStatEffectData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x44\x69\x73\x74\x72\x69\x63\x74\x20\x54\x79\x70\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x2F\x44\x69\x73\x74\x72\x69\x63\x74\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__districtName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x72\x69\x63\x74\x20\x44\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator_districtDescription(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator_districtEffect(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__districtImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isResidential(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x72\x69\x63\x74\x20\x54\x79\x70\x65"), NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isIndustry(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isHarbour(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isCommercial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isBadZone(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__moneyBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x6E\x75\x73\x20\x61\x64\x64\x20\x75\x70"), NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__publicOpinionBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__economyBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__militaryBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__moneyPercent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x20\x62\x6F\x6E\x75\x73\x20\x61\x64\x64\x20\x75\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__publicOpinionPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__economyPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__militaryPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x64\x61\x74\x61\x20\x6D\x61\x6E\x61\x67\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x72\x2F\x45\x63\x6F\x6E\x6F\x6D\x79\x20\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__countryName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x69\x6E\x67\x20\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__startingMoney(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__days(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__months(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__years(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__electionEvery(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__publicOpinionValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__economyValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__militaryValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__minimumOrderToLose(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__incomeBase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x73\x65\x20\x43\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__incomeMultiplying(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__outcomeBase(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__endOfTheMonthDate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__publicOpinionDecayValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 0.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x63\x61\x79\x20\x56\x61\x6C\x75\x65\x20\x50\x65\x72\x20\x4D\x6F\x6E\x74\x68"), NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__economyDecayValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 0.0f, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__militaryDecayValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 0.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__veryBad(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x45\x66\x66\x65\x63\x74\x20\x73\x74\x61\x74\x75\x73\x20\x69\x6E\x20\x70\x65\x72\x63\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__bad(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__normal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__good(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__veryGood(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__goodDistrictRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x72\x69\x63\x74\x20\x52\x61\x74\x65"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator_currentMoney(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator_monthlyIncome(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator_monthlyOutcome(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__cardEventEvery(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64\x20\x45\x76\x65\x6E\x74\x20\x52\x61\x74\x65"), NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__goodEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__badEvent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__minimumElectionValue(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6C\x65\x63\x74\x69\x6F\x6E\x20\x52\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__maximumElectionValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__electionBaseChance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__electionDivider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__votingResultDivider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__votingCheatMoneyPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__votingCheatMoneyMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__beforeVotingDivider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__afterVotingDivider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__upgradeMultiply(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x6F\x72\x65\x20\x43\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__publicMultiply(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__economyMultiply(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__militaryMultiply(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__moneyDivider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__badPublicMultiply(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameData_tBB3875760FC6FBB28B9D4694716D4C0E19EED1CD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x64\x61\x74\x61\x20\x6D\x61\x6E\x61\x67\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x72\x2F\x47\x61\x6D\x65\x20\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void GameData_tBB3875760FC6FBB28B9D4694716D4C0E19EED1CD_CustomAttributesCacheGenerator_countryName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x69\x73\x74\x69\x63"), NULL);
	}
}
static void GameData_tBB3875760FC6FBB28B9D4694716D4C0E19EED1CD_CustomAttributesCacheGenerator_isTransitioning(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__countryName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x69\x73\x74\x69\x63"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__publicOpinionValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__economyValue(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__militaryValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 100.0f, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__electionStackupChance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__economyData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_isPaused(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_electionEvery(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x47\x6F\x76\x65\x72\x6E\x6D\x65\x6E\x74\x20\x54\x79\x70\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x2F\x47\x6F\x76\x65\x72\x6E\x6D\x65\x6E\x74\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__systemName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x6F\x76\x65\x72\x6E\x6D\x65\x6E\x74\x20\x44\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__governmentSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__governmentPassiveStat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__hasElection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x69\x6E\x67\x20\x42\x6F\x6E\x75\x73"), NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__StartingFund(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__publicOpinionValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__economyStrengthValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__militaryStrengthValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x74\x61\x74\x20\x45\x66\x66\x65\x63\x74\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x2F\x53\x74\x61\x74\x20\x45\x66\x66\x65\x63\x74\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__perMonth(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x63\x68\x65\x63\x6B\x20\x74\x6F\x20\x61\x64\x64\x20\x62\x6F\x6E\x75\x73\x20\x6F\x6E\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__moneyBonus(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x6E\x75\x73\x20\x61\x64\x64\x20\x75\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__publicOpinionBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__economyBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__militaryBonus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__moneyPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x20\x62\x6F\x6E\x75\x73\x20\x61\x64\x64\x20\x75\x70"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__publicOpinionPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__economyPercent(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__militaryPercent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__electionPercent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x63\x65\x6E\x74\x20\x65\x76\x65\x6E\x74\x20\x62\x6F\x6E\x75\x73\x20\x61\x64\x64\x20\x75\x70"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__eventPercent(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -1.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x55\x70\x67\x72\x61\x64\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x2F\x55\x70\x67\x72\x61\x64\x65\x20\x44\x61\x74\x61"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x41\x74\x74\x72\x69\x62\x75\x74\x65"), NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 5LL, 20LL, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__price(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__statEffectData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__secondaryStatEffectData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeTier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 20.0f, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__unlockNextTier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__openGovernmentTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__districtData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator_sRenderer(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__informationObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__informationText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__badDText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__ComDText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__HarDText(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__IndDText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__ResDText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void DistrictClass_t36C96854B16828EB2BE99285487F1193F643010A_CustomAttributesCacheGenerator__inputManager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionContent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6C\x65\x63\x74\x69\x6F\x6E\x20\x43\x6F\x6E\x74\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__useMoneyButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__startElectionButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__closeButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__useMoneyText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__cheatingPrice(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x76\x65\x72\x61\x6C\x6C\x20\x45\x6C\x65\x63\x74\x69\x6F\x6E\x20\x53\x6C\x69\x64\x65\x72"), NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionImager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionGradient(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionPercentage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultSlider(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x74\x69\x6E\x67\x20\x52\x65\x73\x75\x6C\x74\x20\x53\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultImager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultGradient(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultPercentage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__startingElection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__isElectionCompleted(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__sfxSource(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__votingSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__countryGovernment(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__countryEconomy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__CountryGovernment(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__governmentData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__governmentSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__systemNameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__StartingFundText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__publicOpinionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__economyStrengthText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__militaryStrengthText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__electionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__normalColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__buffColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__debuffColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator__inputManager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator_shipName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x69\x70\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator_hitPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator_weaponSlot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurretBrain_t3272E825BC1E890CA5EA6CDF25CA4943FA1CE699_CustomAttributesCacheGenerator_turretName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x75\x72\x72\x65\x74\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void TurretBrain_t3272E825BC1E890CA5EA6CDF25CA4943FA1CE699_CustomAttributesCacheGenerator_isEnemy(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x63\x74\x69\x6F\x6E"), NULL);
	}
}
static void TurretController_t27CB355B33C1DFEE9C98335E8E7EECEBC7335154_CustomAttributesCacheGenerator_turretRayStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x79\x63\x61\x73\x74"), NULL);
	}
}
static void TurretController_t27CB355B33C1DFEE9C98335E8E7EECEBC7335154_CustomAttributesCacheGenerator_TurretController_targetDetectionCoroutine_m0863CC446CC1B4BF7AD0E35EF880D35D140406B5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_0_0_0_var), NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6__ctor_mC8B7A8B1CED73E7F9F6B502438360E6952DA2248(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_IDisposable_Dispose_mB9DC26CED4951C9A00BA13465A551E3B28B6D602(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC31340DA08D735F8804197AB7C57EF0DD5C47122(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_m944CD9F92A9C4868E8D50FC36DF765670E37178C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m4C82B1B46097F2A043D70F19EBFCCA082A9BD3EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RegionBrain_t72C592251CE10CE80A5A21161C0FD66B7110AAF7_CustomAttributesCacheGenerator__inputManager(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void RegionBrain_t72C592251CE10CE80A5A21161C0FD66B7110AAF7_CustomAttributesCacheGenerator_originalColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RegionBrain_t72C592251CE10CE80A5A21161C0FD66B7110AAF7_CustomAttributesCacheGenerator_highlightColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__TutorialGameObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__activeTutorialUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__calenderTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__cameraController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__cardEventManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__winLoseManager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__raycastBlocker(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__countryStatBarTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__CalenderTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__FundTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__rankingButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__upgradeButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__UpgradeContent(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TutorialTextBrain_t3371AD3EBB17A96401151A6502D481E4104FF865_CustomAttributesCacheGenerator__textbox(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_timeState(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x20\x53\x74\x61\x74\x65"), NULL);
	}
}
static void CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator__economyData(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_dayText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_monthText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_yearText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_canEditTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__countryName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__rankText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__scoreText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__rankNumber(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__scoreNumber(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__backPlate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__playerHighlight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__cameraController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61"), NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__calenderTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__rankingCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__winButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__objectToHide(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__rankingCanvasIsOpen(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryPrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x75\x6E\x74\x72\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__contentCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryToSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__playerCountryBrain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryname(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryRankBrain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__scoreList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryscore(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryUpgrade(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x6F\x72\x65\x20\x63\x61\x6C\x63\x75\x6C\x61\x74\x69\x6F\x6E\x20\x56\x61\x72\x69\x61\x62\x6C\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator__economyData(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_publicOpinionBar(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_publicOpinionFillRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_economyBar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_economyFillRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_militaryBar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_militaryFillRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_statbarGradient(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_POtext(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_ECOtext(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_MILtext(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionTitleText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionTitle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__noElectionTitle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionImager(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator_electionGradient(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__countryGovernment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__titleText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__valueText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__notificationTimer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__anim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator_InformationText_textDisappear_mF0444AE73D21EBFA1564E06201BD84889D929635(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_0_0_0_var), NULL);
	}
}
static void U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5__ctor_m8DFA67F978C62965EFC1AF45AC6E991523C98153(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_IDisposable_Dispose_mE481914892D7D05A8CC111A5FFE4C9D7CD39B455(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2C8295679F0BC19976D82D542416718DEDDF25A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_Reset_m981818C756CF88BE287D53C3CE979E0A64036394(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_get_Current_mC8F4D7976B07AFEA6EB40BBC5577579B62F135A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__canvasChildPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__informationTextPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__spawnTitleColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__spawnPositiveDescriptionColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__spawnNegativeDescriptionColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_mainMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6E\x75\x20\x43\x61\x6E\x76\x61\x73\x20\x4C\x69\x73\x74"), NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_playMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_settingMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_creditMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_sceneLoaderMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__ResolutionSetting(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__sfxAudioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__newCountryName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__EconomyData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__TutorialData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_pauseMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6E\x75\x20\x43\x61\x6E\x76\x61\x73\x20\x4C\x69\x73\x74"), NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_mainPauseMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_settingMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_sceneLoaderMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator__sfxAudioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator__ResolutionSetting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E"), NULL);
	}
}
static void PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator__gameData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65"), NULL);
	}
}
static void ResolutionSetting_t65491F1B4FCB2E7328CED93748384ADF87E72663_CustomAttributesCacheGenerator_resolutionDropDown(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transitiontime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_0_0_0_var), NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_0_0_0_var), NULL);
	}
}
static void U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6__ctor_m9E1A61EAB40A3F7E55748634CF1479A83D7F0918(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_IDisposable_Dispose_m3843887F758EDED505C5740E27804345C91A4EA5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66A182F82237A6982B1215DFBC583137A405D67B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_Collections_IEnumerator_Reset_mC33203E5184B62410066554665A683CBBA2473D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_Collections_IEnumerator_get_Current_m42EAEC5702621CBB823CE3BC0BB3AAA3FCD734D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7__ctor_m34E1725C8F3B9C5DD0D1FDDF031FFA605C0B536E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_IDisposable_Dispose_m215E49BCB1D7D0EFBC5830D1BD850C61C13AF9E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9120CA81B69CCFA43D38D74EE44FE8D7A435F707(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_Collections_IEnumerator_Reset_mDCFD249F2AF563D7D20D1091C1772940EA653B3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_Collections_IEnumerator_get_Current_m9807A48ED782BD25BC3E49EA47642C45B903ABEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SceneLoaderCheck_tA0FA9E713AD3360E9B61A13AEB7228C7A8E297B9_CustomAttributesCacheGenerator__gameData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator__upgradeItemUI(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x4F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator_buyButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator_priceTagText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator_upgradeLogo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator__sfxSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__upgradeContentTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x76\x61\x73\x20\x54\x61\x62"), NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__governmentTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__upgradeCanvasChild(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__upgradePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator_upgradeTitleText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x54\x65\x78\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator_upgradeDescriptionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier0(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x4F\x70\x74\x69\x6F\x6E\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier1(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier3(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier4(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier5(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__cameraController(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winDescriptionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__sceneLoaderMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__isGameOver(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E"), NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[2];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionVoting(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionOverthrown(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionNoMoney(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void moneyUpdate_t614B8E7F089DAF8A3675EACE211727E7E72E505A_CustomAttributesCacheGenerator__economyData(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x61\x74\x61\x20\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void moneyUpdate_t614B8E7F089DAF8A3675EACE211727E7E72E505A_CustomAttributesCacheGenerator_moneyText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void moneyUpdate_t614B8E7F089DAF8A3675EACE211727E7E72E505A_CustomAttributesCacheGenerator_countryNameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[429] = 
{
	U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator,
	U3CU3Ec_tEC5C4C07EE132E4E2EC3788DC946FE9D1C61D093_CustomAttributesCacheGenerator,
	CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator,
	GameData_tBB3875760FC6FBB28B9D4694716D4C0E19EED1CD_CustomAttributesCacheGenerator,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator,
	U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator,
	U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator,
	U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator,
	U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator,
	Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField,
	CountryAudio_tD98AEE2B6E07CF70DF16AE69E4490E2DF4E53840_CustomAttributesCacheGenerator__soundManager,
	CountryAudio_tD98AEE2B6E07CF70DF16AE69E4490E2DF4E53840_CustomAttributesCacheGenerator__countryGovernment,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSource1,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSource2,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__sfxSource,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmMainmenu,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmWinning,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmLosing,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmDefault,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmDemocracy,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSocialism,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmMonarchy,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmDictator,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmCommunism,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__currentBGMList,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__clicking,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__hovering,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__popup,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__electionVoting,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__afterVoting,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_soundForce,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_audioMixer,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_slider,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_ExposerString,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__audioSource,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__testSound,
	CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator__inputManager,
	CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator_panSpeed,
	CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator_scrollLimitMin,
	CameraController_tCFCE51ADE50A46097682FD3E2CEA53100D84E7E0_CustomAttributesCacheGenerator_scrollLimitMax,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardEventData,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__animator,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__openTrigger,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardName,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardDescription,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__cardEffect,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__backCard,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__frontCard,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__originalColor,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__highlightColor,
	CardEventBrain_t9EA48B5D2B41988DF5AB62BD4515F53B53FB4B55_CustomAttributesCacheGenerator__sfxSource,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__countryBrain,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__gameManager,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__cameraController,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__calenderTimer,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__eventContent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__cardCanvasChild,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__cardPrefab,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__returnButton,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__totalSpawnCard,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__eventEvery,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__dayToEvent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__spawnedCard,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__goodPublicEvent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__badPublicEvent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__goodEconomyEvent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__badEconomyEvent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__goodMilitaryEvent,
	CardEventManager_tCB7A7D06C3E20D39DC8FCEB79D88590C7AD51EED_CustomAttributesCacheGenerator__badMilitaryEvent,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__enableCheating,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__cheatPanel,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__pauseMenu,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__money,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__moneyText,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__moneyText2,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__POSlider,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__POSliderText,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__ECOSlider,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__ECOSliderText,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__MILSlider,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__MILSliderText,
	Cheating_t515B7B2959EC3DF1706B46D85FCD769663654DFC_CustomAttributesCacheGenerator__ElectionDayField,
	ShipClass_tCEC33637778AE65ED3C124A8853D8146EA28096F_CustomAttributesCacheGenerator__inputManager,
	CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__inputManager,
	CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__gameData,
	CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__countryGovernment,
	CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__countryUpgrade,
	CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator__countryEconomy,
	CountryBrain_t24E5EDA16B6DBC1C697EA9B8ACC2EEBF96ACD0B5_CustomAttributesCacheGenerator_Regions,
	CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator__countryBrain,
	CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator_DistrictPrefab,
	CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator_DistrictData,
	CountryDistrict_tE3FDF1E2CB3CBF56079100CC2D4341B299761260_CustomAttributesCacheGenerator_BadDistrictData,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__countryBrain,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_incomeValueList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_publicOpinionValueList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_economyBonusList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_MilitaryBonusList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_incomePercentList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_publicPercentList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_economyPercentList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator_MilitaryPercentList,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__publicOpinionStatEffect,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__economyStatEffect,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__militaryStatEffect,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__POEffectInUse,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__ECOEffectInUse,
	CountryEconomy_t408E15A900D06A327F0C19A6630AC921D87D5997_CustomAttributesCacheGenerator__MILffectInUse,
	CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentRevenueText,
	CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentPOText,
	CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentECOText,
	CountryGovernment_t650F7C723CA53A400BCDBBD5BC2DAE413E4F8EAB_CustomAttributesCacheGenerator__changeGovernmentMILText,
	CountryUpgrade_tBC8EE09975B567D6ED594264D41B1922CDF5D5C8_CustomAttributesCacheGenerator__currentUpgradeTier,
	CountryUpgrade_tBC8EE09975B567D6ED594264D41B1922CDF5D5C8_CustomAttributesCacheGenerator__upgradeData,
	CountryUpgrade_tBC8EE09975B567D6ED594264D41B1922CDF5D5C8_CustomAttributesCacheGenerator__statEffectData,
	CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__cardName,
	CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__cardDescription,
	CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__effectDescription,
	CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__statEffectData,
	CardEventData_t6B481FB5EE260A6A028AC54FC3CFEB0C1A3CBC74_CustomAttributesCacheGenerator__subsStatEffectData,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__districtName,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator_districtDescription,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator_districtEffect,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__districtImage,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isResidential,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isIndustry,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isHarbour,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isCommercial,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__isBadZone,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__moneyBonus,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__publicOpinionBonus,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__economyBonus,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__militaryBonus,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__moneyPercent,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__publicOpinionPercent,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__economyPercent,
	DistrictData_t45A5DF68CE0A9C31DEC417341F30BABF5899CE30_CustomAttributesCacheGenerator__militaryPercent,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__countryName,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__startingMoney,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__days,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__months,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__years,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__electionEvery,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__publicOpinionValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__economyValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__militaryValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__minimumOrderToLose,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__incomeBase,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__incomeMultiplying,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__outcomeBase,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__endOfTheMonthDate,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__publicOpinionDecayValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__economyDecayValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__militaryDecayValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__veryBad,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__bad,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__normal,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__good,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__veryGood,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__goodDistrictRate,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator_currentMoney,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator_monthlyIncome,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator_monthlyOutcome,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__cardEventEvery,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__goodEvent,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__badEvent,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__minimumElectionValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__maximumElectionValue,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__electionBaseChance,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__electionDivider,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__votingResultDivider,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__votingCheatMoneyPercent,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__votingCheatMoneyMultiplier,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__beforeVotingDivider,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__afterVotingDivider,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__upgradeMultiply,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__publicMultiply,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__economyMultiply,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__militaryMultiply,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__moneyDivider,
	EconomyData_t0323E5E80159988CCA4E76513B137C3BF4DD7A54_CustomAttributesCacheGenerator__badPublicMultiply,
	GameData_tBB3875760FC6FBB28B9D4694716D4C0E19EED1CD_CustomAttributesCacheGenerator_countryName,
	GameData_tBB3875760FC6FBB28B9D4694716D4C0E19EED1CD_CustomAttributesCacheGenerator_isTransitioning,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__countryName,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__publicOpinionValue,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__economyValue,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__militaryValue,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__electionStackupChance,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator__economyData,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_isPaused,
	GameManager_t9013B33302D2B40A51D0E8059DEE0DC180218AA1_CustomAttributesCacheGenerator_electionEvery,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__systemName,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__governmentSprite,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__governmentPassiveStat,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__hasElection,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__StartingFund,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__publicOpinionValue,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__economyStrengthValue,
	GovernmentData_t4040BCD5970B33B40CBAA92FDB4C179ED2FD4843_CustomAttributesCacheGenerator__militaryStrengthValue,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__perMonth,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__moneyBonus,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__publicOpinionBonus,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__economyBonus,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__militaryBonus,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__moneyPercent,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__publicOpinionPercent,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__economyPercent,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__militaryPercent,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__electionPercent,
	StatEffectData_tF7A97F89B2B2973E025B51871CB87A2AEEF86DC7_CustomAttributesCacheGenerator__eventPercent,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeName,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeDescription,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__price,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeSprite,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__statEffectData,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__secondaryStatEffectData,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__upgradeTier,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__unlockNextTier,
	UpgradeData_tA7FCF9E7AD73792EF701A4A79807961C7C6113BC_CustomAttributesCacheGenerator__openGovernmentTab,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__districtData,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator_sRenderer,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__informationObject,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__informationText,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__badDText,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__ComDText,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__HarDText,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__IndDText,
	DistrictBrain_tA98E0941CAC872BD5044CBD7D99496258598E32A_CustomAttributesCacheGenerator__ResDText,
	DistrictClass_t36C96854B16828EB2BE99285487F1193F643010A_CustomAttributesCacheGenerator__inputManager,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionContent,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__useMoneyButton,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__startElectionButton,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__closeButton,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__useMoneyText,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__cheatingPrice,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionSlider,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionImager,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionGradient,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__electionPercentage,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultSlider,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultImager,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultGradient,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__resultPercentage,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__startingElection,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__isElectionCompleted,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__sfxSource,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__votingSound,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__countryGovernment,
	ElectionManager_tB257F38EC137B451E366E72F01011135830752FA_CustomAttributesCacheGenerator__countryEconomy,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__CountryGovernment,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__governmentData,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__governmentSprite,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__systemNameText,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__StartingFundText,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__publicOpinionText,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__economyStrengthText,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__militaryStrengthText,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__electionText,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__normalColor,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__buffColor,
	GovernmentBrain_t1C60098A8DF781581CA314F8DACC926831597181_CustomAttributesCacheGenerator__debuffColor,
	ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator__inputManager,
	ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator_shipName,
	ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator_hitPoint,
	ShipBrain_t4B260632AC3F09CD6E329EC32EB6282FACCAD968_CustomAttributesCacheGenerator_weaponSlot,
	TurretBrain_t3272E825BC1E890CA5EA6CDF25CA4943FA1CE699_CustomAttributesCacheGenerator_turretName,
	TurretBrain_t3272E825BC1E890CA5EA6CDF25CA4943FA1CE699_CustomAttributesCacheGenerator_isEnemy,
	TurretController_t27CB355B33C1DFEE9C98335E8E7EECEBC7335154_CustomAttributesCacheGenerator_turretRayStart,
	RegionBrain_t72C592251CE10CE80A5A21161C0FD66B7110AAF7_CustomAttributesCacheGenerator__inputManager,
	RegionBrain_t72C592251CE10CE80A5A21161C0FD66B7110AAF7_CustomAttributesCacheGenerator_originalColor,
	RegionBrain_t72C592251CE10CE80A5A21161C0FD66B7110AAF7_CustomAttributesCacheGenerator_highlightColor,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__TutorialGameObject,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__activeTutorialUI,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__calenderTimer,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__cameraController,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__cardEventManager,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__winLoseManager,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__raycastBlocker,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__countryStatBarTab,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__CalenderTab,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__FundTab,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__rankingButton,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__upgradeButton,
	TutorialManager_t455EC239F2DA229A1C6877BD092D9E686F7D1C58_CustomAttributesCacheGenerator__UpgradeContent,
	TutorialTextBrain_t3371AD3EBB17A96401151A6502D481E4104FF865_CustomAttributesCacheGenerator__textbox,
	CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_timeState,
	CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator__economyData,
	CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_dayText,
	CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_monthText,
	CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_yearText,
	CalenderTimer_t52DF624337A691A7EE5895A8EEDFA2A8CB9208B7_CustomAttributesCacheGenerator_canEditTime,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__countryName,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__rankText,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__scoreText,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__rankNumber,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__scoreNumber,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__backPlate,
	CountryRankBrain_tC92896630F56D90CFB95AB32C7045372B38C489D_CustomAttributesCacheGenerator__playerHighlight,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__cameraController,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__calenderTimer,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__rankingCanvas,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__winButton,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__objectToHide,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__rankingCanvasIsOpen,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryPrefab,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__contentCanvas,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryToSpawn,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__playerCountryBrain,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryname,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryRankBrain,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__scoreList,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryscore,
	CountryRankManager_t34BDB1D5A883A30DBD8EF8C8AA2C4DB90F5AC0A5_CustomAttributesCacheGenerator__countryUpgrade,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator__economyData,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_publicOpinionBar,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_publicOpinionFillRect,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_economyBar,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_economyFillRect,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_militaryBar,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_militaryFillRect,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_statbarGradient,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_POtext,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_ECOtext,
	CountryStatBarUpdate_t4C765590C43DF079893526881BF8274CF4C939C9_CustomAttributesCacheGenerator_MILtext,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionTitleText,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionTitle,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__noElectionTitle,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionSlider,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__electionImager,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator_electionGradient,
	ElectionBar_tBF1CD55ED137CFA35D38957A28D72A64A24F7678_CustomAttributesCacheGenerator__countryGovernment,
	InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__titleText,
	InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__valueText,
	InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__notificationTimer,
	InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator__anim,
	InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__canvasChildPrefab,
	InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__informationTextPrefab,
	InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__spawnTitleColor,
	InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__spawnPositiveDescriptionColor,
	InformationbarUpdate_tDD55CFF69CF436397397F30C4DFB972845D17D98_CustomAttributesCacheGenerator__spawnNegativeDescriptionColor,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_mainMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_playMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_settingMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_creditMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator_sceneLoaderMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__ResolutionSetting,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__sfxAudioSource,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__newCountryName,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__EconomyData,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__TutorialData,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_pauseMenu,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_mainPauseMenu,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_settingMenu,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator_sceneLoaderMenu,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator__sfxAudioSource,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator__ResolutionSetting,
	PauseMenu_tA57AC8D7056D427531596655447E6DDDBB7DB791_CustomAttributesCacheGenerator__gameData,
	ResolutionSetting_t65491F1B4FCB2E7328CED93748384ADF87E72663_CustomAttributesCacheGenerator_resolutionDropDown,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transition,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_image,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transitiontime,
	SceneLoaderCheck_tA0FA9E713AD3360E9B61A13AEB7228C7A8E297B9_CustomAttributesCacheGenerator__gameData,
	UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator__upgradeItemUI,
	UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator_buyButton,
	UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator_priceTagText,
	UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator_upgradeLogo,
	UpgradeItemUI_t1489824047A7EDBB3282D1BDDF4C7CC4BCEBD0FA_CustomAttributesCacheGenerator__sfxSource,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__upgradeContentTab,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__governmentTab,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__upgradeCanvasChild,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__upgradePrefab,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator_upgradeTitleText,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator_upgradeDescriptionText,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier0,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier1,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier2,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier3,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier4,
	UpgradeUI_t8725F8EDF94D124DDDCEB3CBB7498061D36AB8E9_CustomAttributesCacheGenerator__UpgradeTier5,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__cameraController,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winCanvas,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseCanvas,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winDescriptionText,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionText,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__sceneLoaderMenu,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__isGameOver,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winDescription,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionVoting,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionOverthrown,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__loseDescriptionNoMoney,
	moneyUpdate_t614B8E7F089DAF8A3675EACE211727E7E72E505A_CustomAttributesCacheGenerator__economyData,
	moneyUpdate_t614B8E7F089DAF8A3675EACE211727E7E72E505A_CustomAttributesCacheGenerator_moneyText,
	moneyUpdate_t614B8E7F089DAF8A3675EACE211727E7E72E505A_CustomAttributesCacheGenerator_countryNameText,
	Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF,
	TurretClass_t381B8B951F6379C6BD738D5ADBB5E4E0750A9F21_CustomAttributesCacheGenerator_TurretClass_targetDetectionCoroutine_m633FD800BF36138D7A2B26D8932088823863C0E0,
	U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9__ctor_mBA51F5C3479716B5B5D1CB4CCE4AE935676E7801,
	U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_IDisposable_Dispose_m13935AB646A11C03594BDF24BE60C20703CC4069,
	U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AD1BFF77A8EAA12CA4A22894B9D76289F3605DA,
	U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_mA388A93F226960939E6D49C21E9EEF25BC6599E2,
	U3CtargetDetectionCoroutineU3Ed__9_tFD4D6B030B38EA81E865E3A48E1145D3B795865B_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mFF85508530D8B6F47243884D4293F575601073A5,
	TurretController_t27CB355B33C1DFEE9C98335E8E7EECEBC7335154_CustomAttributesCacheGenerator_TurretController_targetDetectionCoroutine_m0863CC446CC1B4BF7AD0E35EF880D35D140406B5,
	U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6__ctor_mC8B7A8B1CED73E7F9F6B502438360E6952DA2248,
	U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_IDisposable_Dispose_mB9DC26CED4951C9A00BA13465A551E3B28B6D602,
	U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC31340DA08D735F8804197AB7C57EF0DD5C47122,
	U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_m944CD9F92A9C4868E8D50FC36DF765670E37178C,
	U3CtargetDetectionCoroutineU3Ed__6_tA7BDAF107FB1BFC172B9056258E277B3DE30BF59_CustomAttributesCacheGenerator_U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m4C82B1B46097F2A043D70F19EBFCCA082A9BD3EB,
	InformationText_tAB208D1066C09064EDEADA60A8DF59E82629AF1A_CustomAttributesCacheGenerator_InformationText_textDisappear_mF0444AE73D21EBFA1564E06201BD84889D929635,
	U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5__ctor_m8DFA67F978C62965EFC1AF45AC6E991523C98153,
	U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_IDisposable_Dispose_mE481914892D7D05A8CC111A5FFE4C9D7CD39B455,
	U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2C8295679F0BC19976D82D542416718DEDDF25A,
	U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_Reset_m981818C756CF88BE287D53C3CE979E0A64036394,
	U3CtextDisappearU3Ed__5_t327A0A5394B7ED8B117BBDC6642DA3950251AA99_CustomAttributesCacheGenerator_U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_get_Current_mC8F4D7976B07AFEA6EB40BBC5577579B62F135A6,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36,
	U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6__ctor_m9E1A61EAB40A3F7E55748634CF1479A83D7F0918,
	U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_IDisposable_Dispose_m3843887F758EDED505C5740E27804345C91A4EA5,
	U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66A182F82237A6982B1215DFBC583137A405D67B,
	U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_Collections_IEnumerator_Reset_mC33203E5184B62410066554665A683CBBA2473D2,
	U3CloadlevelU3Ed__6_t87763B2C76A48B7CDD2AC2982BAAC1B9D973599C_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__6_System_Collections_IEnumerator_get_Current_m42EAEC5702621CBB823CE3BC0BB3AAA3FCD734D0,
	U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7__ctor_m34E1725C8F3B9C5DD0D1FDDF031FFA605C0B536E,
	U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_IDisposable_Dispose_m215E49BCB1D7D0EFBC5830D1BD850C61C13AF9E0,
	U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9120CA81B69CCFA43D38D74EE44FE8D7A435F707,
	U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_Collections_IEnumerator_Reset_mDCFD249F2AF563D7D20D1091C1772940EA653B3B,
	U3CexitGameU3Ed__7_t1978E7945D4464AB5F95EE288B6F3D4B1260065B_CustomAttributesCacheGenerator_U3CexitGameU3Ed__7_System_Collections_IEnumerator_get_Current_m9807A48ED782BD25BC3E49EA47642C45B903ABEA,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
