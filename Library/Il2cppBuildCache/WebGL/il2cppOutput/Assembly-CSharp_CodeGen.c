﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.InputSystem.InputActionAsset Input_master::get_asset()
extern void Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF (void);
// 0x00000002 System.Void Input_master::.ctor()
extern void Input_master__ctor_m05570375675155D2756C12748D40E3A55E995118 (void);
// 0x00000003 System.Void Input_master::Dispose()
extern void Input_master_Dispose_m4E1AB80951FBD83DE2BBD7152B250AD5B62DD907 (void);
// 0x00000004 System.Nullable`1<UnityEngine.InputSystem.InputBinding> Input_master::get_bindingMask()
extern void Input_master_get_bindingMask_mA351518215DD2A46E0AC3B43C4F21E07FF4D6DAB (void);
// 0x00000005 System.Void Input_master::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void Input_master_set_bindingMask_m47CCB6C56E9964B4F39B0C3D83AD6FBFBACCD5DA (void);
// 0x00000006 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> Input_master::get_devices()
extern void Input_master_get_devices_mC144889E948D43D5E0BE4762C1897D94CACAEDE8 (void);
// 0x00000007 System.Void Input_master::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void Input_master_set_devices_mCE7B8BD9B4FA936178C0A7A5012C01D6227BD8C9 (void);
// 0x00000008 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> Input_master::get_controlSchemes()
extern void Input_master_get_controlSchemes_mA89274C4467E476A0A54E36E5AE827B527453038 (void);
// 0x00000009 System.Boolean Input_master::Contains(UnityEngine.InputSystem.InputAction)
extern void Input_master_Contains_m3B11EA2D2A14801BD2ECD5CE5FDF548B82EFFCC3 (void);
// 0x0000000A System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> Input_master::GetEnumerator()
extern void Input_master_GetEnumerator_m576439B219C6489068DF54CD3A888D911AF81BD4 (void);
// 0x0000000B System.Collections.IEnumerator Input_master::System.Collections.IEnumerable.GetEnumerator()
extern void Input_master_System_Collections_IEnumerable_GetEnumerator_m5C5C020C36FECC05CBA84F4112857A516B18059C (void);
// 0x0000000C System.Void Input_master::Enable()
extern void Input_master_Enable_mAA5A4A5A3B5048D37FE1BE9BE3E654439F40771D (void);
// 0x0000000D System.Void Input_master::Disable()
extern void Input_master_Disable_m375CAC17441BA7AEFDE01B73AC869E9974E00A6A (void);
// 0x0000000E Input_master/PlayerActions Input_master::get_Player()
extern void Input_master_get_Player_mC7F6E1D47AB6D2D60FC4C5397098EB159F4660B9 (void);
// 0x0000000F System.Void Input_master/PlayerActions::.ctor(Input_master)
extern void PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621 (void);
// 0x00000010 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_CameraMovement()
extern void PlayerActions_get_CameraMovement_m9537EE57866532B0778B7AFEBCE6DACEA9288508 (void);
// 0x00000011 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_MouseDelta()
extern void PlayerActions_get_MouseDelta_m392EE5FB891CDA0EB030C4F407DA0F4A0AE67194 (void);
// 0x00000012 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_MousePosition()
extern void PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26 (void);
// 0x00000013 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_MouseScroll()
extern void PlayerActions_get_MouseScroll_m988F2A5C3F04837EAA4D88F052D7870DEC3F6B31 (void);
// 0x00000014 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_LeftClick()
extern void PlayerActions_get_LeftClick_mBA9C4A91D948482870A5B3D6F299F1C2BEF78B23 (void);
// 0x00000015 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_RightClick()
extern void PlayerActions_get_RightClick_m613ED5127E32C7A730EA7B303B37CFD6E5B2A402 (void);
// 0x00000016 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Escape()
extern void PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD (void);
// 0x00000017 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Spacebar()
extern void PlayerActions_get_Spacebar_m70DE77C67F95D3504D5250C5BC4299D65E907C7E (void);
// 0x00000018 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_F1Cheat()
extern void PlayerActions_get_F1Cheat_mC8E401D56F3F86E371011488053C2B0D46393FFA (void);
// 0x00000019 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_TutorialNext()
extern void PlayerActions_get_TutorialNext_m5D0CA16ABE26948ECDF5421ABE9F6E9F304C9FE3 (void);
// 0x0000001A UnityEngine.InputSystem.InputActionMap Input_master/PlayerActions::Get()
extern void PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8 (void);
// 0x0000001B System.Void Input_master/PlayerActions::Enable()
extern void PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C (void);
// 0x0000001C System.Void Input_master/PlayerActions::Disable()
extern void PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9 (void);
// 0x0000001D System.Boolean Input_master/PlayerActions::get_enabled()
extern void PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7 (void);
// 0x0000001E UnityEngine.InputSystem.InputActionMap Input_master/PlayerActions::op_Implicit(Input_master/PlayerActions)
extern void PlayerActions_op_Implicit_m00773CF39CF596BDE1B46506EC4558388936D99B (void);
// 0x0000001F System.Void Input_master/PlayerActions::SetCallbacks(Input_master/IPlayerActions)
extern void PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7 (void);
// 0x00000020 System.Void Input_master/IPlayerActions::OnCameraMovement(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000021 System.Void Input_master/IPlayerActions::OnMouseDelta(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000022 System.Void Input_master/IPlayerActions::OnMousePosition(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000023 System.Void Input_master/IPlayerActions::OnMouseScroll(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000024 System.Void Input_master/IPlayerActions::OnLeftClick(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000025 System.Void Input_master/IPlayerActions::OnRightClick(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000026 System.Void Input_master/IPlayerActions::OnEscape(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000027 System.Void Input_master/IPlayerActions::OnSpacebar(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000028 System.Void Input_master/IPlayerActions::OnF1Cheat(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000029 System.Void Input_master/IPlayerActions::OnTutorialNext(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000002A CountryAudio CountryAudio::get_Instance()
extern void CountryAudio_get_Instance_m58C639822654FFFEEED2F06E4A82644DA29842D6 (void);
// 0x0000002B System.Void CountryAudio::Start()
extern void CountryAudio_Start_m8E8FA545C51B4885513FF601E78B64C0A20A8596 (void);
// 0x0000002C System.Void CountryAudio::ChangeGovernmentAudio()
extern void CountryAudio_ChangeGovernmentAudio_m9822E0E03403C9B3F91693532CE02EA3AD474B5C (void);
// 0x0000002D System.Void CountryAudio::.ctor()
extern void CountryAudio__ctor_mAF9408E08926ACCF56FCE41146DB8D632F79970B (void);
// 0x0000002E SoundManager SoundManager::get_Instance()
extern void SoundManager_get_Instance_mB2D05669AF91B4D14A63BC76820A362B6F36AB90 (void);
// 0x0000002F System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundManager::get_bgmMainmenu()
extern void SoundManager_get_bgmMainmenu_m8368E91C1207C6C2295598F7C010BD28D264A4E3 (void);
// 0x00000030 UnityEngine.AudioClip SoundManager::get_bgmWinning()
extern void SoundManager_get_bgmWinning_mE47BC72B0C8282ED2F630569B9CC47AD569305B5 (void);
// 0x00000031 UnityEngine.AudioClip SoundManager::get_bgmLosing()
extern void SoundManager_get_bgmLosing_m0E9C51BB4FEFEC329D0EAD0676ADC275CE00CCC2 (void);
// 0x00000032 System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundManager::get_bgmDemocracy()
extern void SoundManager_get_bgmDemocracy_mF2416A9A2869F1B4995BDD8548C69F063B6926B0 (void);
// 0x00000033 System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundManager::get_bgmSocialism()
extern void SoundManager_get_bgmSocialism_mBF5912E35F9784322A14EB4F353319C101F09E92 (void);
// 0x00000034 System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundManager::get_bgmMonarchy()
extern void SoundManager_get_bgmMonarchy_m3913D831D3179DBE89A76F94586802AAD357EE6F (void);
// 0x00000035 System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundManager::get_bgmDictator()
extern void SoundManager_get_bgmDictator_mEA0AE2463B56B9874D2DDB784D2337AAD6DA0B3D (void);
// 0x00000036 System.Collections.Generic.List`1<UnityEngine.AudioClip> SoundManager::get_bgmCommunism()
extern void SoundManager_get_bgmCommunism_m878710D150CCCFBD7F0DEA470F65E21B3FD20BD0 (void);
// 0x00000037 UnityEngine.AudioClip SoundManager::get_clicking()
extern void SoundManager_get_clicking_m3B7E901A4E7D6FB81ABF045C493202D423E4DA1D (void);
// 0x00000038 UnityEngine.AudioClip SoundManager::get_hovering()
extern void SoundManager_get_hovering_m84EF844D72F5D23F13D13F7A172A7C45F9BEDDD0 (void);
// 0x00000039 UnityEngine.AudioClip SoundManager::get_popup()
extern void SoundManager_get_popup_mFBE01E1F909C214E7EB8996714D6F4314F8CA1F8 (void);
// 0x0000003A UnityEngine.AudioClip SoundManager::get_electionVoting()
extern void SoundManager_get_electionVoting_mCA570C8E698306EB17BE3FC95789202B83BC30BA (void);
// 0x0000003B UnityEngine.AudioClip SoundManager::get_afterVoting()
extern void SoundManager_get_afterVoting_mBB24CC18BFD589EDAEE0E28C716AB22398DDC44C (void);
// 0x0000003C System.Void SoundManager::Awake()
extern void SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76 (void);
// 0x0000003D System.Void SoundManager::Update()
extern void SoundManager_Update_mDD188B65FF1E9B1DF1B8345A6290D149E70E657C (void);
// 0x0000003E System.Void SoundManager::CheckIfPlaying()
extern void SoundManager_CheckIfPlaying_m74F7C53D4283A6061B3A3516308D994AF281DC87 (void);
// 0x0000003F System.Void SoundManager::OnSceneLoaded()
extern void SoundManager_OnSceneLoaded_m77B3E475EE4B25348C20D0B125F9FF6690057132 (void);
// 0x00000040 System.Void SoundManager::ChooseListBGM(System.Collections.Generic.List`1<UnityEngine.AudioClip>)
extern void SoundManager_ChooseListBGM_m82117BF5751922FE01517FA5F7DE0C3035D74DD9 (void);
// 0x00000041 System.Void SoundManager::ChooseClipBGM(UnityEngine.AudioClip)
extern void SoundManager_ChooseClipBGM_m212EB6CC2532F9EF2F430592AF3C562AAD4468C4 (void);
// 0x00000042 System.Void SoundManager::OnHoverUI()
extern void SoundManager_OnHoverUI_mF2796CAFCC262451D8BF99918CBBF013B0AABA21 (void);
// 0x00000043 System.Void SoundManager::OnClickUI()
extern void SoundManager_OnClickUI_m4AAC7AE59343DA3C3B8D08F0230159F3317F5DD3 (void);
// 0x00000044 System.Void SoundManager::PlaySFX(UnityEngine.AudioClip)
extern void SoundManager_PlaySFX_m816BF84FC9AAB3A1199B472F908AF7796823169B (void);
// 0x00000045 System.Int32 SoundManager::RandomClip(System.Collections.Generic.List`1<UnityEngine.AudioClip>)
extern void SoundManager_RandomClip_mA883AE254C7198EA6A2660CB9AB412136E38464A (void);
// 0x00000046 System.Void SoundManager::.ctor()
extern void SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE (void);
// 0x00000047 System.Void VolumeMixer::Awake()
extern void VolumeMixer_Awake_m23F7C0578529E573B13C7A8FDC402A310BFD12B6 (void);
// 0x00000048 System.Void VolumeMixer::SetVolume(System.Single)
extern void VolumeMixer_SetVolume_mFE149DEB806A9D3FDAE5242829C35248A479D795 (void);
// 0x00000049 System.Void VolumeMixer::PlayEffectOnce()
extern void VolumeMixer_PlayEffectOnce_mC3AD134F86585FBA710D30736C38E412224012BD (void);
// 0x0000004A System.Void VolumeMixer::.ctor()
extern void VolumeMixer__ctor_m0026E479FD2F148D6067EF304C73ECDE1E915FCE (void);
// 0x0000004B System.Void CameraController::Start()
extern void CameraController_Start_mBDE87C2FCF352957C2B86B67610667663422FBE6 (void);
// 0x0000004C System.Void CameraController::Update()
extern void CameraController_Update_m3C257AC762117CFDDAD03C9C4FBBFDE51C61D534 (void);
// 0x0000004D System.Void CameraController::cameraMovement()
extern void CameraController_cameraMovement_mBC9FD807DE88226C18451AC26ED8EA1F5B6222E1 (void);
// 0x0000004E System.Void CameraController::cameraScroll()
extern void CameraController_cameraScroll_m41835690BF9C5E682F39CC8AC516C58AEF45202A (void);
// 0x0000004F System.Void CameraController::cameraFocus(UnityEngine.Vector2)
extern void CameraController_cameraFocus_m3B5D6E8ED0C1ED2DCB804C182F76EF83C1B635D1 (void);
// 0x00000050 System.Void CameraController::.ctor()
extern void CameraController__ctor_m07EC5A8C82742876097619BE7DD9043F47327DAE (void);
// 0x00000051 UnityEngine.UI.Image CardEventBrain::get_backCard()
extern void CardEventBrain_get_backCard_m3648CBCC9F0D7CB5EAE50925723C4AA761AB602A (void);
// 0x00000052 UnityEngine.UI.Image CardEventBrain::get_frontCard()
extern void CardEventBrain_get_frontCard_mAAF6B2A29764F9AB5E149C279F74BA1FEB3ECF0A (void);
// 0x00000053 System.Void CardEventBrain::Start()
extern void CardEventBrain_Start_mE3D499C19DF45EE0E3383EC85577FEC4E9CDF98E (void);
// 0x00000054 System.Void CardEventBrain::Update()
extern void CardEventBrain_Update_mBFEDF61DC23FF9125BB001105C4E25611EA99C47 (void);
// 0x00000055 System.Void CardEventBrain::PlayAnimation()
extern void CardEventBrain_PlayAnimation_m3F3BDE54E889825DA105D61E1DB2D80A636437E9 (void);
// 0x00000056 System.Void CardEventBrain::setCardEventData(CardEventData)
extern void CardEventBrain_setCardEventData_mA9FBF82EFC14698F2AF6C1C8BBD89F2272011F99 (void);
// 0x00000057 System.Void CardEventBrain::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void CardEventBrain_OnPointerEnter_mA2F34E7081E26E374BF855FE1BF99380C725A8D6 (void);
// 0x00000058 System.Void CardEventBrain::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void CardEventBrain_OnPointerExit_m45F4BB6F288BECCE7A63B3A633FB23A0082A2FED (void);
// 0x00000059 System.Void CardEventBrain::OnHoverUI()
extern void CardEventBrain_OnHoverUI_m77C92A7D39B4B2F111101E7F6311008085EA01B0 (void);
// 0x0000005A System.Void CardEventBrain::OnClickUI()
extern void CardEventBrain_OnClickUI_m5938FF276B383F15280960D4AB0897AF1FB54E92 (void);
// 0x0000005B System.Void CardEventBrain::.ctor()
extern void CardEventBrain__ctor_m114A6315B7805CB1AFB9C47AD8A462479C19576C (void);
// 0x0000005C System.Boolean CardEventManager::get_alreadySelectCard()
extern void CardEventManager_get_alreadySelectCard_m9A9ABB70838B7D3FE3234D649A03D103DB450894 (void);
// 0x0000005D System.Void CardEventManager::Start()
extern void CardEventManager_Start_mE198601839CBAF404F275AB0DE4F954594732D59 (void);
// 0x0000005E System.Void CardEventManager::Update()
extern void CardEventManager_Update_m932B4298D6887A07B0A97EAEEB723611A56C5909 (void);
// 0x0000005F System.Void CardEventManager::OpenCloseEventUIButton()
extern void CardEventManager_OpenCloseEventUIButton_mD16DC5CFDB59DA120F3A86C03E976EA6067628CE (void);
// 0x00000060 System.Void CardEventManager::spawnCard()
extern void CardEventManager_spawnCard_m8C0EFC6FD963F1386E01E463F8091C2586023793 (void);
// 0x00000061 System.Void CardEventManager::startCardSpawn(System.Collections.Generic.List`1<CardEventData>)
extern void CardEventManager_startCardSpawn_mD1B9D79FA2419EF5C1F4D9D885488E4897F9DEA5 (void);
// 0x00000062 System.Int32 CardEventManager::get_designatedGoodBadType()
extern void CardEventManager_get_designatedGoodBadType_m849499FE1BB3151B8EF36094B0E2B025A24921D4 (void);
// 0x00000063 System.Void CardEventManager::designatedCardType()
extern void CardEventManager_designatedCardType_m6F5DC114BDD39CA493AC7D264AE6E60AB2BB8885 (void);
// 0x00000064 System.Void CardEventManager::SelectedCard()
extern void CardEventManager_SelectedCard_m9463E3666A53296C56A78378C66D5603253867F8 (void);
// 0x00000065 System.Void CardEventManager::ClosedEvent()
extern void CardEventManager_ClosedEvent_mE020B4B7933DF9ADAFEF4AAF66DE9FBB4F74342F (void);
// 0x00000066 System.Void CardEventManager::AddDayToEvent(System.Int32)
extern void CardEventManager_AddDayToEvent_m9B95B9313BE24A2CAC3C0818E7A8C85DD3A277C8 (void);
// 0x00000067 System.Void CardEventManager::CallEvent()
extern void CardEventManager_CallEvent_m85B35A5031FF4AB265A6AF5A906BB30186DDC4E6 (void);
// 0x00000068 System.Void CardEventManager::.ctor()
extern void CardEventManager__ctor_m87DDB08CCAF594E2D603ED24CF16928ADA10F203 (void);
// 0x00000069 System.Void Cheating::Start()
extern void Cheating_Start_m38F556D594711CE5180E9381C509180C4697842E (void);
// 0x0000006A System.Void Cheating::Update()
extern void Cheating_Update_m97EA5875C51E3B9E925F92AFC90D83032C0E1705 (void);
// 0x0000006B System.Void Cheating::OpenCloseCheatTab()
extern void Cheating_OpenCloseCheatTab_m3FE5D1229FBE3FA80557F77A5BEF11E2AC6B2564 (void);
// 0x0000006C System.Void Cheating::OpenTab()
extern void Cheating_OpenTab_mB3A970EF42E35987031A1873B2FF02300293543D (void);
// 0x0000006D System.Void Cheating::CloseTab()
extern void Cheating_CloseTab_mD259BB060BA26FD28B8F6D9397081B7EDD3C0B29 (void);
// 0x0000006E System.Void Cheating::CheatMoney()
extern void Cheating_CheatMoney_mAE279B8B4D9CA37A5B0AAAF97D5F2F884C0E7FAE (void);
// 0x0000006F System.Void Cheating::CheatMoney2()
extern void Cheating_CheatMoney2_m96342637F9ADF85C5874E1D9587E9331E964F80B (void);
// 0x00000070 System.Void Cheating::CheatPublicValue()
extern void Cheating_CheatPublicValue_m2566A7A2E03C6303A1FB52517372AAB937C7E85F (void);
// 0x00000071 System.Void Cheating::CheatEconomicValue()
extern void Cheating_CheatEconomicValue_m10B5D2D121D540DEC6EF2652BEDE3424F2DC835A (void);
// 0x00000072 System.Void Cheating::CheatMilitaryValue()
extern void Cheating_CheatMilitaryValue_mF751F1690354B35D60511CB6538C365F2B364604 (void);
// 0x00000073 System.Void Cheating::CheatElectionDay()
extern void Cheating_CheatElectionDay_m35B1DEB2A51A18CD1A32A5FCE9898203C3008429 (void);
// 0x00000074 System.Void Cheating::.ctor()
extern void Cheating__ctor_m93D162DDFA96A378CC68CDE2E915BF81D6DBF0AB (void);
// 0x00000075 System.String ShipClass::get_name()
extern void ShipClass_get_name_mE79E57465EA2595E7B73E6B1265C5EB04A18E19C (void);
// 0x00000076 System.Void ShipClass::.ctor(System.String,System.Single,System.Int32)
extern void ShipClass__ctor_m0E937B988FBC915A5AA3F2FD6BB3F85ADD0D227A (void);
// 0x00000077 System.Void ShipClass::moving()
extern void ShipClass_moving_mD79E16108D09BE9963A4DDE7EFC27F5640EE94E7 (void);
// 0x00000078 System.String TurretClass::get_turretName()
extern void TurretClass_get_turretName_m46C7B0DE0F05A4232AE37C8F186602156FF815C8 (void);
// 0x00000079 System.Void TurretClass::.ctor(System.String,System.Single,System.Int32,System.Boolean)
extern void TurretClass__ctor_mAE4842A58B0DF0C6344C0BDF803B410F342A4ABE (void);
// 0x0000007A System.Void TurretClass::targetDetection(UnityEngine.GameObject,System.Single)
extern void TurretClass_targetDetection_m4D0ED4B29A9108FDF0F812F04EBB9A3B2778F993 (void);
// 0x0000007B System.Collections.IEnumerator TurretClass::targetDetectionCoroutine(System.Single,UnityEngine.GameObject,System.Single)
extern void TurretClass_targetDetectionCoroutine_m633FD800BF36138D7A2B26D8932088823863C0E0 (void);
// 0x0000007C System.Void TurretClass::turretTargeting(UnityEngine.GameObject,System.Single)
extern void TurretClass_turretTargeting_m8BA82DA5464EBA743C4ECC2FAB492A1381299FCF (void);
// 0x0000007D System.Void TurretClass/<targetDetectionCoroutine>d__9::.ctor(System.Int32)
extern void U3CtargetDetectionCoroutineU3Ed__9__ctor_mBA51F5C3479716B5B5D1CB4CCE4AE935676E7801 (void);
// 0x0000007E System.Void TurretClass/<targetDetectionCoroutine>d__9::System.IDisposable.Dispose()
extern void U3CtargetDetectionCoroutineU3Ed__9_System_IDisposable_Dispose_m13935AB646A11C03594BDF24BE60C20703CC4069 (void);
// 0x0000007F System.Boolean TurretClass/<targetDetectionCoroutine>d__9::MoveNext()
extern void U3CtargetDetectionCoroutineU3Ed__9_MoveNext_mCFAFD3B872EB309E2F9B309793AC5ADD12DDFC7E (void);
// 0x00000080 System.Object TurretClass/<targetDetectionCoroutine>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtargetDetectionCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AD1BFF77A8EAA12CA4A22894B9D76289F3605DA (void);
// 0x00000081 System.Void TurretClass/<targetDetectionCoroutine>d__9::System.Collections.IEnumerator.Reset()
extern void U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_mA388A93F226960939E6D49C21E9EEF25BC6599E2 (void);
// 0x00000082 System.Object TurretClass/<targetDetectionCoroutine>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mFF85508530D8B6F47243884D4293F575601073A5 (void);
// 0x00000083 CountryGovernment CountryBrain::get_countryGovernment()
extern void CountryBrain_get_countryGovernment_m26DE7E2E7F082B5F2EA851006F4863C5759F6DD5 (void);
// 0x00000084 CountryUpgrade CountryBrain::get_countryUpgrade()
extern void CountryBrain_get_countryUpgrade_m3596D2161F97D9CB971D03EA2A905643DFC5062F (void);
// 0x00000085 CountryEconomy CountryBrain::get_countryEconomy()
extern void CountryBrain_get_countryEconomy_m796937780C513AE849ECC70AE5DFF87CC2B24F8C (void);
// 0x00000086 System.Void CountryBrain::Start()
extern void CountryBrain_Start_m6912885CA2073680BC4C368450F19A429EF8E3FF (void);
// 0x00000087 System.Void CountryBrain::Update()
extern void CountryBrain_Update_mE3586BB21DF8012E34F47A6582BEC4CEE7582C0A (void);
// 0x00000088 System.Void CountryBrain::regionSelect()
extern void CountryBrain_regionSelect_m1E2A915FFC850EE92F7DDC8C69E96459B1DA10C9 (void);
// 0x00000089 System.Void CountryBrain::.ctor()
extern void CountryBrain__ctor_mCE56B125651C62760E306CA89019CFBA032903E2 (void);
// 0x0000008A System.Void CountryDistrict::Start()
extern void CountryDistrict_Start_m912584DD25F4BC8F20D39271856629252631CB44 (void);
// 0x0000008B System.Void CountryDistrict::createDistrict()
extern void CountryDistrict_createDistrict_m9D8368F04F179B7343EDF05BA9BFFB18A158D240 (void);
// 0x0000008C System.Int32 CountryDistrict::get_designatedDistrict()
extern void CountryDistrict_get_designatedDistrict_m72632C47B4DA167DED4AF8783AB1F26A4BBC5133 (void);
// 0x0000008D System.Void CountryDistrict::.ctor()
extern void CountryDistrict__ctor_m4B04111BEF2DA3B38BC5B043AB7E9114CFE64FFC (void);
// 0x0000008E System.Single CountryEconomy::get_cardEventChance()
extern void CountryEconomy_get_cardEventChance_mDC1FBD4EE012D9F1DED14B0E043399B517182388 (void);
// 0x0000008F System.Single CountryEconomy::get_electionChance()
extern void CountryEconomy_get_electionChance_m4CC6AC2BD55BDC866621B48CA95F133B36E69A77 (void);
// 0x00000090 System.Void CountryEconomy::Start()
extern void CountryEconomy_Start_mEA0F88F510C4675446F6CEFF7542BE98F386677E (void);
// 0x00000091 System.Void CountryEconomy::revenueCalculate()
extern void CountryEconomy_revenueCalculate_mFC8B38093063B75F239DD7BA0F9097D4E16A9EA3 (void);
// 0x00000092 System.Int32 CountryEconomy::get_IncomeCalculate()
extern void CountryEconomy_get_IncomeCalculate_m83DF6CBC55500641CF802DB03CFC1B5E313473DB (void);
// 0x00000093 System.Int32 CountryEconomy::get_OutcomeCalculate()
extern void CountryEconomy_get_OutcomeCalculate_m6CE6A6D66C085A2F4EB72E5B15050D95589900D0 (void);
// 0x00000094 System.Void CountryEconomy::StatCalculate()
extern void CountryEconomy_StatCalculate_m8F309A1B832A529D57721073470F61A1E8F7B3B2 (void);
// 0x00000095 System.Void CountryEconomy::currentStatAndEconomy()
extern void CountryEconomy_currentStatAndEconomy_m16A5757B5D642372D65F95CE67E4F1AB19BAFF33 (void);
// 0x00000096 System.Int32 CountryEconomy::CurrentStatInPercent(System.Single)
extern void CountryEconomy_CurrentStatInPercent_m6BEACEAB86A400825C2725EB267D0E7163C6F986 (void);
// 0x00000097 System.Void CountryEconomy::upgradeCalculate()
extern void CountryEconomy_upgradeCalculate_mBAEEF020289ED6F4636ED54599A0411B8A47249C (void);
// 0x00000098 System.Single CountryEconomy::get_CalculateCardEventStatChance()
extern void CountryEconomy_get_CalculateCardEventStatChance_m79EA3633A23C90B2FD50FB798C26888219AF8367 (void);
// 0x00000099 System.Single CountryEconomy::get_CalculateElectionStatChance()
extern void CountryEconomy_get_CalculateElectionStatChance_m8E89AFDD89254B9AE65DFE6FCA261ABAD5F80681 (void);
// 0x0000009A System.Void CountryEconomy::EndOfTheMonth()
extern void CountryEconomy_EndOfTheMonth_mF2557B29DDC2B1EAD6F70D81CE72E329152A7D80 (void);
// 0x0000009B System.Void CountryEconomy::.ctor()
extern void CountryEconomy__ctor_mCC98899C44072BF6703CD4198F55EEA222AA7EE6 (void);
// 0x0000009C System.Void CountryEconomy/<>c::.cctor()
extern void U3CU3Ec__cctor_m53FAA027DFCABE8C1174C7BD88ED6E53F9362747 (void);
// 0x0000009D System.Void CountryEconomy/<>c::.ctor()
extern void U3CU3Ec__ctor_mB34CDBBA6DE39B91D9ACA700E1362AEA5C23672D (void);
// 0x0000009E System.Boolean CountryEconomy/<>c::<upgradeCalculate>b__51_0(StatEffectData)
extern void U3CU3Ec_U3CupgradeCalculateU3Eb__51_0_m1E880F6D0C501B528C2620D287D683D22BCD0DA9 (void);
// 0x0000009F GovernmentData CountryGovernment::get_governmentData()
extern void CountryGovernment_get_governmentData_m896B901392E40E5DF54EE44616C4B6826E518FD7 (void);
// 0x000000A0 System.Void CountryGovernment::Start()
extern void CountryGovernment_Start_m86C2B74F2124DFDBF0EBFA21714A990ED0D8D017 (void);
// 0x000000A1 System.Void CountryGovernment::changeGovernment(GovernmentData)
extern void CountryGovernment_changeGovernment_mE09E2FF10E7A81342B1839227F4FAC5170B69D9D (void);
// 0x000000A2 System.Void CountryGovernment::updateCurrentStat()
extern void CountryGovernment_updateCurrentStat_mD1127FA7282E4B32F320DEF9C054D96223800190 (void);
// 0x000000A3 System.Void CountryGovernment::.ctor()
extern void CountryGovernment__ctor_mCD399ACFBB316DF55CA5421D0E8F72D3F5709974 (void);
// 0x000000A4 System.Int32 CountryUpgrade::get_currentUpgradeTier()
extern void CountryUpgrade_get_currentUpgradeTier_mC4807DC07B1DE4D90CFA4A95AB277899037A5124 (void);
// 0x000000A5 System.Collections.Generic.List`1<UpgradeData> CountryUpgrade::get_upgradeData()
extern void CountryUpgrade_get_upgradeData_m2D04625F075E88CE2592FD4EB3AD04EDD13CFCAF (void);
// 0x000000A6 System.Collections.Generic.List`1<StatEffectData> CountryUpgrade::get_statEffectData()
extern void CountryUpgrade_get_statEffectData_m7B9EC2E4DF9263961BC1E9FAAFFA42AF5F38B079 (void);
// 0x000000A7 System.Void CountryUpgrade::increaseTier(System.Int32)
extern void CountryUpgrade_increaseTier_m44A7F36ED5CE23AE2E56D53113EC31BB7152D35A (void);
// 0x000000A8 System.Void CountryUpgrade::insertNewUpgradeData(UpgradeData,StatEffectData)
extern void CountryUpgrade_insertNewUpgradeData_m4DA49ACAF40B70C95E9B76F193AE28278E82A9F0 (void);
// 0x000000A9 System.Void CountryUpgrade::insertNewEventData(StatEffectData)
extern void CountryUpgrade_insertNewEventData_m8F74129AF6758B46D599F14486EBE92F257FF691 (void);
// 0x000000AA System.Void CountryUpgrade::.ctor()
extern void CountryUpgrade__ctor_mCB8C263660CF601EA8D9FA17E83F91A78FBD8C41 (void);
// 0x000000AB System.String CardEventData::get_cardName()
extern void CardEventData_get_cardName_mBAD9B38CF0B04088C7EC73684C1BDA83512E7F09 (void);
// 0x000000AC System.String CardEventData::get_cardDescription()
extern void CardEventData_get_cardDescription_m1E5A7B09F10D7258DDEA33321BB07D6FCC81898A (void);
// 0x000000AD System.String CardEventData::get_effectDescription()
extern void CardEventData_get_effectDescription_m168E468D912729BFD37A075849A8BC4FE9DB9D52 (void);
// 0x000000AE StatEffectData CardEventData::get_statEffectData()
extern void CardEventData_get_statEffectData_mEC8F9B6D8DDE2C334BEFD2DBF26DC9B9B7CCBE5C (void);
// 0x000000AF StatEffectData CardEventData::get_subsStatEffectData()
extern void CardEventData_get_subsStatEffectData_m05FAB503C9C174B0F94710DF86AE463CB5DC6560 (void);
// 0x000000B0 System.Void CardEventData::.ctor()
extern void CardEventData__ctor_mF6AC570E6DB9BDB389EFBB7F6881B1F37B9E69A7 (void);
// 0x000000B1 System.String DistrictData::get_districtName()
extern void DistrictData_get_districtName_m3B0A7A438DC28A7EDC702FDB021234172F0F1098 (void);
// 0x000000B2 UnityEngine.Sprite DistrictData::get_districtImage()
extern void DistrictData_get_districtImage_m4B518315EA85A08FC22801F90556883848DD1790 (void);
// 0x000000B3 System.Boolean DistrictData::get_isResidential()
extern void DistrictData_get_isResidential_mF95E716529D5FC1C944330697BD050852FF6BB61 (void);
// 0x000000B4 System.Boolean DistrictData::get_isIndustry()
extern void DistrictData_get_isIndustry_m90148564F8DE8B2C06C89C4962153938EA7AD99E (void);
// 0x000000B5 System.Boolean DistrictData::get_isHarbour()
extern void DistrictData_get_isHarbour_m8A261E8B11BD8CE8053D4C890EB830401A932DED (void);
// 0x000000B6 System.Boolean DistrictData::get_isCommercial()
extern void DistrictData_get_isCommercial_mFFC3175EC32D1587BFF4FAA4294740A0DFBB3509 (void);
// 0x000000B7 System.Boolean DistrictData::get_isBadZone()
extern void DistrictData_get_isBadZone_m8AEE05C4AA8A516350E89B469659F96F9BB6932E (void);
// 0x000000B8 System.Int32 DistrictData::get_moneyBonus()
extern void DistrictData_get_moneyBonus_mF37C88A4138A91C0DAE301C77A802805584A38AA (void);
// 0x000000B9 System.Single DistrictData::get_publicOpinionBonus()
extern void DistrictData_get_publicOpinionBonus_m3DAC4187175544E59B5F517E0C6FC36B262C54EF (void);
// 0x000000BA System.Single DistrictData::get_economyBonus()
extern void DistrictData_get_economyBonus_m389A3060D859F71908086B0F77993264E06629BD (void);
// 0x000000BB System.Single DistrictData::get_militaryBonus()
extern void DistrictData_get_militaryBonus_mDB1FAC68A1A2822D8FA328B024C8CADBA66A58A6 (void);
// 0x000000BC System.Single DistrictData::get_moneyPercent()
extern void DistrictData_get_moneyPercent_mA5DEF5190184C1B9B9950D8A081939D1C3E7DF88 (void);
// 0x000000BD System.Single DistrictData::get_publicOpinionPercent()
extern void DistrictData_get_publicOpinionPercent_mC13CE3B35D013F650C0D7D37FAED5DE0D53BF468 (void);
// 0x000000BE System.Single DistrictData::get_economyPercent()
extern void DistrictData_get_economyPercent_mDCF574941DBD27D4203CE4C083E8F76681C8F01A (void);
// 0x000000BF System.Single DistrictData::get_militaryPercent()
extern void DistrictData_get_militaryPercent_mF1E6B08075F4D51B70713CB875AA3BB7CBB6E24E (void);
// 0x000000C0 System.Void DistrictData::.ctor()
extern void DistrictData__ctor_mFDC75FAF2B37646DDEC41384BFBA1544F0F0BF61 (void);
// 0x000000C1 System.String EconomyData::get_countryName()
extern void EconomyData_get_countryName_mF6CAFE2A64857565F54CC20B1F607EB8C455CAFB (void);
// 0x000000C2 System.String EconomyData::get_newCountryName()
extern void EconomyData_get_newCountryName_mFD9CCC0CD34DF9ACC37720C4D46A6AC0391A9E16 (void);
// 0x000000C3 System.Int32 EconomyData::get_startingMoney()
extern void EconomyData_get_startingMoney_mF7C4EEFB89EE4D7C5BCF8A0B5A6BAEC5F6EFECC8 (void);
// 0x000000C4 System.Int32 EconomyData::get_days()
extern void EconomyData_get_days_mBBAD1A61082D971231E8C53FA1337D7200D22701 (void);
// 0x000000C5 System.Int32 EconomyData::get_months()
extern void EconomyData_get_months_mA6E6A8C46F0580077F3E4A2655AFD9D40B44217E (void);
// 0x000000C6 System.Int32 EconomyData::get_years()
extern void EconomyData_get_years_m26A5D2198869827F680C0289EF95707F0AD9887F (void);
// 0x000000C7 System.Int32 EconomyData::get_electionEvery()
extern void EconomyData_get_electionEvery_m8F13E9B836DF7F13F95E73A6FE8A6FE6EC68285F (void);
// 0x000000C8 System.Single EconomyData::get_publicOpinionValue()
extern void EconomyData_get_publicOpinionValue_m04AA45A9B2496613CAECB9CDFDF319CAFBFAA99B (void);
// 0x000000C9 System.Single EconomyData::get_economyValue()
extern void EconomyData_get_economyValue_m9970D18307128BF7096843FE467E8F9824F8BFA4 (void);
// 0x000000CA System.Single EconomyData::get_militaryValue()
extern void EconomyData_get_militaryValue_m9FBDC9FA711772266EB3E56382368C6484272E39 (void);
// 0x000000CB System.Single EconomyData::get_minimumOrderToLose()
extern void EconomyData_get_minimumOrderToLose_m5F2BB946545A620417B83DC8DDAB3CA6827B5CE4 (void);
// 0x000000CC System.Int32 EconomyData::get_incomeBase()
extern void EconomyData_get_incomeBase_m03008DCF12907ED3478B49F2B72166B327C61261 (void);
// 0x000000CD System.Single EconomyData::get_incomeMultiplying()
extern void EconomyData_get_incomeMultiplying_m2CB7DD3B4F337272F28E5A5CACE8B0BD279E4F1A (void);
// 0x000000CE System.Int32 EconomyData::get_outcomeBase()
extern void EconomyData_get_outcomeBase_mCB50EE00CC4BC502003A957D9F325233ADF67FBA (void);
// 0x000000CF System.Int32 EconomyData::get_endOfTheMonthDate()
extern void EconomyData_get_endOfTheMonthDate_mFEABDADD2517DC3024E91218F7003DDC75531524 (void);
// 0x000000D0 System.Single EconomyData::get_publicOpinionDecayValue()
extern void EconomyData_get_publicOpinionDecayValue_mDAAF7B41BCBBC8FAE6ABACB4C1881A958205FE54 (void);
// 0x000000D1 System.Single EconomyData::get_economyDecayValue()
extern void EconomyData_get_economyDecayValue_m00CA6DD50BCABD4465E8EDD4C42A262B351A3832 (void);
// 0x000000D2 System.Single EconomyData::get_militaryDecayValue()
extern void EconomyData_get_militaryDecayValue_mEC0D47FAAC6393F7A5CD4DAE73DFE96C3EEF2234 (void);
// 0x000000D3 System.Single EconomyData::get_veryBad()
extern void EconomyData_get_veryBad_m90FC791B525C0D587281C0AAC417529142B574A0 (void);
// 0x000000D4 System.Single EconomyData::get_bad()
extern void EconomyData_get_bad_m5BAA33527AB3F90CF9C179BEB7650150497F3345 (void);
// 0x000000D5 System.Single EconomyData::get_normal()
extern void EconomyData_get_normal_m0ED8A334D26144B9E99BFF15431CA7B4E657AE76 (void);
// 0x000000D6 System.Single EconomyData::get_good()
extern void EconomyData_get_good_m99896FE4DFAF6F4B4D917171CCC3859CA20C22C2 (void);
// 0x000000D7 System.Single EconomyData::get_veryGood()
extern void EconomyData_get_veryGood_m2134A6B014BD445520ED42CC26FF5AC40D595567 (void);
// 0x000000D8 System.Single EconomyData::get_goodDistrictRate()
extern void EconomyData_get_goodDistrictRate_m308C1B4ACEB8B5FC541A8547C82C2B50FB0A9237 (void);
// 0x000000D9 System.Int32 EconomyData::get_cardEventEvery()
extern void EconomyData_get_cardEventEvery_mA85B565EDA1B6F7E3A93AF95B1BF20DA8F7F2BD0 (void);
// 0x000000DA System.Single EconomyData::get_goodEvent()
extern void EconomyData_get_goodEvent_m06FD9C42FACAE599538682455966C5E2E363B919 (void);
// 0x000000DB System.Single EconomyData::get_badEvent()
extern void EconomyData_get_badEvent_m18C8A7CA47ED21537EC556F1135AAE8E3CF32513 (void);
// 0x000000DC System.Single EconomyData::get_minimumElectionValue()
extern void EconomyData_get_minimumElectionValue_m534C619225ED30A8E521AD91DEA36E3FB65890CF (void);
// 0x000000DD System.Single EconomyData::get_maximumElectionValue()
extern void EconomyData_get_maximumElectionValue_m1744BBD84F77A2FF66E76D32EF808931F22D87C1 (void);
// 0x000000DE System.Single EconomyData::get_electionBaseChance()
extern void EconomyData_get_electionBaseChance_mEA7B3EF797360A97C7C5485D1C6E5A5C48B65461 (void);
// 0x000000DF System.Single EconomyData::get_electionDivider()
extern void EconomyData_get_electionDivider_m55BC4D5FD07DBEA01F38A3608BA79F9C1B4D9FC9 (void);
// 0x000000E0 System.Single EconomyData::get_votingResultDivider()
extern void EconomyData_get_votingResultDivider_m5634C403F2FF081CACA6FD129E47CD6EFCE48697 (void);
// 0x000000E1 System.Single EconomyData::get_votingCheatMoneyPercent()
extern void EconomyData_get_votingCheatMoneyPercent_m8AF83BB58CD27BF1D0C802679F2EBA20ABBAD5B4 (void);
// 0x000000E2 System.Single EconomyData::get_votingCheatMoneyMultiplier()
extern void EconomyData_get_votingCheatMoneyMultiplier_mDC7C50F7654114D3D3687253EE0183B38741BD79 (void);
// 0x000000E3 System.Single EconomyData::get_beforeVotingDivider()
extern void EconomyData_get_beforeVotingDivider_mC24C65B53F266C717EFFD9BDC218BD5A6B26C428 (void);
// 0x000000E4 System.Single EconomyData::get_afterVotingDivider()
extern void EconomyData_get_afterVotingDivider_mA7EB42693A59CBB797C919E501C4447720CB72ED (void);
// 0x000000E5 System.Single EconomyData::get_upgradeMultiply()
extern void EconomyData_get_upgradeMultiply_m2863D8521A2A662CCA6073ECC426D233061160B3 (void);
// 0x000000E6 System.Single EconomyData::get_publicMultiply()
extern void EconomyData_get_publicMultiply_mAE63886A9B76704B0FAF27A8A5F03BD2F52FC690 (void);
// 0x000000E7 System.Single EconomyData::get_economyMultiply()
extern void EconomyData_get_economyMultiply_m25B68928CE08515C00789B74DA531787A04C99B7 (void);
// 0x000000E8 System.Single EconomyData::get_militaryMultiply()
extern void EconomyData_get_militaryMultiply_m884A0AA5DB72A5D0FB5A3F16E8CA9D7A704EB454 (void);
// 0x000000E9 System.Single EconomyData::get_moneyDivider()
extern void EconomyData_get_moneyDivider_m5D3518A4D42F452C3974D90213BFB6BBBCF6FE5E (void);
// 0x000000EA System.Single EconomyData::get_badPublicMultiply()
extern void EconomyData_get_badPublicMultiply_m82033CCD3669F347E7DD9295501798B7F9328A98 (void);
// 0x000000EB System.Void EconomyData::updateNewValue(System.Int32,System.Int32,System.Int32)
extern void EconomyData_updateNewValue_mE2540EB3C53EBD889B3DED3FFE612E1B115FC75B (void);
// 0x000000EC System.Boolean EconomyData::endOfTheMonth()
extern void EconomyData_endOfTheMonth_mD32D4053F644BC7AF446CB31694BFFB4D35E1187 (void);
// 0x000000ED System.Int32 EconomyData::get_getCurrentMoney()
extern void EconomyData_get_getCurrentMoney_mFC7EA2F72106D48CEB99EB0C2553D473CA220493 (void);
// 0x000000EE System.Void EconomyData::changeCountryName(System.String)
extern void EconomyData_changeCountryName_m9C7DFE74CEDFFBDBA008170C54A813C8ACC5395E (void);
// 0x000000EF System.Void EconomyData::.ctor()
extern void EconomyData__ctor_mFFB98EE599F23728210BE480E06751FE27EFC3DD (void);
// 0x000000F0 System.Void GameData::loadGameData(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern void GameData_loadGameData_mD4A0B810F65FC82B7F16D83C30C16908A0E75B22 (void);
// 0x000000F1 System.Void GameData::saveGameData(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern void GameData_saveGameData_mF07531056195A9CDED3566E76707AE63F5F3E483 (void);
// 0x000000F2 System.Void GameData::transitionCompleteSwitch(System.Boolean)
extern void GameData_transitionCompleteSwitch_mC93504D1E1267016AE46BD8BC848A3DB75D8563E (void);
// 0x000000F3 System.Void GameData::.ctor()
extern void GameData__ctor_mF787127A0B2A0A3EBFA33BAB7050087D66E80F14 (void);
// 0x000000F4 GameManager GameManager::get_Instance()
extern void GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232 (void);
// 0x000000F5 System.String GameManager::get_countryName()
extern void GameManager_get_countryName_m31D2248199BF40AC162091777DAA53242E2CB925 (void);
// 0x000000F6 System.Single GameManager::get_publicOpinionValue()
extern void GameManager_get_publicOpinionValue_mA3A1BE248BD88479ED97D303E6DB3C70A3434931 (void);
// 0x000000F7 System.Single GameManager::get_economyValue()
extern void GameManager_get_economyValue_mAA20EE6E4B693BBFCBCE06E546F382A459C16196 (void);
// 0x000000F8 System.Single GameManager::get_militaryValue()
extern void GameManager_get_militaryValue_m626CA1ECA2A6126A4254E313D537287031184112 (void);
// 0x000000F9 System.Single GameManager::get_electionStackupChance()
extern void GameManager_get_electionStackupChance_m9ADCE6BB1A2D3965AFF96A79BBEB8A285E73C6FF (void);
// 0x000000FA EconomyData GameManager::get_economyData()
extern void GameManager_get_economyData_m9EFD32566B6C8B0C8AC28EC0D06F4E4818965504 (void);
// 0x000000FB System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x000000FC System.Void GameManager::loadDateData(System.Int32,System.Int32,System.Int32)
extern void GameManager_loadDateData_mAFEE44A35CF54F0FA2DC48F3430E06A86E4A5944 (void);
// 0x000000FD System.Void GameManager::loadMoneyData(System.Int32)
extern void GameManager_loadMoneyData_mE6D1E1A168AACADA2A7249B901452A5757E7C38B (void);
// 0x000000FE System.Void GameManager::loadCountryStatData(System.Single,System.Single,System.Single)
extern void GameManager_loadCountryStatData_mDD4191F3E78C68BB8A56FCD3EE0F72237FC0D4C1 (void);
// 0x000000FF System.Void GameManager::addDataToCountryStat(System.Single,System.Single,System.Single)
extern void GameManager_addDataToCountryStat_m8869AF5AC61D5A71844E4709D436F691ECB2FF78 (void);
// 0x00000100 System.Void GameManager::AddElectionBonus(System.Single)
extern void GameManager_AddElectionBonus_mBC56F1FEAF9B1BE79E959B310DC1E25F2821D72A (void);
// 0x00000101 System.Void GameManager::DebugSkipEntireMonth()
extern void GameManager_DebugSkipEntireMonth_m0DB0BD18E6906BA29BF1FEB96D11AA3AF93E00A9 (void);
// 0x00000102 System.Void GameManager::DebugStartElection()
extern void GameManager_DebugStartElection_mD2EAB4CE9C83A1606492DECCFC377CFE2EC75C32 (void);
// 0x00000103 System.Void GameManager::CheatPublicOpinion(System.Single)
extern void GameManager_CheatPublicOpinion_m142849F4AA27F9A96EAC378F3BAC881E5D5A5943 (void);
// 0x00000104 System.Void GameManager::CheatEconomic(System.Single)
extern void GameManager_CheatEconomic_mD9432D80ABC2ED0F469ABF1989EA993346326689 (void);
// 0x00000105 System.Void GameManager::CheatMilitary(System.Single)
extern void GameManager_CheatMilitary_m59142F75AD5FBD5DC534564AAD46E18F120C8870 (void);
// 0x00000106 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000107 System.String GovernmentData::get_systemName()
extern void GovernmentData_get_systemName_mF626D3FF534722409B5573E8D06F9C235432E715 (void);
// 0x00000108 UnityEngine.Sprite GovernmentData::get_governmentSprite()
extern void GovernmentData_get_governmentSprite_m42BAEF7B483321CA7FAF70AFE9EE7A5F5262E86F (void);
// 0x00000109 StatEffectData GovernmentData::get_governmentPassiveStat()
extern void GovernmentData_get_governmentPassiveStat_mB3A8B80F12817BDE0D73DF12BF1B35E2FC3DBB4E (void);
// 0x0000010A System.Boolean GovernmentData::get_hasElection()
extern void GovernmentData_get_hasElection_m5EA0D871AEBA68A6019F9395E30B94962293D2BF (void);
// 0x0000010B System.Int32 GovernmentData::get_StartingFund()
extern void GovernmentData_get_StartingFund_mBD320DFB328B059C5535060644038C25BAD0929A (void);
// 0x0000010C System.Single GovernmentData::get_publicOpinionValue()
extern void GovernmentData_get_publicOpinionValue_m3B0434F04A3D124F2B7E08F3B7449EC5A3A2779F (void);
// 0x0000010D System.Single GovernmentData::get_economyStrengthValue()
extern void GovernmentData_get_economyStrengthValue_m202A6F952377F629D4222BEC623BDD1775BACB31 (void);
// 0x0000010E System.Single GovernmentData::get_militaryStrengthValue()
extern void GovernmentData_get_militaryStrengthValue_m5C6DBF15CCAC03F82B97A7E8829E8D0C03CA6BFD (void);
// 0x0000010F System.Void GovernmentData::.ctor()
extern void GovernmentData__ctor_mAD64C350792317C17A6D04F4C7E82C270DBB6B16 (void);
// 0x00000110 System.Boolean StatEffectData::get_perMonth()
extern void StatEffectData_get_perMonth_mEBC235B2B5501FE85CC65297CBF2AFB372BF50CE (void);
// 0x00000111 System.Int32 StatEffectData::get_moneyBonus()
extern void StatEffectData_get_moneyBonus_m8C8D949AE9CF1FC14592453178F5ABAE7B314A57 (void);
// 0x00000112 System.Single StatEffectData::get_publicOpinionBonus()
extern void StatEffectData_get_publicOpinionBonus_mB0C04288B1065D51F55C322141CC6BD4CE972ADC (void);
// 0x00000113 System.Single StatEffectData::get_economyBonus()
extern void StatEffectData_get_economyBonus_m9E1FF70C5BE164E5108AB8958A6F074D28610C22 (void);
// 0x00000114 System.Single StatEffectData::get_militaryBonus()
extern void StatEffectData_get_militaryBonus_mAC8E6FE23355AA1F15885460CFB419609C05DEC0 (void);
// 0x00000115 System.Single StatEffectData::get_moneyPercent()
extern void StatEffectData_get_moneyPercent_mAF24E24C754DDAF8B7360586F6CD60D11901D7C5 (void);
// 0x00000116 System.Single StatEffectData::get_publicOpinionPercent()
extern void StatEffectData_get_publicOpinionPercent_m1FE483A83A1BA09F125291A81DFF347CF84C8D61 (void);
// 0x00000117 System.Single StatEffectData::get_economyPercent()
extern void StatEffectData_get_economyPercent_mDC82FFB395BCF5A1FB28B9A0485A9D8019A12237 (void);
// 0x00000118 System.Single StatEffectData::get_militaryPercent()
extern void StatEffectData_get_militaryPercent_mA11680A1E0A8D8EAC80B3B24F8334FA696CC96A8 (void);
// 0x00000119 System.Single StatEffectData::get_electionPercent()
extern void StatEffectData_get_electionPercent_m77D70CF732B57DD4292B6AA097EE6ED6352F0222 (void);
// 0x0000011A System.Single StatEffectData::get_eventPercent()
extern void StatEffectData_get_eventPercent_mD5DEBE259A790F2192A8E71794117D188868CB7F (void);
// 0x0000011B System.Void StatEffectData::.ctor()
extern void StatEffectData__ctor_m2A05396F02559188384619E79D612794ECFF08D2 (void);
// 0x0000011C System.String UpgradeData::get_upgradeName()
extern void UpgradeData_get_upgradeName_m11838A2F769B71A117532719E0436180C6D494AA (void);
// 0x0000011D System.String UpgradeData::get_upgradeDescription()
extern void UpgradeData_get_upgradeDescription_m66CC8AE04910FB8EDB61E1A2032373FE7D07BC50 (void);
// 0x0000011E System.Int32 UpgradeData::get_price()
extern void UpgradeData_get_price_m6315317427C38B42CB2DB07ADA4C5A9EDC77DA80 (void);
// 0x0000011F UnityEngine.Sprite UpgradeData::get_upgradeSprite()
extern void UpgradeData_get_upgradeSprite_m4FC22BF9F48892BFB6F198938C0AC6B752F966B2 (void);
// 0x00000120 StatEffectData UpgradeData::get_statEffectData()
extern void UpgradeData_get_statEffectData_m74C632D00501CE19D0EA6276565CD6869FE36606 (void);
// 0x00000121 StatEffectData UpgradeData::get_secondaryStatEffectData()
extern void UpgradeData_get_secondaryStatEffectData_mE4CE8E348C290E2E9814C16D2AFBC8BF17D741B2 (void);
// 0x00000122 System.Int32 UpgradeData::get_upgradeTier()
extern void UpgradeData_get_upgradeTier_mCA6458D7AA250432E695113F1C57C715A8E8E123 (void);
// 0x00000123 System.Boolean UpgradeData::get_unlockNextTier()
extern void UpgradeData_get_unlockNextTier_mAC3769C932B1EE7996F722C92EE9AB56D822D38B (void);
// 0x00000124 System.Boolean UpgradeData::get_openGovernmentTab()
extern void UpgradeData_get_openGovernmentTab_mACACB61B2CF1B5BA814E820E43D97D857F69D9B6 (void);
// 0x00000125 System.Void UpgradeData::.ctor()
extern void UpgradeData__ctor_m19BB07FDA91289152BB1B640386238C25312E101 (void);
// 0x00000126 System.Void DistrictBrain::Start()
extern void DistrictBrain_Start_mC55A53A39B8520542033B8AE4BE3AA146E1CB2B1 (void);
// 0x00000127 System.Void DistrictBrain::Update()
extern void DistrictBrain_Update_m53874AC59B8EEF435C9E5C394222747535783CFE (void);
// 0x00000128 System.Void DistrictBrain::districtEconomyToCountryEconomy()
extern void DistrictBrain_districtEconomyToCountryEconomy_m5DB42ACAB5C6FF91F46D989CBC58926E9B18C6C4 (void);
// 0x00000129 System.Void DistrictBrain::districtStatToGameManager()
extern void DistrictBrain_districtStatToGameManager_m202E77DE9877B97552755F093EEFCF36E64D29A7 (void);
// 0x0000012A System.Void DistrictBrain::regionHovering()
extern void DistrictBrain_regionHovering_mB4992574F46CA7CB4E5F92678114BCF3CF2DD1BB (void);
// 0x0000012B System.Void DistrictBrain::PlayHoverSound()
extern void DistrictBrain_PlayHoverSound_m743BB4603B5EB6EABC168DBA1883C2673E6C20CB (void);
// 0x0000012C System.Void DistrictBrain::.ctor()
extern void DistrictBrain__ctor_mFC493167A7CFF35BACFAD56A981655B25BF2D3FA (void);
// 0x0000012D System.Void DistrictClass::start()
extern void DistrictClass_start_m509C4E464D6DD389637B48F7863F9F69EB04B97F (void);
// 0x0000012E System.Void DistrictClass::.ctor(UnityEngine.GameObject,UnityEngine.SpriteRenderer,UnityEngine.Sprite)
extern void DistrictClass__ctor_m5E88372038B07B6300650198ADA3CC47457A5511 (void);
// 0x0000012F CountryGovernment ElectionManager::get_countryGovernment()
extern void ElectionManager_get_countryGovernment_m59A8A2219700F158EF02E7749A2D5641DCE097AD (void);
// 0x00000130 GovernmentData ElectionManager::get_governmentData()
extern void ElectionManager_get_governmentData_mC948167CFC3674BDCC7727A20B247C45C4FC3173 (void);
// 0x00000131 System.Void ElectionManager::Start()
extern void ElectionManager_Start_m0B5590808113860EF37A1D7B60F266B3AE75FDA5 (void);
// 0x00000132 System.Void ElectionManager::Update()
extern void ElectionManager_Update_mE3B330A6E17A9B9CF00F7FB8708ECD6542C505CC (void);
// 0x00000133 System.Void ElectionManager::UpdateValue()
extern void ElectionManager_UpdateValue_mAE8753AB408948ECCB35A90EF177F0E171E5D9EA (void);
// 0x00000134 System.Void ElectionManager::VotingSound(UnityEngine.AudioClip)
extern void ElectionManager_VotingSound_mA687F0936E65B382802CB84D8A8CBDC43B84BBAC (void);
// 0x00000135 System.Void ElectionManager::EnterElection()
extern void ElectionManager_EnterElection_m266B3DFF2F59123CE8DD8F081D8ECC2776FCF517 (void);
// 0x00000136 System.Void ElectionManager::StartElection()
extern void ElectionManager_StartElection_mB1E27AC7EC835F5038FE155589E79BA42B1EDB75 (void);
// 0x00000137 System.Void ElectionManager::CloseElectionTab()
extern void ElectionManager_CloseElectionTab_m36BB1ABB87DD6755A1AF9292DB5908ABCAC5ECFF (void);
// 0x00000138 System.Void ElectionManager::UseMoney()
extern void ElectionManager_UseMoney_m84BD1601A6A3FF006EC43C5EF1F0A69EC2D68644 (void);
// 0x00000139 System.Void ElectionManager::PriceToBribe()
extern void ElectionManager_PriceToBribe_m2ED5D0AB693D21779D8A12B833FF170B8D9F828D (void);
// 0x0000013A System.Void ElectionManager::OpenCloseElectionUIButton()
extern void ElectionManager_OpenCloseElectionUIButton_mE55F36B3892E0E88C581D29ED9529503ED07EA0F (void);
// 0x0000013B System.Single ElectionManager::get_ElectionCalculate()
extern void ElectionManager_get_ElectionCalculate_mE2C1F9B38F0B73917668AB7C7F5D0D416B33D270 (void);
// 0x0000013C System.Void ElectionManager::NewCheatElectionValue()
extern void ElectionManager_NewCheatElectionValue_mA51DE15E649C77B08C362B69BC1D924038FC4853 (void);
// 0x0000013D System.Void ElectionManager::NewCheatResultValue()
extern void ElectionManager_NewCheatResultValue_mD24F2D0B056A54B8D07E9A49D2084E771647A846 (void);
// 0x0000013E System.Void ElectionManager::AfterElectionCheat()
extern void ElectionManager_AfterElectionCheat_m23D0A4587C1CAFA23361AD90C51A52334B5C45D1 (void);
// 0x0000013F System.Void ElectionManager::ElectionResultReport()
extern void ElectionManager_ElectionResultReport_m8A1FAB8A425511C781AE988974DEDA774A47F828 (void);
// 0x00000140 System.Void ElectionManager::DebugCheatingVoting()
extern void ElectionManager_DebugCheatingVoting_m1831DCAA67941EDB7DFF1C6A339B9A5862225FA2 (void);
// 0x00000141 System.Void ElectionManager::.ctor()
extern void ElectionManager__ctor_m0EDEA08C528A4674FB63C9D93960A6ACDA4D0891 (void);
// 0x00000142 System.Void GovernmentBrain::Start()
extern void GovernmentBrain_Start_m57AF6019298785EB7560B0CE3E13688B7E9A984E (void);
// 0x00000143 System.Void GovernmentBrain::startCallOut()
extern void GovernmentBrain_startCallOut_m93DE4D60EF69B3310CFB51CB2A57E35EF797F3AE (void);
// 0x00000144 System.Void GovernmentBrain::selectSystem()
extern void GovernmentBrain_selectSystem_m2FB1B7BB23F687DD70A9AC6CA86B11DC13659B24 (void);
// 0x00000145 System.Void GovernmentBrain::.ctor()
extern void GovernmentBrain__ctor_mCBA2F9B7E1FDB2B9579751E3E741F66D812811C5 (void);
// 0x00000146 PlayerInputManager PlayerInputManager::get_Instance()
extern void PlayerInputManager_get_Instance_mF8F7F5261FF5FAF01C2BBD0056369800A2372958 (void);
// 0x00000147 System.Void PlayerInputManager::Awake()
extern void PlayerInputManager_Awake_m604E659FF3A4DD2E1F03683B340186656960368E (void);
// 0x00000148 System.Void PlayerInputManager::OnEnable()
extern void PlayerInputManager_OnEnable_m7429B0738543BF35A8BCAED2F8CC0F714D9874D6 (void);
// 0x00000149 System.Void PlayerInputManager::OnDisable()
extern void PlayerInputManager_OnDisable_mFBA07FB429774CC92ED02D04C7B624690528AC54 (void);
// 0x0000014A UnityEngine.Vector2 PlayerInputManager::GetCamMovement()
extern void PlayerInputManager_GetCamMovement_m6020E13D390A483EF77A3E1090C0C0BDCE7BD341 (void);
// 0x0000014B UnityEngine.Vector2 PlayerInputManager::GetMouseDelta()
extern void PlayerInputManager_GetMouseDelta_m383452689DCFD8D1F6C1EFCD30108F67C8CF726C (void);
// 0x0000014C UnityEngine.Vector2 PlayerInputManager::GetMousePosition()
extern void PlayerInputManager_GetMousePosition_m2D034B68812106EB36E549E371F3F02E38A04371 (void);
// 0x0000014D UnityEngine.Vector2 PlayerInputManager::GetMouseScroll()
extern void PlayerInputManager_GetMouseScroll_m2651D1B3C1A39983C370889F4E660D0E7115CA98 (void);
// 0x0000014E System.Boolean PlayerInputManager::LeftClick()
extern void PlayerInputManager_LeftClick_m3DB0EA872EDB02AB5C5A9B4AA87639B7B6716974 (void);
// 0x0000014F System.Boolean PlayerInputManager::RightClick()
extern void PlayerInputManager_RightClick_m47E2EE6C05812A73C355C85423255AB582F56E41 (void);
// 0x00000150 System.Boolean PlayerInputManager::EscapePress()
extern void PlayerInputManager_EscapePress_m2F61942AD1759CB8E4F5B8689BFFFD9544A7D4EA (void);
// 0x00000151 System.Boolean PlayerInputManager::SpaceBarPress()
extern void PlayerInputManager_SpaceBarPress_mDC5B3E2C0B19773762B2693C1D8C14AFC3F61EC2 (void);
// 0x00000152 System.Boolean PlayerInputManager::tutorialNext()
extern void PlayerInputManager_tutorialNext_m9DD4B808F7ED3FBA3105C3E5E1B9D2B780FC4084 (void);
// 0x00000153 System.Boolean PlayerInputManager::CheatPress()
extern void PlayerInputManager_CheatPress_mEA946ABB139E20879D119FB96C77972232509467 (void);
// 0x00000154 System.Void PlayerInputManager::.ctor()
extern void PlayerInputManager__ctor_mD8A05B4F73F64E8BD5F0DEBAA227E240F3CCF566 (void);
// 0x00000155 System.Void ShipBrain::Start()
extern void ShipBrain_Start_m46244685DF92D7CA2ADD0A55098FA256B9F12C00 (void);
// 0x00000156 System.Void ShipBrain::Update()
extern void ShipBrain_Update_m59DBC28230860E7CD4B4956F8449E935C4310ED1 (void);
// 0x00000157 System.Void ShipBrain::.ctor()
extern void ShipBrain__ctor_m235F7ADD0E3BB7076FE5C83C78726EF743714FAE (void);
// 0x00000158 System.Void TurretBrain::Start()
extern void TurretBrain_Start_mDE0C02C8A41D1086014F451BE1673BD79EE4CEF6 (void);
// 0x00000159 System.Void TurretBrain::Update()
extern void TurretBrain_Update_m61D1F27A04A8F7C9D269DE378DF2449155823C4D (void);
// 0x0000015A System.Void TurretBrain::.ctor()
extern void TurretBrain__ctor_mF99056F6D2E2507079984F900BA2E0DC338108B7 (void);
// 0x0000015B System.Void TurretController::targetDetection()
extern void TurretController_targetDetection_m23A10C5771418102BF46883AB5450E0023173B54 (void);
// 0x0000015C System.Collections.IEnumerator TurretController::targetDetectionCoroutine()
extern void TurretController_targetDetectionCoroutine_m0863CC446CC1B4BF7AD0E35EF880D35D140406B5 (void);
// 0x0000015D System.Void TurretController::.ctor()
extern void TurretController__ctor_m473631F3C700500730044026BCF22B2116A71B54 (void);
// 0x0000015E System.Void TurretController/<targetDetectionCoroutine>d__6::.ctor(System.Int32)
extern void U3CtargetDetectionCoroutineU3Ed__6__ctor_mC8B7A8B1CED73E7F9F6B502438360E6952DA2248 (void);
// 0x0000015F System.Void TurretController/<targetDetectionCoroutine>d__6::System.IDisposable.Dispose()
extern void U3CtargetDetectionCoroutineU3Ed__6_System_IDisposable_Dispose_mB9DC26CED4951C9A00BA13465A551E3B28B6D602 (void);
// 0x00000160 System.Boolean TurretController/<targetDetectionCoroutine>d__6::MoveNext()
extern void U3CtargetDetectionCoroutineU3Ed__6_MoveNext_m900452B73E8D53ACC0233244B82DA370F6978E8D (void);
// 0x00000161 System.Object TurretController/<targetDetectionCoroutine>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtargetDetectionCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC31340DA08D735F8804197AB7C57EF0DD5C47122 (void);
// 0x00000162 System.Void TurretController/<targetDetectionCoroutine>d__6::System.Collections.IEnumerator.Reset()
extern void U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_m944CD9F92A9C4868E8D50FC36DF765670E37178C (void);
// 0x00000163 System.Object TurretController/<targetDetectionCoroutine>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m4C82B1B46097F2A043D70F19EBFCCA082A9BD3EB (void);
// 0x00000164 System.Void RegionBrain::Start()
extern void RegionBrain_Start_m6D9A3482F3520E499C42A961C0594E0A629A8913 (void);
// 0x00000165 System.Void RegionBrain::Update()
extern void RegionBrain_Update_mDE9100C051FCF1692F4C015C72982F313354C945 (void);
// 0x00000166 System.Void RegionBrain::regionHovering()
extern void RegionBrain_regionHovering_m15D56C88709BBD0393B6154E2428305B868DA3AA (void);
// 0x00000167 System.Void RegionBrain::changeHighlightColor()
extern void RegionBrain_changeHighlightColor_m9E333BB4E847522155EFE00E6E1DB6A8EFCA3FEE (void);
// 0x00000168 System.Void RegionBrain::resetColor()
extern void RegionBrain_resetColor_m1311E7B8F7A27763E4CC8E590DB81F19A8FA2E10 (void);
// 0x00000169 System.Void RegionBrain::.ctor()
extern void RegionBrain__ctor_m9AC85AD60108CE169BE79F758DF427684F6E933E (void);
// 0x0000016A System.Void TutorialManager::Start()
extern void TutorialManager_Start_m0CE3E9449B63E7930C4035203E1ED9B260E72D59 (void);
// 0x0000016B System.Void TutorialManager::Update()
extern void TutorialManager_Update_m0486AA97C0E1344C37EE31B176ED32EE3B4446D9 (void);
// 0x0000016C System.Void TutorialManager::SimpleTutorial()
extern void TutorialManager_SimpleTutorial_mA790574AB9E5CBF77FF2601F1303F90ECE3AB3F1 (void);
// 0x0000016D System.Void TutorialManager::PlaySession1()
extern void TutorialManager_PlaySession1_m7EE6128D7404D320A891A292410C878C8C79ED33 (void);
// 0x0000016E System.Void TutorialManager::PlaySession2()
extern void TutorialManager_PlaySession2_m775D8D8A1B829140880092A81BC491302ED82F01 (void);
// 0x0000016F System.Void TutorialManager::PlaySession3()
extern void TutorialManager_PlaySession3_mC74B91AC4AC7D9F418FDE823614CB9299E1E9F39 (void);
// 0x00000170 System.Void TutorialManager::PlaySession4()
extern void TutorialManager_PlaySession4_m4739D2284EA994370681182096E0AD274D12C56C (void);
// 0x00000171 System.Void TutorialManager::PlaySession5()
extern void TutorialManager_PlaySession5_mDC42E4AFE68DFDAEE8A817EB04B1AECCF1DF737E (void);
// 0x00000172 System.Void TutorialManager::PlaySessionRanking()
extern void TutorialManager_PlaySessionRanking_mD07203D95432994C9E32685340B4032DAC15F1F4 (void);
// 0x00000173 System.Void TutorialManager::PlaySession6()
extern void TutorialManager_PlaySession6_m64F8F9692FC7599BB1DF38F045FD3E5ACDDA74F4 (void);
// 0x00000174 System.Void TutorialManager::StopPlayerMovement()
extern void TutorialManager_StopPlayerMovement_m74333738AEBA9C5BC31FBE387DE83AE0A5E27FD5 (void);
// 0x00000175 System.Void TutorialManager::.ctor()
extern void TutorialManager__ctor_m1A6F653729E4CCE30F0A0BC39EEB304409459A84 (void);
// 0x00000176 System.Void TutorialTextBrain::Start()
extern void TutorialTextBrain_Start_m2B99EA0792942D055DFC7F6D3740670008A2E3EB (void);
// 0x00000177 System.Void TutorialTextBrain::.ctor()
extern void TutorialTextBrain__ctor_m87E4C272DF0BF017A8EBB50072348BE9FF70BBA3 (void);
// 0x00000178 System.Void CalenderTimer::Start()
extern void CalenderTimer_Start_m6BF381D5D058C7E1D9564C90235E549874DD2DC5 (void);
// 0x00000179 System.Void CalenderTimer::Update()
extern void CalenderTimer_Update_mA2CAA2E80284E8C0AD1089C7AC7EBE852F59675A (void);
// 0x0000017A System.Void CalenderTimer::Timer()
extern void CalenderTimer_Timer_m103F133D645200CD8754FEA50812C2794C937034 (void);
// 0x0000017B System.Void CalenderTimer::changeSpeed(System.Int32)
extern void CalenderTimer_changeSpeed_m950D1E22578EAF9763653C65A8D853BF3C5E9127 (void);
// 0x0000017C System.Void CalenderTimer::pauseButton()
extern void CalenderTimer_pauseButton_m45CAB65FCADFC9C001C5D11A99241EA43B908D73 (void);
// 0x0000017D System.Void CalenderTimer::.ctor()
extern void CalenderTimer__ctor_mED929E0471C84B2F9C9534D7219ABE9B28FB9738 (void);
// 0x0000017E System.Int32 CountryRankBrain::get_rankNumber()
extern void CountryRankBrain_get_rankNumber_m0104A98522D35471E5C9EB6190C8DBF8D594E277 (void);
// 0x0000017F System.Single CountryRankBrain::get_scoreNumber()
extern void CountryRankBrain_get_scoreNumber_m7F658C58FD9FBAA939678BB064C0438C8B289677 (void);
// 0x00000180 System.Int32 CountryRankBrain::get_currentIndex()
extern void CountryRankBrain_get_currentIndex_m1E2A6BF2C123196DD515EAFEB50DA8D8D640A1DF (void);
// 0x00000181 System.Boolean CountryRankBrain::get_isPlayer()
extern void CountryRankBrain_get_isPlayer_m2E17DC36F29801A1214D110469D402556F3FCA7D (void);
// 0x00000182 System.Void CountryRankBrain::SetUpBrain(System.String,System.Int32,System.Single,System.Boolean)
extern void CountryRankBrain_SetUpBrain_m0C5330863FAEB70491C86250477D42774F87B95C (void);
// 0x00000183 System.Void CountryRankBrain::setRankScore(System.Int32,System.Single)
extern void CountryRankBrain_setRankScore_m95C01F331F07C7A9B31F550664CC1C4C0685BB19 (void);
// 0x00000184 System.Void CountryRankBrain::changeScore(System.Single)
extern void CountryRankBrain_changeScore_mABC6F74148629F2E0B151E052CAFD88621F39DEE (void);
// 0x00000185 System.Void CountryRankBrain::changeRank(System.Int32)
extern void CountryRankBrain_changeRank_m4E73C8412CBA373A10F50D1DA93F46AF7F59F415 (void);
// 0x00000186 System.Void CountryRankBrain::.ctor()
extern void CountryRankBrain__ctor_m2D50AC51BA826FED36BE56F552646A44F4900890 (void);
// 0x00000187 System.Void CountryRankManager::Start()
extern void CountryRankManager_Start_m1DE6784A2A4877BA5DCEB11AC4CC2BDBA2D0D369 (void);
// 0x00000188 System.Void CountryRankManager::Update()
extern void CountryRankManager_Update_m65E1BB9980D711210DBA24A37419E7993B3497DD (void);
// 0x00000189 System.Void CountryRankManager::Ranking()
extern void CountryRankManager_Ranking_m05D9F53A981AC129BC1EEE1289DF7E6D089BA43B (void);
// 0x0000018A System.Void CountryRankManager::SpawnSetup()
extern void CountryRankManager_SpawnSetup_mD40C56A4F575DE8119138C9CEBA148514CEDB29B (void);
// 0x0000018B System.Void CountryRankManager::SpawnPlayerRank()
extern void CountryRankManager_SpawnPlayerRank_m9BA65494353532C11B4AB4AB287DB3B86818058A (void);
// 0x0000018C System.Single CountryRankManager::get_ScoreCalculator()
extern void CountryRankManager_get_ScoreCalculator_m8DD3DAF682481DD72F8FD61F8F03F80361788B9F (void);
// 0x0000018D System.Void CountryRankManager::OpenCloseEventUIButton()
extern void CountryRankManager_OpenCloseEventUIButton_m68CC5861E43D0E28556442CF028176C6874B09C4 (void);
// 0x0000018E System.Void CountryRankManager::.ctor()
extern void CountryRankManager__ctor_m996302B6194B7EB29EA12EAB6E60BE333FD5FDB7 (void);
// 0x0000018F System.Void CountryStatBarUpdate::Start()
extern void CountryStatBarUpdate_Start_mAB8332441BF9F670AE27A98BD56964E7E0518787 (void);
// 0x00000190 System.Void CountryStatBarUpdate::Update()
extern void CountryStatBarUpdate_Update_mC39605D75D9331A0F1B2DAE51EE234848DD93914 (void);
// 0x00000191 System.Void CountryStatBarUpdate::updateBar()
extern void CountryStatBarUpdate_updateBar_m9A8CE752A9774F6F1F19F32B6E129703E67E1FEE (void);
// 0x00000192 System.Void CountryStatBarUpdate::.ctor()
extern void CountryStatBarUpdate__ctor_mFB9B1B26184EA916BCD8A0C0C3EBAC5A9BC11A7A (void);
// 0x00000193 System.Void ElectionBar::Start()
extern void ElectionBar_Start_m2FFFB413BBD2F87F1FC5BB8610B1EE792532F562 (void);
// 0x00000194 System.Void ElectionBar::Update()
extern void ElectionBar_Update_mFC726ECBE088C9B8EAAD4A164321E7CDD9E3A67A (void);
// 0x00000195 System.Void ElectionBar::ElectionUpdate()
extern void ElectionBar_ElectionUpdate_m785D72B10A7C45725691AF75FF422180C711BF5F (void);
// 0x00000196 System.Void ElectionBar::.ctor()
extern void ElectionBar__ctor_mAA0877B53DE097FD67854D0B4FD9089AEC05C5D7 (void);
// 0x00000197 System.Void InformationText::Start()
extern void InformationText_Start_mFE511E8C05973A5905C14D34EAE51B30AD2EC695 (void);
// 0x00000198 System.Collections.IEnumerator InformationText::textDisappear(System.Single)
extern void InformationText_textDisappear_mF0444AE73D21EBFA1564E06201BD84889D929635 (void);
// 0x00000199 System.Void InformationText::SetText(System.String,System.Single,UnityEngine.Color)
extern void InformationText_SetText_m819FAC546E50E90C5921E4F4AF4CB3C6F1831E89 (void);
// 0x0000019A System.Void InformationText::SetMoneyText(System.String,System.Single,UnityEngine.Color)
extern void InformationText_SetMoneyText_m711C3300D7D3495EFDF2A3D98E993FBDBE4CEC2A (void);
// 0x0000019B System.Void InformationText::selfDestroy()
extern void InformationText_selfDestroy_mA11162CA297ACCF1FAF937EB6D0083AF4E11CFF9 (void);
// 0x0000019C System.Void InformationText::.ctor()
extern void InformationText__ctor_m5FA57BDA8717F3A8B2A7D54F4139A9700ABFB85C (void);
// 0x0000019D System.Void InformationText/<textDisappear>d__5::.ctor(System.Int32)
extern void U3CtextDisappearU3Ed__5__ctor_m8DFA67F978C62965EFC1AF45AC6E991523C98153 (void);
// 0x0000019E System.Void InformationText/<textDisappear>d__5::System.IDisposable.Dispose()
extern void U3CtextDisappearU3Ed__5_System_IDisposable_Dispose_mE481914892D7D05A8CC111A5FFE4C9D7CD39B455 (void);
// 0x0000019F System.Boolean InformationText/<textDisappear>d__5::MoveNext()
extern void U3CtextDisappearU3Ed__5_MoveNext_mE17846254DA0C2A71BDE15991D88C512AD9BDB16 (void);
// 0x000001A0 System.Object InformationText/<textDisappear>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtextDisappearU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2C8295679F0BC19976D82D542416718DEDDF25A (void);
// 0x000001A1 System.Void InformationText/<textDisappear>d__5::System.Collections.IEnumerator.Reset()
extern void U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_Reset_m981818C756CF88BE287D53C3CE979E0A64036394 (void);
// 0x000001A2 System.Object InformationText/<textDisappear>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_get_Current_mC8F4D7976B07AFEA6EB40BBC5577579B62F135A6 (void);
// 0x000001A3 UnityEngine.Color InformationbarUpdate::get_spawnTitleColor()
extern void InformationbarUpdate_get_spawnTitleColor_m1C925EC8F26FEB7D18D25B115C838EA6A119FF4A (void);
// 0x000001A4 UnityEngine.Color InformationbarUpdate::get_spawnPositiveDescriptionColor()
extern void InformationbarUpdate_get_spawnPositiveDescriptionColor_m0B81894C6B5FBD0C4D43AD8F642495932FE942C1 (void);
// 0x000001A5 UnityEngine.Color InformationbarUpdate::get_spawnNegativeDescriptionColor()
extern void InformationbarUpdate_get_spawnNegativeDescriptionColor_m9D01262E52C263C78064441628488FB72800DAE8 (void);
// 0x000001A6 System.Void InformationbarUpdate::spawnText(System.String,System.Single,System.Boolean)
extern void InformationbarUpdate_spawnText_mF84AB3C416C3296585C9C653DC0059B4CF4F05A1 (void);
// 0x000001A7 System.Void InformationbarUpdate::.ctor()
extern void InformationbarUpdate__ctor_m4C1CF1B1788A29A8262CD84EC184B22BA1F496C8 (void);
// 0x000001A8 System.Void MenuManager::Start()
extern void MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2 (void);
// 0x000001A9 System.Void MenuManager::onStart()
extern void MenuManager_onStart_m37A432E425B6C737EEC07E155DD5E28006167692 (void);
// 0x000001AA System.Void MenuManager::playOpen()
extern void MenuManager_playOpen_m009219C377403B43F6D50C5A0FCE324638A7C7D0 (void);
// 0x000001AB System.Void MenuManager::settingOpen()
extern void MenuManager_settingOpen_m50C827E8AC177315BFF3D25031737E0A4F268963 (void);
// 0x000001AC System.Void MenuManager::creditOpen()
extern void MenuManager_creditOpen_m370B5D6F6500A4C3AE4708911A0AE8813034DAEC (void);
// 0x000001AD System.Void MenuManager::returnToMenu()
extern void MenuManager_returnToMenu_mD96BF329F1B7E8CC9F3F48D780125006D056ACDD (void);
// 0x000001AE System.Void MenuManager::loadScene(System.Int32)
extern void MenuManager_loadScene_mB717D5E9676CEF035C842EF68CE71DC876F68F3A (void);
// 0x000001AF System.Void MenuManager::exitGame()
extern void MenuManager_exitGame_m2398AD601B7A003C8292E90B41BEEE1ECCC8337A (void);
// 0x000001B0 System.Void MenuManager::changeCountryName(System.Int32)
extern void MenuManager_changeCountryName_m678BD873168A8F695F574EC856D8111293ECB9C3 (void);
// 0x000001B1 System.Void MenuManager::loadTutorialStage(System.Int32)
extern void MenuManager_loadTutorialStage_m993543543599FA4413065AAFF9977BB027ACA302 (void);
// 0x000001B2 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x000001B3 GameData PauseMenu::get_gameData()
extern void PauseMenu_get_gameData_mDED7068E36894CB5DCFF10639AB49783B50B9B7D (void);
// 0x000001B4 System.Void PauseMenu::Start()
extern void PauseMenu_Start_mB3762C9E5B204FDE0381A6409728D7DCAD7E6C37 (void);
// 0x000001B5 System.Void PauseMenu::Update()
extern void PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57 (void);
// 0x000001B6 System.Void PauseMenu::onStart()
extern void PauseMenu_onStart_m6603E7C3E54C72676E857FB0E2140F5526F9969B (void);
// 0x000001B7 System.Void PauseMenu::PauseAndResume()
extern void PauseMenu_PauseAndResume_m62DAF4151BA954D2C4A9D45A15F2006223CEDD23 (void);
// 0x000001B8 System.Void PauseMenu::Pause()
extern void PauseMenu_Pause_m395165A04A026E9974327328181ACFA512DD76C7 (void);
// 0x000001B9 System.Void PauseMenu::Resume()
extern void PauseMenu_Resume_m256AFDD68DF9851B6D716189F709ED93D45C3717 (void);
// 0x000001BA System.Void PauseMenu::OpenOptions()
extern void PauseMenu_OpenOptions_mCE41B2A7D1EF301C1B3A0245AA77A21527721864 (void);
// 0x000001BB System.Void PauseMenu::Return()
extern void PauseMenu_Return_m34397667F74A7177E2DCFF524B13594B5785AAF5 (void);
// 0x000001BC System.Void PauseMenu::loadScene(System.Int32)
extern void PauseMenu_loadScene_mDD0743151614450F11B1AD8249E9EA02BE6BDA8D (void);
// 0x000001BD System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018 (void);
// 0x000001BE System.Void ResolutionSetting::SetResolution(System.Int32)
extern void ResolutionSetting_SetResolution_m6CE1C2CA19F20697FFF1F93D0A4E7FD718D282C2 (void);
// 0x000001BF System.Void ResolutionSetting::SetFullscreen(System.Boolean)
extern void ResolutionSetting_SetFullscreen_m9A3ACC61D389703B6579FD109FCB33F77911A5A8 (void);
// 0x000001C0 System.Void ResolutionSetting::dropDownCurrentStartResolution()
extern void ResolutionSetting_dropDownCurrentStartResolution_m561E225092EB39044346CA7CF855022DEA3AD828 (void);
// 0x000001C1 System.Void ResolutionSetting::.ctor()
extern void ResolutionSetting__ctor_mB0A19DB25DD45D37837224D84E80A95C1F0739AD (void);
// 0x000001C2 System.Void SceneLoader::Awake()
extern void SceneLoader_Awake_m0618C263DDE94B4607D9D5148F87247B506A337C (void);
// 0x000001C3 System.Void SceneLoader::startLoadLevel(System.Int32)
extern void SceneLoader_startLoadLevel_m04754948AF7C6B19FB5FB11C8875F6F6B8D3EA25 (void);
// 0x000001C4 System.Void SceneLoader::exitToDestop()
extern void SceneLoader_exitToDestop_mABAD45692C757391FE8C86289AC7128ABFECC8F3 (void);
// 0x000001C5 System.Collections.IEnumerator SceneLoader::loadlevel(System.Int32)
extern void SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97 (void);
// 0x000001C6 System.Collections.IEnumerator SceneLoader::exitGame()
extern void SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36 (void);
// 0x000001C7 System.Void SceneLoader::.ctor()
extern void SceneLoader__ctor_m8708D080848349110CEA260D8779F30BD5823912 (void);
// 0x000001C8 System.Void SceneLoader/<loadlevel>d__6::.ctor(System.Int32)
extern void U3CloadlevelU3Ed__6__ctor_m9E1A61EAB40A3F7E55748634CF1479A83D7F0918 (void);
// 0x000001C9 System.Void SceneLoader/<loadlevel>d__6::System.IDisposable.Dispose()
extern void U3CloadlevelU3Ed__6_System_IDisposable_Dispose_m3843887F758EDED505C5740E27804345C91A4EA5 (void);
// 0x000001CA System.Boolean SceneLoader/<loadlevel>d__6::MoveNext()
extern void U3CloadlevelU3Ed__6_MoveNext_m68C2A1F46A5BA914646835FBD00B903007D5823C (void);
// 0x000001CB System.Object SceneLoader/<loadlevel>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CloadlevelU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66A182F82237A6982B1215DFBC583137A405D67B (void);
// 0x000001CC System.Void SceneLoader/<loadlevel>d__6::System.Collections.IEnumerator.Reset()
extern void U3CloadlevelU3Ed__6_System_Collections_IEnumerator_Reset_mC33203E5184B62410066554665A683CBBA2473D2 (void);
// 0x000001CD System.Object SceneLoader/<loadlevel>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CloadlevelU3Ed__6_System_Collections_IEnumerator_get_Current_m42EAEC5702621CBB823CE3BC0BB3AAA3FCD734D0 (void);
// 0x000001CE System.Void SceneLoader/<exitGame>d__7::.ctor(System.Int32)
extern void U3CexitGameU3Ed__7__ctor_m34E1725C8F3B9C5DD0D1FDDF031FFA605C0B536E (void);
// 0x000001CF System.Void SceneLoader/<exitGame>d__7::System.IDisposable.Dispose()
extern void U3CexitGameU3Ed__7_System_IDisposable_Dispose_m215E49BCB1D7D0EFBC5830D1BD850C61C13AF9E0 (void);
// 0x000001D0 System.Boolean SceneLoader/<exitGame>d__7::MoveNext()
extern void U3CexitGameU3Ed__7_MoveNext_m9893BA2217E1D8E3EC7678321B9F3912D4F1B263 (void);
// 0x000001D1 System.Object SceneLoader/<exitGame>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CexitGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9120CA81B69CCFA43D38D74EE44FE8D7A435F707 (void);
// 0x000001D2 System.Void SceneLoader/<exitGame>d__7::System.Collections.IEnumerator.Reset()
extern void U3CexitGameU3Ed__7_System_Collections_IEnumerator_Reset_mDCFD249F2AF563D7D20D1091C1772940EA653B3B (void);
// 0x000001D3 System.Object SceneLoader/<exitGame>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CexitGameU3Ed__7_System_Collections_IEnumerator_get_Current_m9807A48ED782BD25BC3E49EA47642C45B903ABEA (void);
// 0x000001D4 System.Void SceneLoaderCheck::transitioningSwitch()
extern void SceneLoaderCheck_transitioningSwitch_m22454809B7646961A344498212874F4F70E0FC1D (void);
// 0x000001D5 System.Void SceneLoaderCheck::transitionCompleteSwitch()
extern void SceneLoaderCheck_transitionCompleteSwitch_m141092CC40E193C2A57FC4326CED10D3021A3932 (void);
// 0x000001D6 System.Void SceneLoaderCheck::.ctor()
extern void SceneLoaderCheck__ctor_mAF65D9F76BB3A414D42F532F7A00A4D4397989BA (void);
// 0x000001D7 System.Void UpgradeItemUI::Start()
extern void UpgradeItemUI_Start_mCECD4E4957C906C234B03F67777832943AE8E3B3 (void);
// 0x000001D8 System.Void UpgradeItemUI::Update()
extern void UpgradeItemUI_Update_m347754ADFE3AEE0FEEC3568D96586302CE23BAE2 (void);
// 0x000001D9 System.Void UpgradeItemUI::check()
extern void UpgradeItemUI_check_mD67D15B0FAF0B7F7381622E5B3B999DA0A03676D (void);
// 0x000001DA System.Void UpgradeItemUI::buyUpgrade()
extern void UpgradeItemUI_buyUpgrade_m330820EEB8F9FEDC4F52857C3B5A69E7CEE95005 (void);
// 0x000001DB System.Void UpgradeItemUI::changeUpgradeText()
extern void UpgradeItemUI_changeUpgradeText_m57343E414F9D313511D812809CE37C11BCFCF88C (void);
// 0x000001DC System.Void UpgradeItemUI::clearUpgradeText()
extern void UpgradeItemUI_clearUpgradeText_m3511EBCC5C2C85E820BD5CC93D54A1E6AE1CCCC3 (void);
// 0x000001DD System.Void UpgradeItemUI::OnHoverUI()
extern void UpgradeItemUI_OnHoverUI_mBE3D1963E403B86798A10CF9874866C164FEF8C3 (void);
// 0x000001DE System.Void UpgradeItemUI::OnClickUI()
extern void UpgradeItemUI_OnClickUI_m1D1B490C888F8757E439D23349A23EA368D7F4DE (void);
// 0x000001DF System.Void UpgradeItemUI::.ctor()
extern void UpgradeItemUI__ctor_m8E161961355F560EB46B0DC329DCFD84AB9D8DA0 (void);
// 0x000001E0 UnityEngine.GameObject UpgradeUI::get_upgradeContentTab()
extern void UpgradeUI_get_upgradeContentTab_mA9522FBA98323AA9155C38E4F2E2FA1A8E423734 (void);
// 0x000001E1 UnityEngine.GameObject UpgradeUI::get_governmentTab()
extern void UpgradeUI_get_governmentTab_m283782268755E34A62BEAA2D5213D2132FC8C04D (void);
// 0x000001E2 CountryUpgrade UpgradeUI::get_countryUpgrade()
extern void UpgradeUI_get_countryUpgrade_m05B8077D76597F847F7AF799DAE409EAEA88E874 (void);
// 0x000001E3 System.Void UpgradeUI::Start()
extern void UpgradeUI_Start_mFA6E269456D93CEA02D684B20804673E4968C4E3 (void);
// 0x000001E4 System.Void UpgradeUI::OpenCloseUpgradeUIButton()
extern void UpgradeUI_OpenCloseUpgradeUIButton_m7167EC675FD542F02F8FCECDDCBD1320C22A00FB (void);
// 0x000001E5 System.Void UpgradeUI::changeUpgradeText(System.String,System.String)
extern void UpgradeUI_changeUpgradeText_m894D5DE80D03B3922476382780B94EF7D0D8E058 (void);
// 0x000001E6 System.Void UpgradeUI::spawnUpgrade()
extern void UpgradeUI_spawnUpgrade_mFFA57776A8A8A156CA48BD739BCE6F7BF2E6BBEC (void);
// 0x000001E7 System.Int32 UpgradeUI::checkCurrentTier()
extern void UpgradeUI_checkCurrentTier_m5BEEAD00E65B321E5D102A1CAC3F11BE995CFFB5 (void);
// 0x000001E8 System.Void UpgradeUI::unlockTier(System.Int32)
extern void UpgradeUI_unlockTier_m6D4A273DEE587DFEB9BE4C024A35502322675145 (void);
// 0x000001E9 System.Void UpgradeUI::.ctor()
extern void UpgradeUI__ctor_m5C6191FD99F1E10B4D1115F06471E39E59727EF1 (void);
// 0x000001EA System.String WinLoseManager::get_winDescription()
extern void WinLoseManager_get_winDescription_m1160B0F5C018A1807668D47F1A74F278F3C911C8 (void);
// 0x000001EB System.String WinLoseManager::get_loseDescriptionVoting()
extern void WinLoseManager_get_loseDescriptionVoting_mCD6FE18EC2A7AA0E6F61667DBA9569EC23394953 (void);
// 0x000001EC System.String WinLoseManager::get_loseDescriptionOverthrown()
extern void WinLoseManager_get_loseDescriptionOverthrown_m11AF8413454755B9BA53617373012193B65D2417 (void);
// 0x000001ED System.String WinLoseManager::get_loseDescriptionNoMoney()
extern void WinLoseManager_get_loseDescriptionNoMoney_m736AC40B6FCE70638F0AB52987E9508E4733938A (void);
// 0x000001EE System.Void WinLoseManager::Start()
extern void WinLoseManager_Start_m2E32930E3AF736B1D19D5F101559E63754AE4CE6 (void);
// 0x000001EF System.Void WinLoseManager::Update()
extern void WinLoseManager_Update_mD99C4FF5969FC2B24E42809CCBFD4FCAA49FCBF6 (void);
// 0x000001F0 System.Void WinLoseManager::GameOver(System.String)
extern void WinLoseManager_GameOver_mF9911D19E50D5039EFBB0BA3D8D108D9EFA8E620 (void);
// 0x000001F1 System.Void WinLoseManager::WinGame()
extern void WinLoseManager_WinGame_m1342034D770605BE666C0AB4E3B95533F11011BA (void);
// 0x000001F2 System.Void WinLoseManager::loadScene(System.Int32)
extern void WinLoseManager_loadScene_m413563DFDD56316B7E5D5D1E0FDF4FF11213D931 (void);
// 0x000001F3 System.Void WinLoseManager::resetScene()
extern void WinLoseManager_resetScene_mF7A5CBCBF5529E201E1D74041F31AFCFADF50490 (void);
// 0x000001F4 System.Void WinLoseManager::LoseCondition()
extern void WinLoseManager_LoseCondition_m8AE6F37675B9248E7B114B81A3975C79C5415A85 (void);
// 0x000001F5 System.Void WinLoseManager::OpenCloseElectionUIButton()
extern void WinLoseManager_OpenCloseElectionUIButton_mD2F0B63D1E3F0C905234196290946F07CB9F34C1 (void);
// 0x000001F6 System.Void WinLoseManager::.ctor()
extern void WinLoseManager__ctor_m9BEA6A4A6A01273878129498C2CC4177230EDAC9 (void);
// 0x000001F7 System.Void moneyUpdate::Start()
extern void moneyUpdate_Start_m60F18BE7A4CA09B2BD23F203FD220F18E63DF14D (void);
// 0x000001F8 System.Void moneyUpdate::Update()
extern void moneyUpdate_Update_mEBFDC6BD2C393AB7588329D8F25914498DFDA8F6 (void);
// 0x000001F9 System.Void moneyUpdate::money()
extern void moneyUpdate_money_m45C7AD16C50D5E37D8C8C8397FC33AFF98B765DF (void);
// 0x000001FA System.Void moneyUpdate::updateMoney(System.Int32)
extern void moneyUpdate_updateMoney_m78DC7861B9206BEF03043D40240DBCA4FB671E71 (void);
// 0x000001FB System.Void moneyUpdate::.ctor()
extern void moneyUpdate__ctor_m1C4DC32ACB5BB0C130042F32BB94DB8AF72823DD (void);
static Il2CppMethodPointer s_methodPointers[507] = 
{
	Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF,
	Input_master__ctor_m05570375675155D2756C12748D40E3A55E995118,
	Input_master_Dispose_m4E1AB80951FBD83DE2BBD7152B250AD5B62DD907,
	Input_master_get_bindingMask_mA351518215DD2A46E0AC3B43C4F21E07FF4D6DAB,
	Input_master_set_bindingMask_m47CCB6C56E9964B4F39B0C3D83AD6FBFBACCD5DA,
	Input_master_get_devices_mC144889E948D43D5E0BE4762C1897D94CACAEDE8,
	Input_master_set_devices_mCE7B8BD9B4FA936178C0A7A5012C01D6227BD8C9,
	Input_master_get_controlSchemes_mA89274C4467E476A0A54E36E5AE827B527453038,
	Input_master_Contains_m3B11EA2D2A14801BD2ECD5CE5FDF548B82EFFCC3,
	Input_master_GetEnumerator_m576439B219C6489068DF54CD3A888D911AF81BD4,
	Input_master_System_Collections_IEnumerable_GetEnumerator_m5C5C020C36FECC05CBA84F4112857A516B18059C,
	Input_master_Enable_mAA5A4A5A3B5048D37FE1BE9BE3E654439F40771D,
	Input_master_Disable_m375CAC17441BA7AEFDE01B73AC869E9974E00A6A,
	Input_master_get_Player_mC7F6E1D47AB6D2D60FC4C5397098EB159F4660B9,
	PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621,
	PlayerActions_get_CameraMovement_m9537EE57866532B0778B7AFEBCE6DACEA9288508,
	PlayerActions_get_MouseDelta_m392EE5FB891CDA0EB030C4F407DA0F4A0AE67194,
	PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26,
	PlayerActions_get_MouseScroll_m988F2A5C3F04837EAA4D88F052D7870DEC3F6B31,
	PlayerActions_get_LeftClick_mBA9C4A91D948482870A5B3D6F299F1C2BEF78B23,
	PlayerActions_get_RightClick_m613ED5127E32C7A730EA7B303B37CFD6E5B2A402,
	PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD,
	PlayerActions_get_Spacebar_m70DE77C67F95D3504D5250C5BC4299D65E907C7E,
	PlayerActions_get_F1Cheat_mC8E401D56F3F86E371011488053C2B0D46393FFA,
	PlayerActions_get_TutorialNext_m5D0CA16ABE26948ECDF5421ABE9F6E9F304C9FE3,
	PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8,
	PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C,
	PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9,
	PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7,
	PlayerActions_op_Implicit_m00773CF39CF596BDE1B46506EC4558388936D99B,
	PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CountryAudio_get_Instance_m58C639822654FFFEEED2F06E4A82644DA29842D6,
	CountryAudio_Start_m8E8FA545C51B4885513FF601E78B64C0A20A8596,
	CountryAudio_ChangeGovernmentAudio_m9822E0E03403C9B3F91693532CE02EA3AD474B5C,
	CountryAudio__ctor_mAF9408E08926ACCF56FCE41146DB8D632F79970B,
	SoundManager_get_Instance_mB2D05669AF91B4D14A63BC76820A362B6F36AB90,
	SoundManager_get_bgmMainmenu_m8368E91C1207C6C2295598F7C010BD28D264A4E3,
	SoundManager_get_bgmWinning_mE47BC72B0C8282ED2F630569B9CC47AD569305B5,
	SoundManager_get_bgmLosing_m0E9C51BB4FEFEC329D0EAD0676ADC275CE00CCC2,
	SoundManager_get_bgmDemocracy_mF2416A9A2869F1B4995BDD8548C69F063B6926B0,
	SoundManager_get_bgmSocialism_mBF5912E35F9784322A14EB4F353319C101F09E92,
	SoundManager_get_bgmMonarchy_m3913D831D3179DBE89A76F94586802AAD357EE6F,
	SoundManager_get_bgmDictator_mEA0AE2463B56B9874D2DDB784D2337AAD6DA0B3D,
	SoundManager_get_bgmCommunism_m878710D150CCCFBD7F0DEA470F65E21B3FD20BD0,
	SoundManager_get_clicking_m3B7E901A4E7D6FB81ABF045C493202D423E4DA1D,
	SoundManager_get_hovering_m84EF844D72F5D23F13D13F7A172A7C45F9BEDDD0,
	SoundManager_get_popup_mFBE01E1F909C214E7EB8996714D6F4314F8CA1F8,
	SoundManager_get_electionVoting_mCA570C8E698306EB17BE3FC95789202B83BC30BA,
	SoundManager_get_afterVoting_mBB24CC18BFD589EDAEE0E28C716AB22398DDC44C,
	SoundManager_Awake_m78F953F39CFB3F539240E1226D04270B793B1A76,
	SoundManager_Update_mDD188B65FF1E9B1DF1B8345A6290D149E70E657C,
	SoundManager_CheckIfPlaying_m74F7C53D4283A6061B3A3516308D994AF281DC87,
	SoundManager_OnSceneLoaded_m77B3E475EE4B25348C20D0B125F9FF6690057132,
	SoundManager_ChooseListBGM_m82117BF5751922FE01517FA5F7DE0C3035D74DD9,
	SoundManager_ChooseClipBGM_m212EB6CC2532F9EF2F430592AF3C562AAD4468C4,
	SoundManager_OnHoverUI_mF2796CAFCC262451D8BF99918CBBF013B0AABA21,
	SoundManager_OnClickUI_m4AAC7AE59343DA3C3B8D08F0230159F3317F5DD3,
	SoundManager_PlaySFX_m816BF84FC9AAB3A1199B472F908AF7796823169B,
	SoundManager_RandomClip_mA883AE254C7198EA6A2660CB9AB412136E38464A,
	SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE,
	VolumeMixer_Awake_m23F7C0578529E573B13C7A8FDC402A310BFD12B6,
	VolumeMixer_SetVolume_mFE149DEB806A9D3FDAE5242829C35248A479D795,
	VolumeMixer_PlayEffectOnce_mC3AD134F86585FBA710D30736C38E412224012BD,
	VolumeMixer__ctor_m0026E479FD2F148D6067EF304C73ECDE1E915FCE,
	CameraController_Start_mBDE87C2FCF352957C2B86B67610667663422FBE6,
	CameraController_Update_m3C257AC762117CFDDAD03C9C4FBBFDE51C61D534,
	CameraController_cameraMovement_mBC9FD807DE88226C18451AC26ED8EA1F5B6222E1,
	CameraController_cameraScroll_m41835690BF9C5E682F39CC8AC516C58AEF45202A,
	CameraController_cameraFocus_m3B5D6E8ED0C1ED2DCB804C182F76EF83C1B635D1,
	CameraController__ctor_m07EC5A8C82742876097619BE7DD9043F47327DAE,
	CardEventBrain_get_backCard_m3648CBCC9F0D7CB5EAE50925723C4AA761AB602A,
	CardEventBrain_get_frontCard_mAAF6B2A29764F9AB5E149C279F74BA1FEB3ECF0A,
	CardEventBrain_Start_mE3D499C19DF45EE0E3383EC85577FEC4E9CDF98E,
	CardEventBrain_Update_mBFEDF61DC23FF9125BB001105C4E25611EA99C47,
	CardEventBrain_PlayAnimation_m3F3BDE54E889825DA105D61E1DB2D80A636437E9,
	CardEventBrain_setCardEventData_mA9FBF82EFC14698F2AF6C1C8BBD89F2272011F99,
	CardEventBrain_OnPointerEnter_mA2F34E7081E26E374BF855FE1BF99380C725A8D6,
	CardEventBrain_OnPointerExit_m45F4BB6F288BECCE7A63B3A633FB23A0082A2FED,
	CardEventBrain_OnHoverUI_m77C92A7D39B4B2F111101E7F6311008085EA01B0,
	CardEventBrain_OnClickUI_m5938FF276B383F15280960D4AB0897AF1FB54E92,
	CardEventBrain__ctor_m114A6315B7805CB1AFB9C47AD8A462479C19576C,
	CardEventManager_get_alreadySelectCard_m9A9ABB70838B7D3FE3234D649A03D103DB450894,
	CardEventManager_Start_mE198601839CBAF404F275AB0DE4F954594732D59,
	CardEventManager_Update_m932B4298D6887A07B0A97EAEEB723611A56C5909,
	CardEventManager_OpenCloseEventUIButton_mD16DC5CFDB59DA120F3A86C03E976EA6067628CE,
	CardEventManager_spawnCard_m8C0EFC6FD963F1386E01E463F8091C2586023793,
	CardEventManager_startCardSpawn_mD1B9D79FA2419EF5C1F4D9D885488E4897F9DEA5,
	CardEventManager_get_designatedGoodBadType_m849499FE1BB3151B8EF36094B0E2B025A24921D4,
	CardEventManager_designatedCardType_m6F5DC114BDD39CA493AC7D264AE6E60AB2BB8885,
	CardEventManager_SelectedCard_m9463E3666A53296C56A78378C66D5603253867F8,
	CardEventManager_ClosedEvent_mE020B4B7933DF9ADAFEF4AAF66DE9FBB4F74342F,
	CardEventManager_AddDayToEvent_m9B95B9313BE24A2CAC3C0818E7A8C85DD3A277C8,
	CardEventManager_CallEvent_m85B35A5031FF4AB265A6AF5A906BB30186DDC4E6,
	CardEventManager__ctor_m87DDB08CCAF594E2D603ED24CF16928ADA10F203,
	Cheating_Start_m38F556D594711CE5180E9381C509180C4697842E,
	Cheating_Update_m97EA5875C51E3B9E925F92AFC90D83032C0E1705,
	Cheating_OpenCloseCheatTab_m3FE5D1229FBE3FA80557F77A5BEF11E2AC6B2564,
	Cheating_OpenTab_mB3A970EF42E35987031A1873B2FF02300293543D,
	Cheating_CloseTab_mD259BB060BA26FD28B8F6D9397081B7EDD3C0B29,
	Cheating_CheatMoney_mAE279B8B4D9CA37A5B0AAAF97D5F2F884C0E7FAE,
	Cheating_CheatMoney2_m96342637F9ADF85C5874E1D9587E9331E964F80B,
	Cheating_CheatPublicValue_m2566A7A2E03C6303A1FB52517372AAB937C7E85F,
	Cheating_CheatEconomicValue_m10B5D2D121D540DEC6EF2652BEDE3424F2DC835A,
	Cheating_CheatMilitaryValue_mF751F1690354B35D60511CB6538C365F2B364604,
	Cheating_CheatElectionDay_m35B1DEB2A51A18CD1A32A5FCE9898203C3008429,
	Cheating__ctor_m93D162DDFA96A378CC68CDE2E915BF81D6DBF0AB,
	ShipClass_get_name_mE79E57465EA2595E7B73E6B1265C5EB04A18E19C,
	ShipClass__ctor_m0E937B988FBC915A5AA3F2FD6BB3F85ADD0D227A,
	ShipClass_moving_mD79E16108D09BE9963A4DDE7EFC27F5640EE94E7,
	TurretClass_get_turretName_m46C7B0DE0F05A4232AE37C8F186602156FF815C8,
	TurretClass__ctor_mAE4842A58B0DF0C6344C0BDF803B410F342A4ABE,
	TurretClass_targetDetection_m4D0ED4B29A9108FDF0F812F04EBB9A3B2778F993,
	TurretClass_targetDetectionCoroutine_m633FD800BF36138D7A2B26D8932088823863C0E0,
	TurretClass_turretTargeting_m8BA82DA5464EBA743C4ECC2FAB492A1381299FCF,
	U3CtargetDetectionCoroutineU3Ed__9__ctor_mBA51F5C3479716B5B5D1CB4CCE4AE935676E7801,
	U3CtargetDetectionCoroutineU3Ed__9_System_IDisposable_Dispose_m13935AB646A11C03594BDF24BE60C20703CC4069,
	U3CtargetDetectionCoroutineU3Ed__9_MoveNext_mCFAFD3B872EB309E2F9B309793AC5ADD12DDFC7E,
	U3CtargetDetectionCoroutineU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AD1BFF77A8EAA12CA4A22894B9D76289F3605DA,
	U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_Reset_mA388A93F226960939E6D49C21E9EEF25BC6599E2,
	U3CtargetDetectionCoroutineU3Ed__9_System_Collections_IEnumerator_get_Current_mFF85508530D8B6F47243884D4293F575601073A5,
	CountryBrain_get_countryGovernment_m26DE7E2E7F082B5F2EA851006F4863C5759F6DD5,
	CountryBrain_get_countryUpgrade_m3596D2161F97D9CB971D03EA2A905643DFC5062F,
	CountryBrain_get_countryEconomy_m796937780C513AE849ECC70AE5DFF87CC2B24F8C,
	CountryBrain_Start_m6912885CA2073680BC4C368450F19A429EF8E3FF,
	CountryBrain_Update_mE3586BB21DF8012E34F47A6582BEC4CEE7582C0A,
	CountryBrain_regionSelect_m1E2A915FFC850EE92F7DDC8C69E96459B1DA10C9,
	CountryBrain__ctor_mCE56B125651C62760E306CA89019CFBA032903E2,
	CountryDistrict_Start_m912584DD25F4BC8F20D39271856629252631CB44,
	CountryDistrict_createDistrict_m9D8368F04F179B7343EDF05BA9BFFB18A158D240,
	CountryDistrict_get_designatedDistrict_m72632C47B4DA167DED4AF8783AB1F26A4BBC5133,
	CountryDistrict__ctor_m4B04111BEF2DA3B38BC5B043AB7E9114CFE64FFC,
	CountryEconomy_get_cardEventChance_mDC1FBD4EE012D9F1DED14B0E043399B517182388,
	CountryEconomy_get_electionChance_m4CC6AC2BD55BDC866621B48CA95F133B36E69A77,
	CountryEconomy_Start_mEA0F88F510C4675446F6CEFF7542BE98F386677E,
	CountryEconomy_revenueCalculate_mFC8B38093063B75F239DD7BA0F9097D4E16A9EA3,
	CountryEconomy_get_IncomeCalculate_m83DF6CBC55500641CF802DB03CFC1B5E313473DB,
	CountryEconomy_get_OutcomeCalculate_m6CE6A6D66C085A2F4EB72E5B15050D95589900D0,
	CountryEconomy_StatCalculate_m8F309A1B832A529D57721073470F61A1E8F7B3B2,
	CountryEconomy_currentStatAndEconomy_m16A5757B5D642372D65F95CE67E4F1AB19BAFF33,
	CountryEconomy_CurrentStatInPercent_m6BEACEAB86A400825C2725EB267D0E7163C6F986,
	CountryEconomy_upgradeCalculate_mBAEEF020289ED6F4636ED54599A0411B8A47249C,
	CountryEconomy_get_CalculateCardEventStatChance_m79EA3633A23C90B2FD50FB798C26888219AF8367,
	CountryEconomy_get_CalculateElectionStatChance_m8E89AFDD89254B9AE65DFE6FCA261ABAD5F80681,
	CountryEconomy_EndOfTheMonth_mF2557B29DDC2B1EAD6F70D81CE72E329152A7D80,
	CountryEconomy__ctor_mCC98899C44072BF6703CD4198F55EEA222AA7EE6,
	U3CU3Ec__cctor_m53FAA027DFCABE8C1174C7BD88ED6E53F9362747,
	U3CU3Ec__ctor_mB34CDBBA6DE39B91D9ACA700E1362AEA5C23672D,
	U3CU3Ec_U3CupgradeCalculateU3Eb__51_0_m1E880F6D0C501B528C2620D287D683D22BCD0DA9,
	CountryGovernment_get_governmentData_m896B901392E40E5DF54EE44616C4B6826E518FD7,
	CountryGovernment_Start_m86C2B74F2124DFDBF0EBFA21714A990ED0D8D017,
	CountryGovernment_changeGovernment_mE09E2FF10E7A81342B1839227F4FAC5170B69D9D,
	CountryGovernment_updateCurrentStat_mD1127FA7282E4B32F320DEF9C054D96223800190,
	CountryGovernment__ctor_mCD399ACFBB316DF55CA5421D0E8F72D3F5709974,
	CountryUpgrade_get_currentUpgradeTier_mC4807DC07B1DE4D90CFA4A95AB277899037A5124,
	CountryUpgrade_get_upgradeData_m2D04625F075E88CE2592FD4EB3AD04EDD13CFCAF,
	CountryUpgrade_get_statEffectData_m7B9EC2E4DF9263961BC1E9FAAFFA42AF5F38B079,
	CountryUpgrade_increaseTier_m44A7F36ED5CE23AE2E56D53113EC31BB7152D35A,
	CountryUpgrade_insertNewUpgradeData_m4DA49ACAF40B70C95E9B76F193AE28278E82A9F0,
	CountryUpgrade_insertNewEventData_m8F74129AF6758B46D599F14486EBE92F257FF691,
	CountryUpgrade__ctor_mCB8C263660CF601EA8D9FA17E83F91A78FBD8C41,
	CardEventData_get_cardName_mBAD9B38CF0B04088C7EC73684C1BDA83512E7F09,
	CardEventData_get_cardDescription_m1E5A7B09F10D7258DDEA33321BB07D6FCC81898A,
	CardEventData_get_effectDescription_m168E468D912729BFD37A075849A8BC4FE9DB9D52,
	CardEventData_get_statEffectData_mEC8F9B6D8DDE2C334BEFD2DBF26DC9B9B7CCBE5C,
	CardEventData_get_subsStatEffectData_m05FAB503C9C174B0F94710DF86AE463CB5DC6560,
	CardEventData__ctor_mF6AC570E6DB9BDB389EFBB7F6881B1F37B9E69A7,
	DistrictData_get_districtName_m3B0A7A438DC28A7EDC702FDB021234172F0F1098,
	DistrictData_get_districtImage_m4B518315EA85A08FC22801F90556883848DD1790,
	DistrictData_get_isResidential_mF95E716529D5FC1C944330697BD050852FF6BB61,
	DistrictData_get_isIndustry_m90148564F8DE8B2C06C89C4962153938EA7AD99E,
	DistrictData_get_isHarbour_m8A261E8B11BD8CE8053D4C890EB830401A932DED,
	DistrictData_get_isCommercial_mFFC3175EC32D1587BFF4FAA4294740A0DFBB3509,
	DistrictData_get_isBadZone_m8AEE05C4AA8A516350E89B469659F96F9BB6932E,
	DistrictData_get_moneyBonus_mF37C88A4138A91C0DAE301C77A802805584A38AA,
	DistrictData_get_publicOpinionBonus_m3DAC4187175544E59B5F517E0C6FC36B262C54EF,
	DistrictData_get_economyBonus_m389A3060D859F71908086B0F77993264E06629BD,
	DistrictData_get_militaryBonus_mDB1FAC68A1A2822D8FA328B024C8CADBA66A58A6,
	DistrictData_get_moneyPercent_mA5DEF5190184C1B9B9950D8A081939D1C3E7DF88,
	DistrictData_get_publicOpinionPercent_mC13CE3B35D013F650C0D7D37FAED5DE0D53BF468,
	DistrictData_get_economyPercent_mDCF574941DBD27D4203CE4C083E8F76681C8F01A,
	DistrictData_get_militaryPercent_mF1E6B08075F4D51B70713CB875AA3BB7CBB6E24E,
	DistrictData__ctor_mFDC75FAF2B37646DDEC41384BFBA1544F0F0BF61,
	EconomyData_get_countryName_mF6CAFE2A64857565F54CC20B1F607EB8C455CAFB,
	EconomyData_get_newCountryName_mFD9CCC0CD34DF9ACC37720C4D46A6AC0391A9E16,
	EconomyData_get_startingMoney_mF7C4EEFB89EE4D7C5BCF8A0B5A6BAEC5F6EFECC8,
	EconomyData_get_days_mBBAD1A61082D971231E8C53FA1337D7200D22701,
	EconomyData_get_months_mA6E6A8C46F0580077F3E4A2655AFD9D40B44217E,
	EconomyData_get_years_m26A5D2198869827F680C0289EF95707F0AD9887F,
	EconomyData_get_electionEvery_m8F13E9B836DF7F13F95E73A6FE8A6FE6EC68285F,
	EconomyData_get_publicOpinionValue_m04AA45A9B2496613CAECB9CDFDF319CAFBFAA99B,
	EconomyData_get_economyValue_m9970D18307128BF7096843FE467E8F9824F8BFA4,
	EconomyData_get_militaryValue_m9FBDC9FA711772266EB3E56382368C6484272E39,
	EconomyData_get_minimumOrderToLose_m5F2BB946545A620417B83DC8DDAB3CA6827B5CE4,
	EconomyData_get_incomeBase_m03008DCF12907ED3478B49F2B72166B327C61261,
	EconomyData_get_incomeMultiplying_m2CB7DD3B4F337272F28E5A5CACE8B0BD279E4F1A,
	EconomyData_get_outcomeBase_mCB50EE00CC4BC502003A957D9F325233ADF67FBA,
	EconomyData_get_endOfTheMonthDate_mFEABDADD2517DC3024E91218F7003DDC75531524,
	EconomyData_get_publicOpinionDecayValue_mDAAF7B41BCBBC8FAE6ABACB4C1881A958205FE54,
	EconomyData_get_economyDecayValue_m00CA6DD50BCABD4465E8EDD4C42A262B351A3832,
	EconomyData_get_militaryDecayValue_mEC0D47FAAC6393F7A5CD4DAE73DFE96C3EEF2234,
	EconomyData_get_veryBad_m90FC791B525C0D587281C0AAC417529142B574A0,
	EconomyData_get_bad_m5BAA33527AB3F90CF9C179BEB7650150497F3345,
	EconomyData_get_normal_m0ED8A334D26144B9E99BFF15431CA7B4E657AE76,
	EconomyData_get_good_m99896FE4DFAF6F4B4D917171CCC3859CA20C22C2,
	EconomyData_get_veryGood_m2134A6B014BD445520ED42CC26FF5AC40D595567,
	EconomyData_get_goodDistrictRate_m308C1B4ACEB8B5FC541A8547C82C2B50FB0A9237,
	EconomyData_get_cardEventEvery_mA85B565EDA1B6F7E3A93AF95B1BF20DA8F7F2BD0,
	EconomyData_get_goodEvent_m06FD9C42FACAE599538682455966C5E2E363B919,
	EconomyData_get_badEvent_m18C8A7CA47ED21537EC556F1135AAE8E3CF32513,
	EconomyData_get_minimumElectionValue_m534C619225ED30A8E521AD91DEA36E3FB65890CF,
	EconomyData_get_maximumElectionValue_m1744BBD84F77A2FF66E76D32EF808931F22D87C1,
	EconomyData_get_electionBaseChance_mEA7B3EF797360A97C7C5485D1C6E5A5C48B65461,
	EconomyData_get_electionDivider_m55BC4D5FD07DBEA01F38A3608BA79F9C1B4D9FC9,
	EconomyData_get_votingResultDivider_m5634C403F2FF081CACA6FD129E47CD6EFCE48697,
	EconomyData_get_votingCheatMoneyPercent_m8AF83BB58CD27BF1D0C802679F2EBA20ABBAD5B4,
	EconomyData_get_votingCheatMoneyMultiplier_mDC7C50F7654114D3D3687253EE0183B38741BD79,
	EconomyData_get_beforeVotingDivider_mC24C65B53F266C717EFFD9BDC218BD5A6B26C428,
	EconomyData_get_afterVotingDivider_mA7EB42693A59CBB797C919E501C4447720CB72ED,
	EconomyData_get_upgradeMultiply_m2863D8521A2A662CCA6073ECC426D233061160B3,
	EconomyData_get_publicMultiply_mAE63886A9B76704B0FAF27A8A5F03BD2F52FC690,
	EconomyData_get_economyMultiply_m25B68928CE08515C00789B74DA531787A04C99B7,
	EconomyData_get_militaryMultiply_m884A0AA5DB72A5D0FB5A3F16E8CA9D7A704EB454,
	EconomyData_get_moneyDivider_m5D3518A4D42F452C3974D90213BFB6BBBCF6FE5E,
	EconomyData_get_badPublicMultiply_m82033CCD3669F347E7DD9295501798B7F9328A98,
	EconomyData_updateNewValue_mE2540EB3C53EBD889B3DED3FFE612E1B115FC75B,
	EconomyData_endOfTheMonth_mD32D4053F644BC7AF446CB31694BFFB4D35E1187,
	EconomyData_get_getCurrentMoney_mFC7EA2F72106D48CEB99EB0C2553D473CA220493,
	EconomyData_changeCountryName_m9C7DFE74CEDFFBDBA008170C54A813C8ACC5395E,
	EconomyData__ctor_mFFB98EE599F23728210BE480E06751FE27EFC3DD,
	GameData_loadGameData_mD4A0B810F65FC82B7F16D83C30C16908A0E75B22,
	GameData_saveGameData_mF07531056195A9CDED3566E76707AE63F5F3E483,
	GameData_transitionCompleteSwitch_mC93504D1E1267016AE46BD8BC848A3DB75D8563E,
	GameData__ctor_mF787127A0B2A0A3EBFA33BAB7050087D66E80F14,
	GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232,
	GameManager_get_countryName_m31D2248199BF40AC162091777DAA53242E2CB925,
	GameManager_get_publicOpinionValue_mA3A1BE248BD88479ED97D303E6DB3C70A3434931,
	GameManager_get_economyValue_mAA20EE6E4B693BBFCBCE06E546F382A459C16196,
	GameManager_get_militaryValue_m626CA1ECA2A6126A4254E313D537287031184112,
	GameManager_get_electionStackupChance_m9ADCE6BB1A2D3965AFF96A79BBEB8A285E73C6FF,
	GameManager_get_economyData_m9EFD32566B6C8B0C8AC28EC0D06F4E4818965504,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_loadDateData_mAFEE44A35CF54F0FA2DC48F3430E06A86E4A5944,
	GameManager_loadMoneyData_mE6D1E1A168AACADA2A7249B901452A5757E7C38B,
	GameManager_loadCountryStatData_mDD4191F3E78C68BB8A56FCD3EE0F72237FC0D4C1,
	GameManager_addDataToCountryStat_m8869AF5AC61D5A71844E4709D436F691ECB2FF78,
	GameManager_AddElectionBonus_mBC56F1FEAF9B1BE79E959B310DC1E25F2821D72A,
	GameManager_DebugSkipEntireMonth_m0DB0BD18E6906BA29BF1FEB96D11AA3AF93E00A9,
	GameManager_DebugStartElection_mD2EAB4CE9C83A1606492DECCFC377CFE2EC75C32,
	GameManager_CheatPublicOpinion_m142849F4AA27F9A96EAC378F3BAC881E5D5A5943,
	GameManager_CheatEconomic_mD9432D80ABC2ED0F469ABF1989EA993346326689,
	GameManager_CheatMilitary_m59142F75AD5FBD5DC534564AAD46E18F120C8870,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	GovernmentData_get_systemName_mF626D3FF534722409B5573E8D06F9C235432E715,
	GovernmentData_get_governmentSprite_m42BAEF7B483321CA7FAF70AFE9EE7A5F5262E86F,
	GovernmentData_get_governmentPassiveStat_mB3A8B80F12817BDE0D73DF12BF1B35E2FC3DBB4E,
	GovernmentData_get_hasElection_m5EA0D871AEBA68A6019F9395E30B94962293D2BF,
	GovernmentData_get_StartingFund_mBD320DFB328B059C5535060644038C25BAD0929A,
	GovernmentData_get_publicOpinionValue_m3B0434F04A3D124F2B7E08F3B7449EC5A3A2779F,
	GovernmentData_get_economyStrengthValue_m202A6F952377F629D4222BEC623BDD1775BACB31,
	GovernmentData_get_militaryStrengthValue_m5C6DBF15CCAC03F82B97A7E8829E8D0C03CA6BFD,
	GovernmentData__ctor_mAD64C350792317C17A6D04F4C7E82C270DBB6B16,
	StatEffectData_get_perMonth_mEBC235B2B5501FE85CC65297CBF2AFB372BF50CE,
	StatEffectData_get_moneyBonus_m8C8D949AE9CF1FC14592453178F5ABAE7B314A57,
	StatEffectData_get_publicOpinionBonus_mB0C04288B1065D51F55C322141CC6BD4CE972ADC,
	StatEffectData_get_economyBonus_m9E1FF70C5BE164E5108AB8958A6F074D28610C22,
	StatEffectData_get_militaryBonus_mAC8E6FE23355AA1F15885460CFB419609C05DEC0,
	StatEffectData_get_moneyPercent_mAF24E24C754DDAF8B7360586F6CD60D11901D7C5,
	StatEffectData_get_publicOpinionPercent_m1FE483A83A1BA09F125291A81DFF347CF84C8D61,
	StatEffectData_get_economyPercent_mDC82FFB395BCF5A1FB28B9A0485A9D8019A12237,
	StatEffectData_get_militaryPercent_mA11680A1E0A8D8EAC80B3B24F8334FA696CC96A8,
	StatEffectData_get_electionPercent_m77D70CF732B57DD4292B6AA097EE6ED6352F0222,
	StatEffectData_get_eventPercent_mD5DEBE259A790F2192A8E71794117D188868CB7F,
	StatEffectData__ctor_m2A05396F02559188384619E79D612794ECFF08D2,
	UpgradeData_get_upgradeName_m11838A2F769B71A117532719E0436180C6D494AA,
	UpgradeData_get_upgradeDescription_m66CC8AE04910FB8EDB61E1A2032373FE7D07BC50,
	UpgradeData_get_price_m6315317427C38B42CB2DB07ADA4C5A9EDC77DA80,
	UpgradeData_get_upgradeSprite_m4FC22BF9F48892BFB6F198938C0AC6B752F966B2,
	UpgradeData_get_statEffectData_m74C632D00501CE19D0EA6276565CD6869FE36606,
	UpgradeData_get_secondaryStatEffectData_mE4CE8E348C290E2E9814C16D2AFBC8BF17D741B2,
	UpgradeData_get_upgradeTier_mCA6458D7AA250432E695113F1C57C715A8E8E123,
	UpgradeData_get_unlockNextTier_mAC3769C932B1EE7996F722C92EE9AB56D822D38B,
	UpgradeData_get_openGovernmentTab_mACACB61B2CF1B5BA814E820E43D97D857F69D9B6,
	UpgradeData__ctor_m19BB07FDA91289152BB1B640386238C25312E101,
	DistrictBrain_Start_mC55A53A39B8520542033B8AE4BE3AA146E1CB2B1,
	DistrictBrain_Update_m53874AC59B8EEF435C9E5C394222747535783CFE,
	DistrictBrain_districtEconomyToCountryEconomy_m5DB42ACAB5C6FF91F46D989CBC58926E9B18C6C4,
	DistrictBrain_districtStatToGameManager_m202E77DE9877B97552755F093EEFCF36E64D29A7,
	DistrictBrain_regionHovering_mB4992574F46CA7CB4E5F92678114BCF3CF2DD1BB,
	DistrictBrain_PlayHoverSound_m743BB4603B5EB6EABC168DBA1883C2673E6C20CB,
	DistrictBrain__ctor_mFC493167A7CFF35BACFAD56A981655B25BF2D3FA,
	DistrictClass_start_m509C4E464D6DD389637B48F7863F9F69EB04B97F,
	DistrictClass__ctor_m5E88372038B07B6300650198ADA3CC47457A5511,
	ElectionManager_get_countryGovernment_m59A8A2219700F158EF02E7749A2D5641DCE097AD,
	ElectionManager_get_governmentData_mC948167CFC3674BDCC7727A20B247C45C4FC3173,
	ElectionManager_Start_m0B5590808113860EF37A1D7B60F266B3AE75FDA5,
	ElectionManager_Update_mE3B330A6E17A9B9CF00F7FB8708ECD6542C505CC,
	ElectionManager_UpdateValue_mAE8753AB408948ECCB35A90EF177F0E171E5D9EA,
	ElectionManager_VotingSound_mA687F0936E65B382802CB84D8A8CBDC43B84BBAC,
	ElectionManager_EnterElection_m266B3DFF2F59123CE8DD8F081D8ECC2776FCF517,
	ElectionManager_StartElection_mB1E27AC7EC835F5038FE155589E79BA42B1EDB75,
	ElectionManager_CloseElectionTab_m36BB1ABB87DD6755A1AF9292DB5908ABCAC5ECFF,
	ElectionManager_UseMoney_m84BD1601A6A3FF006EC43C5EF1F0A69EC2D68644,
	ElectionManager_PriceToBribe_m2ED5D0AB693D21779D8A12B833FF170B8D9F828D,
	ElectionManager_OpenCloseElectionUIButton_mE55F36B3892E0E88C581D29ED9529503ED07EA0F,
	ElectionManager_get_ElectionCalculate_mE2C1F9B38F0B73917668AB7C7F5D0D416B33D270,
	ElectionManager_NewCheatElectionValue_mA51DE15E649C77B08C362B69BC1D924038FC4853,
	ElectionManager_NewCheatResultValue_mD24F2D0B056A54B8D07E9A49D2084E771647A846,
	ElectionManager_AfterElectionCheat_m23D0A4587C1CAFA23361AD90C51A52334B5C45D1,
	ElectionManager_ElectionResultReport_m8A1FAB8A425511C781AE988974DEDA774A47F828,
	ElectionManager_DebugCheatingVoting_m1831DCAA67941EDB7DFF1C6A339B9A5862225FA2,
	ElectionManager__ctor_m0EDEA08C528A4674FB63C9D93960A6ACDA4D0891,
	GovernmentBrain_Start_m57AF6019298785EB7560B0CE3E13688B7E9A984E,
	GovernmentBrain_startCallOut_m93DE4D60EF69B3310CFB51CB2A57E35EF797F3AE,
	GovernmentBrain_selectSystem_m2FB1B7BB23F687DD70A9AC6CA86B11DC13659B24,
	GovernmentBrain__ctor_mCBA2F9B7E1FDB2B9579751E3E741F66D812811C5,
	PlayerInputManager_get_Instance_mF8F7F5261FF5FAF01C2BBD0056369800A2372958,
	PlayerInputManager_Awake_m604E659FF3A4DD2E1F03683B340186656960368E,
	PlayerInputManager_OnEnable_m7429B0738543BF35A8BCAED2F8CC0F714D9874D6,
	PlayerInputManager_OnDisable_mFBA07FB429774CC92ED02D04C7B624690528AC54,
	PlayerInputManager_GetCamMovement_m6020E13D390A483EF77A3E1090C0C0BDCE7BD341,
	PlayerInputManager_GetMouseDelta_m383452689DCFD8D1F6C1EFCD30108F67C8CF726C,
	PlayerInputManager_GetMousePosition_m2D034B68812106EB36E549E371F3F02E38A04371,
	PlayerInputManager_GetMouseScroll_m2651D1B3C1A39983C370889F4E660D0E7115CA98,
	PlayerInputManager_LeftClick_m3DB0EA872EDB02AB5C5A9B4AA87639B7B6716974,
	PlayerInputManager_RightClick_m47E2EE6C05812A73C355C85423255AB582F56E41,
	PlayerInputManager_EscapePress_m2F61942AD1759CB8E4F5B8689BFFFD9544A7D4EA,
	PlayerInputManager_SpaceBarPress_mDC5B3E2C0B19773762B2693C1D8C14AFC3F61EC2,
	PlayerInputManager_tutorialNext_m9DD4B808F7ED3FBA3105C3E5E1B9D2B780FC4084,
	PlayerInputManager_CheatPress_mEA946ABB139E20879D119FB96C77972232509467,
	PlayerInputManager__ctor_mD8A05B4F73F64E8BD5F0DEBAA227E240F3CCF566,
	ShipBrain_Start_m46244685DF92D7CA2ADD0A55098FA256B9F12C00,
	ShipBrain_Update_m59DBC28230860E7CD4B4956F8449E935C4310ED1,
	ShipBrain__ctor_m235F7ADD0E3BB7076FE5C83C78726EF743714FAE,
	TurretBrain_Start_mDE0C02C8A41D1086014F451BE1673BD79EE4CEF6,
	TurretBrain_Update_m61D1F27A04A8F7C9D269DE378DF2449155823C4D,
	TurretBrain__ctor_mF99056F6D2E2507079984F900BA2E0DC338108B7,
	TurretController_targetDetection_m23A10C5771418102BF46883AB5450E0023173B54,
	TurretController_targetDetectionCoroutine_m0863CC446CC1B4BF7AD0E35EF880D35D140406B5,
	TurretController__ctor_m473631F3C700500730044026BCF22B2116A71B54,
	U3CtargetDetectionCoroutineU3Ed__6__ctor_mC8B7A8B1CED73E7F9F6B502438360E6952DA2248,
	U3CtargetDetectionCoroutineU3Ed__6_System_IDisposable_Dispose_mB9DC26CED4951C9A00BA13465A551E3B28B6D602,
	U3CtargetDetectionCoroutineU3Ed__6_MoveNext_m900452B73E8D53ACC0233244B82DA370F6978E8D,
	U3CtargetDetectionCoroutineU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC31340DA08D735F8804197AB7C57EF0DD5C47122,
	U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_Reset_m944CD9F92A9C4868E8D50FC36DF765670E37178C,
	U3CtargetDetectionCoroutineU3Ed__6_System_Collections_IEnumerator_get_Current_m4C82B1B46097F2A043D70F19EBFCCA082A9BD3EB,
	RegionBrain_Start_m6D9A3482F3520E499C42A961C0594E0A629A8913,
	RegionBrain_Update_mDE9100C051FCF1692F4C015C72982F313354C945,
	RegionBrain_regionHovering_m15D56C88709BBD0393B6154E2428305B868DA3AA,
	RegionBrain_changeHighlightColor_m9E333BB4E847522155EFE00E6E1DB6A8EFCA3FEE,
	RegionBrain_resetColor_m1311E7B8F7A27763E4CC8E590DB81F19A8FA2E10,
	RegionBrain__ctor_m9AC85AD60108CE169BE79F758DF427684F6E933E,
	TutorialManager_Start_m0CE3E9449B63E7930C4035203E1ED9B260E72D59,
	TutorialManager_Update_m0486AA97C0E1344C37EE31B176ED32EE3B4446D9,
	TutorialManager_SimpleTutorial_mA790574AB9E5CBF77FF2601F1303F90ECE3AB3F1,
	TutorialManager_PlaySession1_m7EE6128D7404D320A891A292410C878C8C79ED33,
	TutorialManager_PlaySession2_m775D8D8A1B829140880092A81BC491302ED82F01,
	TutorialManager_PlaySession3_mC74B91AC4AC7D9F418FDE823614CB9299E1E9F39,
	TutorialManager_PlaySession4_m4739D2284EA994370681182096E0AD274D12C56C,
	TutorialManager_PlaySession5_mDC42E4AFE68DFDAEE8A817EB04B1AECCF1DF737E,
	TutorialManager_PlaySessionRanking_mD07203D95432994C9E32685340B4032DAC15F1F4,
	TutorialManager_PlaySession6_m64F8F9692FC7599BB1DF38F045FD3E5ACDDA74F4,
	TutorialManager_StopPlayerMovement_m74333738AEBA9C5BC31FBE387DE83AE0A5E27FD5,
	TutorialManager__ctor_m1A6F653729E4CCE30F0A0BC39EEB304409459A84,
	TutorialTextBrain_Start_m2B99EA0792942D055DFC7F6D3740670008A2E3EB,
	TutorialTextBrain__ctor_m87E4C272DF0BF017A8EBB50072348BE9FF70BBA3,
	CalenderTimer_Start_m6BF381D5D058C7E1D9564C90235E549874DD2DC5,
	CalenderTimer_Update_mA2CAA2E80284E8C0AD1089C7AC7EBE852F59675A,
	CalenderTimer_Timer_m103F133D645200CD8754FEA50812C2794C937034,
	CalenderTimer_changeSpeed_m950D1E22578EAF9763653C65A8D853BF3C5E9127,
	CalenderTimer_pauseButton_m45CAB65FCADFC9C001C5D11A99241EA43B908D73,
	CalenderTimer__ctor_mED929E0471C84B2F9C9534D7219ABE9B28FB9738,
	CountryRankBrain_get_rankNumber_m0104A98522D35471E5C9EB6190C8DBF8D594E277,
	CountryRankBrain_get_scoreNumber_m7F658C58FD9FBAA939678BB064C0438C8B289677,
	CountryRankBrain_get_currentIndex_m1E2A6BF2C123196DD515EAFEB50DA8D8D640A1DF,
	CountryRankBrain_get_isPlayer_m2E17DC36F29801A1214D110469D402556F3FCA7D,
	CountryRankBrain_SetUpBrain_m0C5330863FAEB70491C86250477D42774F87B95C,
	CountryRankBrain_setRankScore_m95C01F331F07C7A9B31F550664CC1C4C0685BB19,
	CountryRankBrain_changeScore_mABC6F74148629F2E0B151E052CAFD88621F39DEE,
	CountryRankBrain_changeRank_m4E73C8412CBA373A10F50D1DA93F46AF7F59F415,
	CountryRankBrain__ctor_m2D50AC51BA826FED36BE56F552646A44F4900890,
	CountryRankManager_Start_m1DE6784A2A4877BA5DCEB11AC4CC2BDBA2D0D369,
	CountryRankManager_Update_m65E1BB9980D711210DBA24A37419E7993B3497DD,
	CountryRankManager_Ranking_m05D9F53A981AC129BC1EEE1289DF7E6D089BA43B,
	CountryRankManager_SpawnSetup_mD40C56A4F575DE8119138C9CEBA148514CEDB29B,
	CountryRankManager_SpawnPlayerRank_m9BA65494353532C11B4AB4AB287DB3B86818058A,
	CountryRankManager_get_ScoreCalculator_m8DD3DAF682481DD72F8FD61F8F03F80361788B9F,
	CountryRankManager_OpenCloseEventUIButton_m68CC5861E43D0E28556442CF028176C6874B09C4,
	CountryRankManager__ctor_m996302B6194B7EB29EA12EAB6E60BE333FD5FDB7,
	CountryStatBarUpdate_Start_mAB8332441BF9F670AE27A98BD56964E7E0518787,
	CountryStatBarUpdate_Update_mC39605D75D9331A0F1B2DAE51EE234848DD93914,
	CountryStatBarUpdate_updateBar_m9A8CE752A9774F6F1F19F32B6E129703E67E1FEE,
	CountryStatBarUpdate__ctor_mFB9B1B26184EA916BCD8A0C0C3EBAC5A9BC11A7A,
	ElectionBar_Start_m2FFFB413BBD2F87F1FC5BB8610B1EE792532F562,
	ElectionBar_Update_mFC726ECBE088C9B8EAAD4A164321E7CDD9E3A67A,
	ElectionBar_ElectionUpdate_m785D72B10A7C45725691AF75FF422180C711BF5F,
	ElectionBar__ctor_mAA0877B53DE097FD67854D0B4FD9089AEC05C5D7,
	InformationText_Start_mFE511E8C05973A5905C14D34EAE51B30AD2EC695,
	InformationText_textDisappear_mF0444AE73D21EBFA1564E06201BD84889D929635,
	InformationText_SetText_m819FAC546E50E90C5921E4F4AF4CB3C6F1831E89,
	InformationText_SetMoneyText_m711C3300D7D3495EFDF2A3D98E993FBDBE4CEC2A,
	InformationText_selfDestroy_mA11162CA297ACCF1FAF937EB6D0083AF4E11CFF9,
	InformationText__ctor_m5FA57BDA8717F3A8B2A7D54F4139A9700ABFB85C,
	U3CtextDisappearU3Ed__5__ctor_m8DFA67F978C62965EFC1AF45AC6E991523C98153,
	U3CtextDisappearU3Ed__5_System_IDisposable_Dispose_mE481914892D7D05A8CC111A5FFE4C9D7CD39B455,
	U3CtextDisappearU3Ed__5_MoveNext_mE17846254DA0C2A71BDE15991D88C512AD9BDB16,
	U3CtextDisappearU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2C8295679F0BC19976D82D542416718DEDDF25A,
	U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_Reset_m981818C756CF88BE287D53C3CE979E0A64036394,
	U3CtextDisappearU3Ed__5_System_Collections_IEnumerator_get_Current_mC8F4D7976B07AFEA6EB40BBC5577579B62F135A6,
	InformationbarUpdate_get_spawnTitleColor_m1C925EC8F26FEB7D18D25B115C838EA6A119FF4A,
	InformationbarUpdate_get_spawnPositiveDescriptionColor_m0B81894C6B5FBD0C4D43AD8F642495932FE942C1,
	InformationbarUpdate_get_spawnNegativeDescriptionColor_m9D01262E52C263C78064441628488FB72800DAE8,
	InformationbarUpdate_spawnText_mF84AB3C416C3296585C9C653DC0059B4CF4F05A1,
	InformationbarUpdate__ctor_m4C1CF1B1788A29A8262CD84EC184B22BA1F496C8,
	MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2,
	MenuManager_onStart_m37A432E425B6C737EEC07E155DD5E28006167692,
	MenuManager_playOpen_m009219C377403B43F6D50C5A0FCE324638A7C7D0,
	MenuManager_settingOpen_m50C827E8AC177315BFF3D25031737E0A4F268963,
	MenuManager_creditOpen_m370B5D6F6500A4C3AE4708911A0AE8813034DAEC,
	MenuManager_returnToMenu_mD96BF329F1B7E8CC9F3F48D780125006D056ACDD,
	MenuManager_loadScene_mB717D5E9676CEF035C842EF68CE71DC876F68F3A,
	MenuManager_exitGame_m2398AD601B7A003C8292E90B41BEEE1ECCC8337A,
	MenuManager_changeCountryName_m678BD873168A8F695F574EC856D8111293ECB9C3,
	MenuManager_loadTutorialStage_m993543543599FA4413065AAFF9977BB027ACA302,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	PauseMenu_get_gameData_mDED7068E36894CB5DCFF10639AB49783B50B9B7D,
	PauseMenu_Start_mB3762C9E5B204FDE0381A6409728D7DCAD7E6C37,
	PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57,
	PauseMenu_onStart_m6603E7C3E54C72676E857FB0E2140F5526F9969B,
	PauseMenu_PauseAndResume_m62DAF4151BA954D2C4A9D45A15F2006223CEDD23,
	PauseMenu_Pause_m395165A04A026E9974327328181ACFA512DD76C7,
	PauseMenu_Resume_m256AFDD68DF9851B6D716189F709ED93D45C3717,
	PauseMenu_OpenOptions_mCE41B2A7D1EF301C1B3A0245AA77A21527721864,
	PauseMenu_Return_m34397667F74A7177E2DCFF524B13594B5785AAF5,
	PauseMenu_loadScene_mDD0743151614450F11B1AD8249E9EA02BE6BDA8D,
	PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018,
	ResolutionSetting_SetResolution_m6CE1C2CA19F20697FFF1F93D0A4E7FD718D282C2,
	ResolutionSetting_SetFullscreen_m9A3ACC61D389703B6579FD109FCB33F77911A5A8,
	ResolutionSetting_dropDownCurrentStartResolution_m561E225092EB39044346CA7CF855022DEA3AD828,
	ResolutionSetting__ctor_mB0A19DB25DD45D37837224D84E80A95C1F0739AD,
	SceneLoader_Awake_m0618C263DDE94B4607D9D5148F87247B506A337C,
	SceneLoader_startLoadLevel_m04754948AF7C6B19FB5FB11C8875F6F6B8D3EA25,
	SceneLoader_exitToDestop_mABAD45692C757391FE8C86289AC7128ABFECC8F3,
	SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97,
	SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36,
	SceneLoader__ctor_m8708D080848349110CEA260D8779F30BD5823912,
	U3CloadlevelU3Ed__6__ctor_m9E1A61EAB40A3F7E55748634CF1479A83D7F0918,
	U3CloadlevelU3Ed__6_System_IDisposable_Dispose_m3843887F758EDED505C5740E27804345C91A4EA5,
	U3CloadlevelU3Ed__6_MoveNext_m68C2A1F46A5BA914646835FBD00B903007D5823C,
	U3CloadlevelU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66A182F82237A6982B1215DFBC583137A405D67B,
	U3CloadlevelU3Ed__6_System_Collections_IEnumerator_Reset_mC33203E5184B62410066554665A683CBBA2473D2,
	U3CloadlevelU3Ed__6_System_Collections_IEnumerator_get_Current_m42EAEC5702621CBB823CE3BC0BB3AAA3FCD734D0,
	U3CexitGameU3Ed__7__ctor_m34E1725C8F3B9C5DD0D1FDDF031FFA605C0B536E,
	U3CexitGameU3Ed__7_System_IDisposable_Dispose_m215E49BCB1D7D0EFBC5830D1BD850C61C13AF9E0,
	U3CexitGameU3Ed__7_MoveNext_m9893BA2217E1D8E3EC7678321B9F3912D4F1B263,
	U3CexitGameU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9120CA81B69CCFA43D38D74EE44FE8D7A435F707,
	U3CexitGameU3Ed__7_System_Collections_IEnumerator_Reset_mDCFD249F2AF563D7D20D1091C1772940EA653B3B,
	U3CexitGameU3Ed__7_System_Collections_IEnumerator_get_Current_m9807A48ED782BD25BC3E49EA47642C45B903ABEA,
	SceneLoaderCheck_transitioningSwitch_m22454809B7646961A344498212874F4F70E0FC1D,
	SceneLoaderCheck_transitionCompleteSwitch_m141092CC40E193C2A57FC4326CED10D3021A3932,
	SceneLoaderCheck__ctor_mAF65D9F76BB3A414D42F532F7A00A4D4397989BA,
	UpgradeItemUI_Start_mCECD4E4957C906C234B03F67777832943AE8E3B3,
	UpgradeItemUI_Update_m347754ADFE3AEE0FEEC3568D96586302CE23BAE2,
	UpgradeItemUI_check_mD67D15B0FAF0B7F7381622E5B3B999DA0A03676D,
	UpgradeItemUI_buyUpgrade_m330820EEB8F9FEDC4F52857C3B5A69E7CEE95005,
	UpgradeItemUI_changeUpgradeText_m57343E414F9D313511D812809CE37C11BCFCF88C,
	UpgradeItemUI_clearUpgradeText_m3511EBCC5C2C85E820BD5CC93D54A1E6AE1CCCC3,
	UpgradeItemUI_OnHoverUI_mBE3D1963E403B86798A10CF9874866C164FEF8C3,
	UpgradeItemUI_OnClickUI_m1D1B490C888F8757E439D23349A23EA368D7F4DE,
	UpgradeItemUI__ctor_m8E161961355F560EB46B0DC329DCFD84AB9D8DA0,
	UpgradeUI_get_upgradeContentTab_mA9522FBA98323AA9155C38E4F2E2FA1A8E423734,
	UpgradeUI_get_governmentTab_m283782268755E34A62BEAA2D5213D2132FC8C04D,
	UpgradeUI_get_countryUpgrade_m05B8077D76597F847F7AF799DAE409EAEA88E874,
	UpgradeUI_Start_mFA6E269456D93CEA02D684B20804673E4968C4E3,
	UpgradeUI_OpenCloseUpgradeUIButton_m7167EC675FD542F02F8FCECDDCBD1320C22A00FB,
	UpgradeUI_changeUpgradeText_m894D5DE80D03B3922476382780B94EF7D0D8E058,
	UpgradeUI_spawnUpgrade_mFFA57776A8A8A156CA48BD739BCE6F7BF2E6BBEC,
	UpgradeUI_checkCurrentTier_m5BEEAD00E65B321E5D102A1CAC3F11BE995CFFB5,
	UpgradeUI_unlockTier_m6D4A273DEE587DFEB9BE4C024A35502322675145,
	UpgradeUI__ctor_m5C6191FD99F1E10B4D1115F06471E39E59727EF1,
	WinLoseManager_get_winDescription_m1160B0F5C018A1807668D47F1A74F278F3C911C8,
	WinLoseManager_get_loseDescriptionVoting_mCD6FE18EC2A7AA0E6F61667DBA9569EC23394953,
	WinLoseManager_get_loseDescriptionOverthrown_m11AF8413454755B9BA53617373012193B65D2417,
	WinLoseManager_get_loseDescriptionNoMoney_m736AC40B6FCE70638F0AB52987E9508E4733938A,
	WinLoseManager_Start_m2E32930E3AF736B1D19D5F101559E63754AE4CE6,
	WinLoseManager_Update_mD99C4FF5969FC2B24E42809CCBFD4FCAA49FCBF6,
	WinLoseManager_GameOver_mF9911D19E50D5039EFBB0BA3D8D108D9EFA8E620,
	WinLoseManager_WinGame_m1342034D770605BE666C0AB4E3B95533F11011BA,
	WinLoseManager_loadScene_m413563DFDD56316B7E5D5D1E0FDF4FF11213D931,
	WinLoseManager_resetScene_mF7A5CBCBF5529E201E1D74041F31AFCFADF50490,
	WinLoseManager_LoseCondition_m8AE6F37675B9248E7B114B81A3975C79C5415A85,
	WinLoseManager_OpenCloseElectionUIButton_mD2F0B63D1E3F0C905234196290946F07CB9F34C1,
	WinLoseManager__ctor_m9BEA6A4A6A01273878129498C2CC4177230EDAC9,
	moneyUpdate_Start_m60F18BE7A4CA09B2BD23F203FD220F18E63DF14D,
	moneyUpdate_Update_mEBFDC6BD2C393AB7588329D8F25914498DFDA8F6,
	moneyUpdate_money_m45C7AD16C50D5E37D8C8C8397FC33AFF98B765DF,
	moneyUpdate_updateMoney_m78DC7861B9206BEF03043D40240DBCA4FB671E71,
	moneyUpdate__ctor_m1C4DC32ACB5BB0C130042F32BB94DB8AF72823DD,
};
extern void PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621_AdjustorThunk (void);
extern void PlayerActions_get_CameraMovement_m9537EE57866532B0778B7AFEBCE6DACEA9288508_AdjustorThunk (void);
extern void PlayerActions_get_MouseDelta_m392EE5FB891CDA0EB030C4F407DA0F4A0AE67194_AdjustorThunk (void);
extern void PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26_AdjustorThunk (void);
extern void PlayerActions_get_MouseScroll_m988F2A5C3F04837EAA4D88F052D7870DEC3F6B31_AdjustorThunk (void);
extern void PlayerActions_get_LeftClick_mBA9C4A91D948482870A5B3D6F299F1C2BEF78B23_AdjustorThunk (void);
extern void PlayerActions_get_RightClick_m613ED5127E32C7A730EA7B303B37CFD6E5B2A402_AdjustorThunk (void);
extern void PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD_AdjustorThunk (void);
extern void PlayerActions_get_Spacebar_m70DE77C67F95D3504D5250C5BC4299D65E907C7E_AdjustorThunk (void);
extern void PlayerActions_get_F1Cheat_mC8E401D56F3F86E371011488053C2B0D46393FFA_AdjustorThunk (void);
extern void PlayerActions_get_TutorialNext_m5D0CA16ABE26948ECDF5421ABE9F6E9F304C9FE3_AdjustorThunk (void);
extern void PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8_AdjustorThunk (void);
extern void PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C_AdjustorThunk (void);
extern void PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9_AdjustorThunk (void);
extern void PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7_AdjustorThunk (void);
extern void PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[16] = 
{
	{ 0x0600000F, PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621_AdjustorThunk },
	{ 0x06000010, PlayerActions_get_CameraMovement_m9537EE57866532B0778B7AFEBCE6DACEA9288508_AdjustorThunk },
	{ 0x06000011, PlayerActions_get_MouseDelta_m392EE5FB891CDA0EB030C4F407DA0F4A0AE67194_AdjustorThunk },
	{ 0x06000012, PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26_AdjustorThunk },
	{ 0x06000013, PlayerActions_get_MouseScroll_m988F2A5C3F04837EAA4D88F052D7870DEC3F6B31_AdjustorThunk },
	{ 0x06000014, PlayerActions_get_LeftClick_mBA9C4A91D948482870A5B3D6F299F1C2BEF78B23_AdjustorThunk },
	{ 0x06000015, PlayerActions_get_RightClick_m613ED5127E32C7A730EA7B303B37CFD6E5B2A402_AdjustorThunk },
	{ 0x06000016, PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD_AdjustorThunk },
	{ 0x06000017, PlayerActions_get_Spacebar_m70DE77C67F95D3504D5250C5BC4299D65E907C7E_AdjustorThunk },
	{ 0x06000018, PlayerActions_get_F1Cheat_mC8E401D56F3F86E371011488053C2B0D46393FFA_AdjustorThunk },
	{ 0x06000019, PlayerActions_get_TutorialNext_m5D0CA16ABE26948ECDF5421ABE9F6E9F304C9FE3_AdjustorThunk },
	{ 0x0600001A, PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8_AdjustorThunk },
	{ 0x0600001B, PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C_AdjustorThunk },
	{ 0x0600001C, PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9_AdjustorThunk },
	{ 0x0600001D, PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7_AdjustorThunk },
	{ 0x0600001F, PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7_AdjustorThunk },
};
static const int32_t s_InvokerIndices[507] = 
{
	2649,
	2708,
	2708,
	2537,
	2098,
	2535,
	2096,
	2546,
	1866,
	2649,
	2649,
	2708,
	2708,
	2750,
	2195,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2708,
	2708,
	2677,
	4045,
	2195,
	2264,
	2264,
	2264,
	2264,
	2264,
	2264,
	2264,
	2264,
	2264,
	2264,
	4197,
	2708,
	2708,
	2708,
	4197,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2649,
	2708,
	2708,
	2708,
	2708,
	2195,
	2195,
	2708,
	2708,
	2195,
	1536,
	2708,
	2708,
	2224,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2246,
	2708,
	2649,
	2649,
	2708,
	2708,
	2708,
	2195,
	2195,
	2195,
	2708,
	2708,
	2708,
	2677,
	2708,
	2708,
	2708,
	2708,
	2195,
	2633,
	2708,
	2708,
	2708,
	2181,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2649,
	736,
	2708,
	2649,
	477,
	1250,
	581,
	1250,
	2181,
	2708,
	2677,
	2649,
	2708,
	2649,
	2649,
	2649,
	2649,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2633,
	2708,
	2681,
	2681,
	2708,
	2708,
	2633,
	2633,
	2708,
	2708,
	1554,
	2708,
	2681,
	2681,
	2708,
	2708,
	4229,
	2708,
	1866,
	2649,
	2708,
	2195,
	2708,
	2708,
	2633,
	2649,
	2649,
	2181,
	1247,
	2195,
	2708,
	2649,
	2649,
	2649,
	2649,
	2649,
	2708,
	2649,
	2649,
	2677,
	2677,
	2677,
	2677,
	2677,
	2633,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2708,
	2649,
	2649,
	2633,
	2633,
	2633,
	2633,
	2633,
	2681,
	2681,
	2681,
	2681,
	2633,
	2681,
	2633,
	2633,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2633,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	685,
	2677,
	2633,
	2195,
	2708,
	196,
	196,
	2220,
	2708,
	4197,
	2649,
	2681,
	2681,
	2681,
	2681,
	2649,
	2708,
	685,
	2181,
	756,
	756,
	2224,
	2708,
	2708,
	2224,
	2224,
	2224,
	2708,
	2649,
	2649,
	2649,
	2677,
	2633,
	2681,
	2681,
	2681,
	2708,
	2677,
	2633,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2681,
	2708,
	2649,
	2649,
	2633,
	2649,
	2649,
	2649,
	2633,
	2677,
	2677,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	724,
	2649,
	2649,
	2708,
	2708,
	2708,
	2195,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2681,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	4197,
	2708,
	2708,
	2708,
	2703,
	2703,
	2703,
	2703,
	2677,
	2677,
	2677,
	2677,
	2677,
	2677,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2649,
	2708,
	2181,
	2708,
	2677,
	2649,
	2708,
	2649,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2181,
	2708,
	2708,
	2633,
	2681,
	2633,
	2677,
	455,
	1149,
	2224,
	2181,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2681,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	1670,
	735,
	735,
	2708,
	2708,
	2181,
	2708,
	2677,
	2649,
	2708,
	2649,
	2588,
	2588,
	2588,
	737,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2181,
	2708,
	2181,
	2181,
	2708,
	2649,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2181,
	2708,
	2181,
	2220,
	2708,
	2708,
	2708,
	2181,
	2708,
	1660,
	2649,
	2708,
	2181,
	2708,
	2677,
	2649,
	2708,
	2649,
	2181,
	2708,
	2677,
	2649,
	2708,
	2649,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2649,
	2649,
	2649,
	2708,
	2708,
	1247,
	2708,
	2633,
	2181,
	2708,
	2649,
	2649,
	2649,
	2649,
	2708,
	2708,
	2195,
	2708,
	2181,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2708,
	2181,
	2708,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	507,
	s_methodPointers,
	16,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
